


let COMMON_FUNC = (function () {

    let concrete_mat = ['3000', '4000', '4500', '5000', '6000'];

    let steel_mat = ['A36', 'A53', 'A242', 'A500', 'A501', 'A514', 'A529', 'A572', 'A588'];

    let weld_mat = ['E60xx', 'E70xx', 'E80xx', 'E90xx', 'E100xx', 'E110xx', 'E120xx'];

    let bolt_mat = ['A325', 'A307', 'A490'];

    anchor_names = [
        "1/2",
        "5/8",
        "3/4",
        "7/8",
        "1",
        "1-1/8",
        "1-1/4",
        "1-3/8",
        "1-1/2"
    ];

    let ancor_type = {
        "1/2": {},
        "5/8": {},
        "3/4": {},
        "7/8": {},
        "1": {},
        "1-1/8": {},
        "1-1/4": {},
        "1-3/8": {},
        "1-1/2": {}
    };


    let fillDropData = function (drop_id, arr, value) {

        let div = jQuery("#" + drop_id + " div.menu");
        div.empty();
        for (let i = 0; i < arr.length; i++) {
            div.append('<div class="item" data-value="' + arr[i] + '">' + arr[i] + '</div>');
        }
        jQuery("#" + drop_id).dropdown();
        jQuery("#" + drop_id).dropdown("set selected", value);
    };


    // New point based on rotate
    /*
        CX @ Origin X  
        CY @ Origin Y
        X  @ Point X to be rotated
        Y  @ Point Y to be rotated  
    */
    let rotate = function (CX, CY, X, Y, angle) {
        var rad = angle * Math.PI / 180.0;
        var nx = Math.cos(rad) * (X - CX) - Math.sin(rad) * (Y - CY) + CX;
        var ny = Math.sin(rad) * (X - CX) + Math.cos(rad) * (Y - CY) + CY;
        return [nx, ny];
    };

    let Text = function (set) {
        let loader = new THREE.FontLoader();
        // loader.load('js/text.js', function (font) {
        loader.load('https://cdn.rawgit.com/redwavedesign/ccb20f24e7399f3d741e49fbe23add84/raw/402bcf913c55ad6b12ecfdd20c52e3047ff26ace/bebas_regular.typeface.js', function (font) {
            let textGeo = new THREE.TextGeometry(set.value, {
                font: font,
                size: set.text_size,
                height: 0,
                curveSegments: 0,
                bevelEnabled: false,
                bevelThickness: 0,
                bevelSize: 0,
                bevelOffset: 0,
                bevelSegments: 0
            });
            let textMaterial = new THREE.MeshPhongMaterial({ color: set.color });
            let mesh = new THREE.Mesh(textGeo, textMaterial);
            mesh.position.set(set.coord[0], set.coord[1], set.coord[2]);
            mesh.rotation.x = THREE.Math.degToRad(set.angle_x);
            mesh.rotation.z = THREE.Math.degToRad(set.angle_z);
            mesh.name = set.name;
            set.scene.add(mesh);
            FUNC_3D_INIT.updateMainScene();
        });
    };


    let drawTwoNodeLine = function (set) {
        let material = new THREE.LineDashedMaterial({
            color: set.color,
            dashSize: set.dashSize,
            gapSize: set.gapSize,
            linewidth: 1,
            scale: 1,
        });   // 0x000000
        let geometry = new THREE.Geometry();
        geometry.vertices.push(set.p1);
        geometry.vertices.push(set.p2);
        let mesh = new THREE.Line(geometry, material);
        mesh.computeLineDistances();
        mesh.name = set.name;
        return mesh;
    };


    let updateDIVXYPos = function (coord, id, shift) {

        let renderer = THREE_INIT.options.renderer;
        let camera = THREE_INIT.options.camera;

        let vector = new THREE.Vector3(coord[0], coord[1], coord[2]);
        let canvas = renderer.domElement;
        let annotation = document.getElementById(id);
        vector.project(camera);
        if (vector.x > 1.0 || vector.x < -1.0 || vector.y > 1.0 || vector.y < -1.0 || vector.z > 1.0 || vector.z < -1.0) {
            annotation.style.display = "none";
            return;
        }
        else annotation.style.display = "block";
        vector.x = Math.round((0.5 + vector.x / 2) * (canvas.width / window.devicePixelRatio));
        vector.y = Math.round((0.5 - vector.y / 2) * (canvas.height / window.devicePixelRatio));
        annotation.style.top = `${vector.y + shift}px`;
        annotation.style.left = `${vector.x + shift}px`;
    };

    let distance = function (pointA, pointB) {
		let dx = pointB.x - pointA.x;
		let dy = pointB.y - pointA.y;
		let dz = pointB.z - pointA.z;
		let dist = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2) + Math.pow(dz, 2));
		return dist;
	};


    return {
        fillDropData: fillDropData,
        steel_mat: steel_mat,
        ancor_type: ancor_type,
        concrete_mat: concrete_mat,
        anchor_names: anchor_names,
        bolt_mat: bolt_mat,
        weld_mat: weld_mat,
        Text: Text,
        drawTwoNodeLine: drawTwoNodeLine,
        updateDIVXYPos: updateDIVXYPos,
        distance: distance,
        rotate: rotate
    };

})();