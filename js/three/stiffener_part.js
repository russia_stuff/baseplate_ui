



let STIFFENER_PART = (function () {

    let options = {
        jsx_data: [],
        jsx_data_vert: [],
        weld_data: [],
        weld_shape: [],
        stiff_index:0
    };


    let init = function (set) {

        let group = new THREE.Group();
        group.name = "stiffener";
        options.jsx_data = [];
        options.jsx_data_vert = [];
        options.weld_data = [];
        options.weld_shape = [];

        let length_factor = (TAB_PROJECT_DETAILS.options.units == "imperial") ? 25.4 : 1.0;
        let sec_dims = TAB_STEEL_JSX_DRAWINGS.options.dims;
        let pipe_deg = 0;

        // Shape of section ------------------------------------------------------------------------------------

        let a = parseFloat(jQuery("#stiffener-prop-a").val()) * length_factor;
        let b = parseFloat(jQuery("#stiffener-prop-b").val()) * length_factor;
        let t = parseFloat(jQuery("#stiffener-prop-t").val()) * length_factor;
        let atop = parseFloat(jQuery("#stiffener-prop-atop").val()) * length_factor;
        let btop = parseFloat(jQuery("#stiffener-prop-btop").val()) * length_factor;
        let abot = parseFloat(jQuery("#stiffener-prop-abot").val()) * length_factor;
        let bbot = parseFloat(jQuery("#stiffener-prop-bbot").val()) * length_factor;



        let shape = new THREE.Shape();
        shape.moveTo(0, b);
        shape.lineTo(a - atop, b);
        shape.lineTo(a, b - btop);
        shape.lineTo(a, 0);
        shape.lineTo(abot, 0);
        shape.lineTo(0, bbot);
        shape.lineTo(0, b);

        let extrudeSettings = {
            steps: 1,
            depth: t,
            bevelEnabled: false,
            bevelThickness: 0,
            bevelSize: 0,
            bevelOffset: 0,
            bevelSegments: 0
        };

        if (TAB_STEEL_MEMBERS.data["steel-column-shape"] == "pipe") {

            let num = parseInt(jQuery("#stiffener-position-num").val());
            let deg = parseInt(jQuery("#stiffener-position-deg").val());
            pipe_deg = deg;

            if (num > 0) {
                let angle = 0;
                let step = 360 / num;
                for (let i = 0; i < num; i++) {
                    let pos = COMMON_FUNC.rotate(0, 0, 0.5 * t, 0.5 * sec_dims.D * length_factor, angle);
                    initMesh([pos[0], 0, pos[1]], (90 - angle) - 180);
                    angle += step;
                }
            }
            group.rotation.y = THREE.Math.degToRad(deg);
            set.scene.add(group);
        }
        else if (TAB_STEEL_MEMBERS.data["steel-column-shape"] == "rectangular") {

            let topside = parseInt(jQuery("#stiffener-position-topside").val());
            let botside = parseInt(jQuery("#stiffener-position-botside").val());
            let leftside = parseInt(jQuery("#stiffener-position-leftside").val());
            let rightside = parseInt(jQuery("#stiffener-position-rightside").val());

            if (topside > 0) {
                if (topside == 1) initMesh([-0.5 * t, 0, -0.5 * sec_dims.h], 90);
                else {
                    let start = -0.5 * sec_dims.b * length_factor + sec_dims.r * length_factor + sec_dims.t * length_factor;
                    let step = (sec_dims.b * length_factor - 2 * sec_dims.r * length_factor - 2 * sec_dims.t * length_factor) / (topside - 1);
                    for (let i = 0; i < topside; i++) {
                        if (i == 0) initMesh([start, 0, -0.5 * sec_dims.h * length_factor], 90);
                        else if (i == topside - 1) initMesh([start - t, 0, -0.5 * sec_dims.h * length_factor], 90);
                        else initMesh([start - 0.5 * t, 0, -0.5 * sec_dims.h * length_factor], 90);
                        start += step;
                    }
                }
            }
            if (botside > 0) {
                if (botside == 1) initMesh([0.5 * t, 0, 0.5 * sec_dims.h * length_factor], -90);
                else {
                    let start = -0.5 * sec_dims.b * length_factor + sec_dims.r * length_factor + sec_dims.t * length_factor;
                    let step = (sec_dims.b * length_factor - 2 * sec_dims.r * length_factor - 2 * sec_dims.t * length_factor) / (botside - 1);
                    for (let i = 0; i < botside; i++) {
                        if (i == 0) initMesh([start + t, 0, 0.5 * sec_dims.h * length_factor], -90);
                        else if (i == botside - 1) initMesh([start, 0, 0.5 * sec_dims.h * length_factor], -90);
                        else initMesh([start + 0.5 * t, 0, 0.5 * sec_dims.h * length_factor], -90);
                        start += step;
                    }
                }
            }
            if (leftside > 0) {
                if (leftside == 1) initMesh([-0.5 * sec_dims.b * length_factor, 0, 0.5 * t], 180);
                else {
                    let start = -0.5 * sec_dims.h * length_factor + sec_dims.r * length_factor + sec_dims.t * length_factor;
                    let step = (sec_dims.h * length_factor - 2 * sec_dims.r * length_factor - 2 * sec_dims.t * length_factor) / (leftside - 1);
                    for (let i = 0; i < leftside; i++) {
                        if (i == 0) initMesh([-0.5 * sec_dims.b * length_factor, 0, start + t], 180);
                        else if (i == leftside - 1) initMesh([-0.5 * sec_dims.b * length_factor, 0, start], 180);
                        else initMesh([-0.5 * sec_dims.b * length_factor, 0, start + 0.5 * t], 180);
                        start += step;
                    }
                }
            }
            if (rightside > 0) {
                if (rightside == 1) initMesh([0.5 * sec_dims.b * length_factor, 0, -0.5 * t], 0);
                else {
                    let start = -0.5 * sec_dims.h * length_factor + sec_dims.t * length_factor + sec_dims.r * length_factor;
                    let step = (sec_dims.h * length_factor - 2 * sec_dims.t * length_factor - 2 * sec_dims.r * length_factor) / (rightside - 1);
                    for (let i = 0; i < rightside; i++) {
                        if (i == 0) initMesh([0.5 * sec_dims.b * length_factor, 0, start], 0);
                        else if (i == rightside - 1) initMesh([0.5 * sec_dims.b * length_factor, 0, start - t], 0);
                        else initMesh([0.5 * sec_dims.b * length_factor, 0, start - 0.5 * t], 0);
                        start += step;
                    }
                }
            }
            set.scene.add(group);
        }
        else if (TAB_STEEL_MEMBERS.data["steel-column-shape"] == "i-shape") {

            let toptop = parseInt(jQuery("#stiffener-position-toptop").val());
            let botbot = parseInt(jQuery("#stiffener-position-botbot").val());
            let webleft = parseInt(jQuery("#stiffener-position-webleft").val());
            let webright = parseInt(jQuery("#stiffener-position-webright").val());
            let topleft = jQuery("#stiffener-position-topleft").checkbox("is checked");
            let botleft = jQuery("#stiffener-position-botleft").checkbox("is checked");
            let topright = jQuery("#stiffener-position-topright").checkbox("is checked");
            let botright = jQuery("#stiffener-position-botright").checkbox("is checked");
            let diff = (0.5 * sec_dims.TFt * length_factor - 0.5 * t);

            if (topleft === true) initMesh([-0.5 * sec_dims.TFw * length_factor, 0, (-0.5 * sec_dims.h * length_factor + diff) + t], 180);
            if (botleft === true) initMesh([-0.5 * sec_dims.TFw * length_factor, 0, (0.5 * sec_dims.h * length_factor - diff)], 180);
            if (topright === true) initMesh([0.5 * sec_dims.TFw * length_factor, 0, (-0.5 * sec_dims.h * length_factor + diff)], 0);
            if (botright === true) initMesh([0.5 * sec_dims.TFw * length_factor, 0, (0.5 * sec_dims.h * length_factor - diff) - t], 0);
            if (toptop > 0) {
                if (toptop == 1) initMesh([-0.5 * t, 0, -0.5 * sec_dims.h * length_factor], 90);
                else {
                    let start = -0.5 * sec_dims.TFw * length_factor;
                    let step = sec_dims.TFw * length_factor / (toptop - 1);
                    for (let i = 0; i < toptop; i++) {
                        if (i == 0) initMesh([start, 0, -0.5 * sec_dims.h * length_factor], 90);
                        else if (i == toptop - 1) initMesh([start - t, 0, -0.5 * sec_dims.h * length_factor], 90);
                        else initMesh([start - 0.5 * t, 0, -0.5 * sec_dims.h * length_factor], 90);
                        start += step;
                    }
                }
            }
            if (botbot > 0) {
                if (botbot == 1) initMesh([0.5 * t, 0, 0.5 * sec_dims.h * length_factor], -90);
                else {
                    let start = -0.5 * sec_dims.TFw * length_factor;
                    let step = sec_dims.TFw * length_factor / (botbot - 1);
                    for (let i = 0; i < botbot; i++) {
                        if (i == 0) initMesh([start + t, 0, 0.5 * sec_dims.h * length_factor], -90);
                        else if (i == botbot - 1) initMesh([start, 0, 0.5 * sec_dims.h * length_factor], -90);
                        else initMesh([start + 0.5 * t, 0, 0.5 * sec_dims.h * length_factor], -90);
                        start += step;
                    }
                }
            }
            if (webleft > 0) {
                if (webleft == 1) initMesh([-0.5 * sec_dims.Wt * length_factor, 0, 0.5 * t], 180);
                else {
                    let start = -0.5 * sec_dims.h * length_factor + sec_dims.TFt * length_factor + sec_dims.r * length_factor;
                    let step = (sec_dims.h * length_factor - 2 * sec_dims.TFt * length_factor - 2 * sec_dims.r * length_factor) / (webleft - 1);
                    for (let i = 0; i < webleft; i++) {
                        if (i == 0) initMesh([-0.5 * sec_dims.Wt * length_factor, 0, start + t], 180);
                        else if (i == webleft - 1) initMesh([-0.5 * sec_dims.Wt * length_factor, 0, start], 180);
                        else initMesh([-0.5 * sec_dims.Wt * length_factor, 0, start + 0.5 * t], 180);
                        start += step;
                    }
                }
            }
            if (webright > 0) {
                if (webright == 1) initMesh([0.5 * sec_dims.Wt * length_factor, 0, -0.5 * t], 0);
                else {
                    let start = -0.5 * sec_dims.h * length_factor + sec_dims.TFt * length_factor + sec_dims.r * length_factor;
                    let step = (sec_dims.h * length_factor - 2 * sec_dims.TFt * length_factor - 2 * sec_dims.r * length_factor) / (webright - 1);
                    for (let i = 0; i < webright; i++) {
                        if (i == 0) initMesh([0.5 * sec_dims.Wt * length_factor, 0, start], 0);
                        else if (i == webright - 1) initMesh([0.5 * sec_dims.Wt * length_factor, 0, start - t], 0);
                        else initMesh([0.5 * sec_dims.Wt * length_factor, 0, start - 0.5 * t], 0);
                        start += step;
                    }
                }
            }
            set.scene.add(group);
        }

        function initMesh(pos, rot) {
            // mesh
            let geometry = new THREE.ExtrudeGeometry(shape, extrudeSettings);
            let material = new THREE.MeshBasicMaterial({ color: 0x33cc33, side: THREE.DoubleSide });
            let mesh = new THREE.Mesh(geometry, material);
            mesh.castShadow = true;
		    mesh.receiveShadow = true;
            mesh.position.set(pos[0], pos[1], pos[2]);
            mesh.position.y = 1;
            mesh.rotation.y = THREE.Math.degToRad(rot);
            mesh.stiff_name;
            // edge
            let edge = new THREE.LineSegments(new THREE.EdgesGeometry(geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
            edge.position.set(pos[0], pos[1], pos[2]);
            edge.position.y = 1;
            edge.rotation.y = THREE.Math.degToRad(rot);
            group.add(mesh);
            group.add(edge);

            // JSX
            let data = [[0 + pos[0], abot - pos[2]], [t + pos[0], abot - pos[2]], [t + pos[0], a - pos[2]], [0 + pos[0], a - pos[2]]];
            let data_vert = [[0 + pos[0], 0 - pos[2]], [t + pos[0], 0 - pos[2]], [t + pos[0], t - pos[2]], [0 + pos[0], t - pos[2]]];
            let data_wtl = [[0 + pos[0], 0 - pos[2]], [-t + pos[0], 0 - pos[2]], [0 + pos[0], t - pos[2]]];
            let data_wtr = [[t - pos[0], 0 - pos[2]], [t - t - pos[0], 0 - pos[2]], [t + pos[0], t - pos[2]]];
            let data_wbl = [[0 + pos[0], abot - pos[2]], [-t + pos[0], abot - pos[2]], [-t + pos[0], a - pos[2]], [0 + pos[0], a - pos[2]]];
            let data_wbr = [[t - pos[0], abot - pos[2]], [t - t - pos[0], abot - pos[2]], [t - t - pos[0], a - pos[2]], [t - pos[0], a - pos[2]]];

            let res = [];
            let res_vert = [];
            let res_weld = { wbl: [], wbr: [], wtl: [], wtr: [] };

            for (let i = 0; i < data.length; i++) {
                if (pipe_deg != 0) {
                    let temp = COMMON_FUNC.rotate(data[0][0], 0 - pos[2], data[i][0], data[i][1], rot - 90);
                    res.push(COMMON_FUNC.rotate(0, 0, temp[0], temp[1], pipe_deg));
                    temp = COMMON_FUNC.rotate(data_vert[0][0], 0 - pos[2], data_vert[i][0], data_vert[i][1], rot - 90);
                    res_vert.push(COMMON_FUNC.rotate(0, 0, temp[0], temp[1], pipe_deg));
                    // wbl
                    temp = COMMON_FUNC.rotate(data_wbl[0][0], 0 - pos[2], data_wbl[i][0], data_wbl[i][1], rot - 90);
                    res_weld.wbl.push(COMMON_FUNC.rotate(0, 0, temp[0], temp[1], pipe_deg));
                    // wbr
                    temp = COMMON_FUNC.rotate(data_wbl[0][0], 0 - pos[2], data_wbr[i][0], data_wbr[i][1], rot - 90);
                    res_weld.wbr.push(COMMON_FUNC.rotate(0, 0, temp[0], temp[1], pipe_deg));
                }
                else {
                    res.push(COMMON_FUNC.rotate(data[0][0], 0 - pos[2], data[i][0], data[i][1], rot - 90));
                    res_vert.push(COMMON_FUNC.rotate(data_vert[0][0], 0 - pos[2], data_vert[i][0], data_vert[i][1], rot - 90));
                    // wbl
                    res_weld.wbl.push(COMMON_FUNC.rotate(data_wbl[0][0], 0 - pos[2], data_wbl[i][0], data_wbl[i][1], rot - 90));
                    // wbr
                    res_weld.wbr.push(COMMON_FUNC.rotate(data_wbl[0][0], 0 - pos[2], data_wbr[i][0], data_wbr[i][1], rot - 90));
                }
            }

            // welds top
            for (let i = 0; i < data_wtl.length; i++) {
                if (pipe_deg != 0) {
                    // wbl
                    temp = COMMON_FUNC.rotate(data_wtl[0][0], 0 - pos[2], data_wtl[i][0], data_wtl[i][1], rot - 90);
                    res_weld.wtl.push(COMMON_FUNC.rotate(0, 0, temp[0], temp[1], pipe_deg));
                    // wbr
                    temp = COMMON_FUNC.rotate(data_wtl[0][0], 0 - pos[2], data_wtr[i][0], data_wtr[i][1], rot - 90);
                    res_weld.wtr.push(COMMON_FUNC.rotate(0, 0, temp[0], temp[1], pipe_deg));
                }
                else {
                    // wbl
                    res_weld.wtl.push(COMMON_FUNC.rotate(data_wtl[0][0], 0 - pos[2], data_wtl[i][0], data_wtl[i][1], rot - 90));
                    // wbr
                    res_weld.wtr.push(COMMON_FUNC.rotate(data_wtl[0][0], 0 - pos[2], data_wtr[i][0], data_wtr[i][1], rot - 90));
                }
            }

            options.jsx_data.push(res);
            options.jsx_data_vert.push(res_vert);
            options.weld_data.push(res_weld);
        }

        if (!set.update) TAB_WELDS.initColStiffTable();

    };


    return {
        init: init,
        options: options
    };


})();