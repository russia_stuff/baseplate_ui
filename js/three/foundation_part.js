



let FOUNDATION_PART = (function () {

    let options = {
        height: null
    };


    let init = function (set) {

        let length_factor = (TAB_PROJECT_DETAILS.options.units == "imperial") ? 25.4 : 1.0;

        let width = parseFloat(jQuery("#steel-foundation-width").val()) * length_factor;
        let height = parseFloat(jQuery("#steel-foundation-height").val()) * length_factor;
        let thickness = parseFloat(jQuery("#steel-foundation-thickness").val()) * length_factor;
        let baseplate_thickness = parseFloat(jQuery("#steel-baseplate-thickness").val()) * length_factor;
        let grout_thickness = parseFloat(jQuery("#steel-baseplate-grouting-thickness").val()) * length_factor;
        options.height = thickness + baseplate_thickness + grout_thickness;

        let group = new THREE.Group();
        group.name = "foundation";


        // Foundation --------------------------------------------------------------------------------------------------
        geometry = new THREE.BoxGeometry(width, height, thickness);
        let mesh = new THREE.Mesh(
            geometry,
            new THREE.MeshPhongMaterial({
                color: "#ffffff",
                transparent: true,
                opacity: 0.35
            })
        );
        mesh.castShadow = true;
		mesh.receiveShadow = true;
        let edge = new THREE.LineSegments(
            new THREE.EdgesGeometry(geometry),
            new THREE.LineBasicMaterial({
                color: 'black',
                linewidth: 1
            })
        );
        mesh.rotation.x = -0.5 * Math.PI;
        mesh.position.y = -baseplate_thickness - grout_thickness - 0.51 * thickness;
        edge.rotation.x = -0.5 * Math.PI;
        edge.position.y = -baseplate_thickness - grout_thickness - 0.51 * thickness;
        group.add(mesh);
        group.add(edge);

        set.scene.add(group);
    };


    return {
        init: init,
        options: options
    };


})();