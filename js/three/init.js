



let THREE_INIT = (function () {

    let options = {
        scene: null,
        renderer: null,
        camera: null,
        controls: null
    };

    let update = function () {
        options.renderer.render(options.scene, options.camera);
        LOCAL_AXIS.options.camera.position.copy(options.camera.position);
        LOCAL_AXIS.options.camera.position.sub(options.controls.target); // added by @libe
        LOCAL_AXIS.options.camera.position.setLength(400);
        LOCAL_AXIS.options.camera.lookAt(LOCAL_AXIS.options.scene.position);
        LOCAL_AXIS.options.renderer.render(LOCAL_AXIS.options.scene, LOCAL_AXIS.options.camera);
    };

    let updateCameraPos = function () {
        let size = COLUMN_PART.options.height + FOUNDATION_PART.options.height;
        options.camera.position.set(size, 2 * size, size);
        options.camera.lookAt(new THREE.Vector3(0, 0, 0));
        update();
    };


    let childsRemove = function (names) {
        let temp_childs = [];
        for (let i = 0; i < options.scene.children.length; i++) {
            if (names.indexOf(options.scene.children[i].name) == -1) temp_childs.push(options.scene.children[i]);
        }
        options.scene.children = temp_childs;
    };


    let genColumnPart = function () {
        childsRemove("column");
        COLUMN_PART.init({ scene: options.scene });
        update();
        MAIN_JSX_DRAWINGS.initBase();
    };

    let genbaseplatePart = function () {
        childsRemove("baseplate");
        BASEPLATE_PART.init({ scene: options.scene });
        update();
        MAIN_JSX_DRAWINGS.initBase();
    };

    let genFoundationPart = function () {
        childsRemove("foundation");
        FOUNDATION_PART.init({ scene: options.scene });
        update();
    };

    let genShearLugPart = function (set) {
        childsRemove("shear lug");
        SHEAR_LUG_PART.init({ scene: options.scene, dims: set.dims });
        update();
    };

    let genStiffenerPart = function (set) {
        childsRemove("stiffener");
        STIFFENER_PART.init({ scene: options.scene, update: set.update });
        update();
        MAIN_JSX_DRAWINGS.initBase();
    };

    let genWeldsColumnPart = function () {
        childsRemove("welds_col");
        WELDS_COLUMN_PART.init({ scene: options.scene });
        update();
        MAIN_JSX_DRAWINGS.initBase();
    };

    let genWeldsStiffPart = function () {
        childsRemove("welds_stiff");
        WELDS_STIFFENER_PART.init({ scene: options.scene });
        update();
        MAIN_JSX_DRAWINGS.initBase();
    };

    let genWeldsShearPart = function () {
        childsRemove("welds_shear");
        WELDS_SHEAR_PART.init({ scene: options.scene });
        update();
    };

    let genLoadsPart = function (loads_row) {
        options.controls.removeEventListener('change', LoadsScaleDisplayChange, false);
        childsRemove("loads");
        childsRemove("highlight-sec");
        LOADS_PART.init({ scene: options.scene, loads_row: loads_row, controls: options.controls, camera: options.camera });
        if (LOADS_PART.options.load_vals.length != 0) options.controls.addEventListener('change', LoadsScaleDisplayChange, false);
        update();
    };

    let onWindowResize = function () {
        let canvas_height = jQuery('#right-panel').height();
        let canvas_width = jQuery('#right-panel').width();
        options.camera.aspect = canvas_width / canvas_height;
        options.camera.updateProjectionMatrix();
        options.renderer.setSize(canvas_width, canvas_height);
        options.renderer.render(options.scene, options.camera);
    };


    let getCamDistance = function (point) {
        return COMMON_FUNC.distance(point, options.camera.position);
    };

    let LoadsScaleDisplayChange = function () {
        let groups = options.scene.getObjectByName("load");
        let dist_rat = getCamDistance(LOADS_PART.options.reper) / LOADS_PART.options.cam_center_dist;
        groups.scale.set(dist_rat, dist_rat, dist_rat);
        groups.position.y = -LOADS_PART.options.reper.y * dist_rat + LOADS_PART.options.top_pos;
    };


    let init = function () {

        jQuery("#three_box").empty();
        options.scene = null;
        options.camera = null;
        options.renderer = null;

        options.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true, preserveDrawingBuffer: true });
        let canvas_height = jQuery('#right-panel').height();
        let canvas_width = jQuery('#right-panel').width();

        options.renderer.setSize(canvas_width, canvas_height);
        document.getElementById("three_box").appendChild(options.renderer.domElement);
        options.scene = new THREE.Scene();

        // var gridXZ = new THREE.GridHelper(400, 20, 'red', 'blue');
        // gridXZ.name = 'grid';
        // options.scene.add(gridXZ);

        options.camera = new THREE.PerspectiveCamera(45, canvas_width / canvas_height, 1, 10000);
        options.camera.name = "camera";
        options.renderer.setClearColor(0xF4F6FA);                   // background color

        options.scene.add(new THREE.AmbientLight(0x444444));
        options.camera.add(new THREE.PointLight(0xffffff, 0.8));

        options.controls = new THREE.OrbitControls(options.camera, options.renderer.domElement);
        options.controls.addEventListener('change', function () { update(); });

        options.controls.mouseButtons.LEFT = 2;

        options.camera.position.set(0, 0, 0);
        options.camera.lookAt(new THREE.Vector3(0, 0, 0));
        options.scene.add(options.camera);

        window.addEventListener('resize', onWindowResize, false);


        update();

    };




    return {
        init: init,
        options: options,
        genColumnPart: genColumnPart,
        genbaseplatePart: genbaseplatePart,
        genFoundationPart: genFoundationPart,
        genShearLugPart: genShearLugPart,
        genStiffenerPart: genStiffenerPart,
        genWeldsColumnPart: genWeldsColumnPart,
        genWeldsStiffPart: genWeldsStiffPart,
        genWeldsShearPart: genWeldsShearPart,
        genLoadsPart: genLoadsPart,
        updateCameraPos: updateCameraPos,
        update: update,
        childsRemove: childsRemove,
        onWindowResize: onWindowResize
    };


})();