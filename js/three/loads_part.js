

let LOADS_PART = (function () {

    let options = {
        load_vals: [],
        div: null,
        load_spinner: 1,
        getCamDistance: null
    };


    function BendingLoadDraw(set) {

        let cyl_rad = 0.1 * set.sec_size;
        let cyl_height = 0.2 * set.sec_size;
        let rad_1 = 0.25 * set.sec_size;
        let rad_2 = 0.2 * set.sec_size;
        let stick_rad = 0.025 * set.sec_size;
        let stick_height = 0.4 * set.sec_size;

        // arrow part
        let geometry = new THREE.CylinderGeometry(cyl_rad, 0, cyl_height, 15);
        let material = new THREE.MeshPhongMaterial({ color: 0xffff00, opacity: 0.5, transperent: true });
        let mesh = new THREE.Mesh(geometry, material);
        let edge = new THREE.LineSegments(new THREE.EdgesGeometry(geometry), new THREE.LineBasicMaterial({ color: 'black', linewidth: 1 }));
        mesh.position.x = 1.1 * rad_1;
        edge.position.x = 1.1 * rad_1;
        mesh.rotation.z = THREE.Math.degToRad(22.5);
        edge.rotation.z = THREE.Math.degToRad(22.5);
        set.group.add(mesh);
        set.group.add(edge);
        // Stick part
        geometry = new THREE.TorusGeometry(rad_1, stick_rad, 15, 15, THREE.Math.degToRad(135));
        material = new THREE.MeshBasicMaterial({ color: 0xffff00, opacity: 0.5, transperent: true });
        mesh = new THREE.Mesh(geometry, material);
        edge = new THREE.LineSegments(new THREE.EdgesGeometry(geometry), new THREE.LineBasicMaterial({ color: 'black', linewidth: 1 }));
        set.group.add(mesh);
        set.group.add(edge);

        set.group.rotation.z = THREE.Math.degToRad(22.5);
        set.group.position.y = set.top_pos;

        if (set.name == "My") {
            set.group.position.z = 0.35 * set.sec_size;
            set.group.rotation.y = THREE.Math.degToRad(set.angle_y);
            options.load_vals.push([set.group.position.x, set.group.position.y, set.group.position.z]);
            options.div.innerHTML += '<p id="load_' + options.load_spinner + '" class="load-label" style="position: absolute; top: 0px; left: 0px;">M<sub>y</sub>: ' + set.value.toFixed(3) + ' ' + TAB_PROJECT_DETAILS.options.bending_f_units + '</p>';
            COMMON_FUNC.updateDIVXYPos([set.group.position.x, set.group.position.y, set.group.position.z], 'load_' + options.load_spinner, 3);
            options.load_spinner++;
        }
        else if (set.name == "Mx") {
            set.group.position.x = 0.35 * set.sec_size;
            set.group.rotation.y = THREE.Math.degToRad(set.angle_y + 90);
            options.load_vals.push([set.group.position.x, set.group.position.y, set.group.position.z]);
            options.div.innerHTML += '<p id="load_' + options.load_spinner + '" class="load-label" style="position: absolute; top: 0px; left: 0px;">M<sub>z</sub>: ' + set.value.toFixed(3) + ' ' + TAB_PROJECT_DETAILS.options.bending_f_units + '</p>';
            COMMON_FUNC.updateDIVXYPos([set.group.position.x, set.group.position.y, set.group.position.z], 'load_' + options.load_spinner, 3);
            options.load_spinner++;
        }
        // else if (set.name == "T") {
        //     set.group.rotation.x = THREE.Math.degToRad(-90);
        //     set.group.rotation.y = THREE.Math.degToRad(set.angle_y);
        //     options.load_vals.push([set.group.position.x, set.group.position.y, set.group.position.z]);
        //     options.div.innerHTML += '<p id="load_' + options.load_spinner + '" class="load-label" style="position: absolute; top: 0px; left: 0px;">T<sub></sub>: ' + set.value.toFixed(3) + ' ' + TAB_PROJECT_DETAILS.options.bending_f_units + '</p>';
        //     COMMON_FUNC.updateDIVXYPos([set.group.position.x, set.group.position.y, set.group.position.z], 'load_' + options.load_spinner, 3);
        //     options.load_spinner++;
        // }
    }


    function AxialLoadDraw(set) {

        let cyl_rad = 0.1 * set.sec_size;
        let cyl_height = 0.2 * set.sec_size;
        let stick_rad = 0.025 * set.sec_size;
        let stick_height = 0.4 * set.sec_size;

        // arrow part
        let geometry = new THREE.CylinderGeometry(cyl_rad, 0, cyl_height, 15);
        let material = new THREE.MeshPhongMaterial({ color: 0xffff00, opacity: 0.5, transperent: true });
        let mesh = new THREE.Mesh(geometry, material);
        let edge = new THREE.LineSegments(new THREE.EdgesGeometry(geometry), new THREE.LineBasicMaterial({ color: 'black', linewidth: 1 }));
        if (set.angle_x != 0) {
            mesh.position.y = -stick_height - 0.5 * cyl_height;
            edge.position.y = -stick_height - 0.5 * cyl_height;
        }
        else {
            mesh.position.y = 0.5 * cyl_height;
            edge.position.y = 0.5 * cyl_height;
        }
        set.group.add(mesh);
        set.group.add(edge);
        // Stick part
        geometry = new THREE.CylinderGeometry(stick_rad, stick_rad, stick_height, 15);
        mesh = new THREE.Mesh(geometry, material);
        edge = new THREE.LineSegments(new THREE.EdgesGeometry(geometry), new THREE.LineBasicMaterial({ color: 'black', linewidth: 1 }));
        if (set.angle_x != 0) {
            mesh.position.y = -stick_height + 0.5 * stick_height;
            edge.position.y = -stick_height + 0.5 * stick_height;
        }
        else {
            mesh.position.y = cyl_height + 0.5 * stick_height;
            edge.position.y = cyl_height + 0.5 * stick_height;
        }
        set.group.add(mesh);
        set.group.add(edge);

        if (set.name == "Nx") {
            set.group.rotation.x = THREE.Math.degToRad(set.angle_x);
            set.group.position.y = set.top_pos // set.top_pos + cyl_height + stick_height;
            options.load_vals.push([0, set.top_pos, 0]);
            options.div.innerHTML += '<p id="load_' + options.load_spinner + '" class="load-label" style="position: absolute; top: 0px; left: 0px;">N<sub>x</sub>: ' + set.value.toFixed(3) + ' ' + TAB_PROJECT_DETAILS.options.axial_f_units + '</p>';
            COMMON_FUNC.updateDIVXYPos([0, set.top_pos, 0], 'load_' + options.load_spinner, 3);
            options.load_spinner++;
        }
        else if (set.name == "Vy") {
            set.group.rotation.x = THREE.Math.degToRad(set.angle_x);
            set.group.position.y = set.top_pos;
            set.group.position.z = (set.angle_x > 0) ? -0.5 * set.sec_size : 0.5 * set.sec_size;
            options.load_vals.push([0, set.group.position.y, set.group.position.z]);
            options.div.innerHTML += '<p id="load_' + options.load_spinner + '" class="load-label" style="position: absolute; top: 0px; left: 0px;">V<sub>y</sub>: ' + set.value.toFixed(3) + ' ' + TAB_PROJECT_DETAILS.options.axial_f_units + '</p>';
            COMMON_FUNC.updateDIVXYPos([0, set.group.position.y, set.group.position.z], 'load_' + options.load_spinner, 3);
            options.load_spinner++;
        }
        else if (set.name == "Vx") {
            set.group.rotation.z = THREE.Math.degToRad(set.angle_z);
            set.group.position.y = set.top_pos;
            set.group.position.x = (set.angle_z > 0) ? -0.5 * set.sec_size : 0.5 * set.sec_size;
            options.load_vals.push([set.group.position.x, set.group.position.y, 0]);
            options.div.innerHTML += '<p id="load_' + options.load_spinner + '" class="load-label" style="position: absolute; top: 0px; left: 0px;">V<sub>x</sub>: ' + set.value.toFixed(3) + ' ' + TAB_PROJECT_DETAILS.options.axial_f_units + '</p>';
            COMMON_FUNC.updateDIVXYPos([set.group.position.x, set.group.position.y, 0], 'load_' + options.load_spinner, 3);
            options.load_spinner++;
        }
    }

    

    let addLabelDisplayChange = function () {
        for (let i = 0; i < options.load_vals.length; i++) {
            let dist_rat = options.getCamDistance({x: options.load_vals[i][0], y: options.load_vals[i][1], z: options.load_vals[i][2]}) / options.cam_center_dist;
            COMMON_FUNC.updateDIVXYPos([options.load_vals[i][0] * dist_rat, options.load_vals[i][1], options.load_vals[i][2] * dist_rat], 'load_' + (i + 1), 3);
        }
    };


    let init = function (set) {

        options.getCamDistance = function (point) {
            let camera = set.camera;
            return COMMON_FUNC.distance(point, camera.position);
        };

        set.controls.removeEventListener('change', LOADS_PART.addLabelDisplayChange, false);
        options.div = document.getElementById('load-labels');
        options.div.innerHTML = "";
        options.load_spinner = 1.0;
        options.load_vals = [];

        let length_factor = (TAB_PROJECT_DETAILS.options.units == "imperial") ? 25.4 : 1.0;

        let width = parseFloat(jQuery("#steel-baseplate-width").val()) * length_factor;
        let height = parseFloat(jQuery("#steel-baseplate-height").val()) * length_factor;

        let Nx = parseFloat(set.loads_row.Nx);
        let Mx = parseFloat(set.loads_row.Mx);
        let My = parseFloat(set.loads_row.My);
        let Vx = parseFloat(set.loads_row.Vx);
        let Vy = parseFloat(set.loads_row.Vy);
        // let T = parseFloat(set.loads_row.T);

        let height_factor = 1.6;
        let sec_size = Math.max(width, height); // TAB_STEEL_JSX_DRAWINGS.options.size;
        let top_pos = height_factor * TAB_STEEL_JSX_DRAWINGS.options.size * length_factor;

        options.cam_center_dist = options.getCamDistance({ x: 0, y: top_pos, z: 0 });
        options.cam_test_dist = options.getCamDistance({ x: 0, y: top_pos + 100, z: 0 });
        options.reper = { x: 0, y: top_pos, z: 0 };
        options.top_pos = top_pos;
        let update_rat = options.cam_center_dist / sec_size;

        // Axis
        let axis_x = COMMON_FUNC.drawTwoNodeLine({
            color: 'black',
            name: 'load',
            p1: new THREE.Vector3(-0.4 * 0.25 * update_rat * sec_size, top_pos, 0),
            p2: new THREE.Vector3(0.4 * 0.25 * update_rat * sec_size, top_pos, 0),
            dashSize: 0.05 * 0.25 * update_rat * sec_size,
            gapSize: 0.05 * 0.25 * update_rat * sec_size
        });
        axis_x.updateScale = [0, top_pos, 0];
        let axis_y = COMMON_FUNC.drawTwoNodeLine({
            color: 'black',
            name: 'load',
            p1: new THREE.Vector3(0, top_pos, -0.4 * 0.25 * update_rat * sec_size),
            p2: new THREE.Vector3(0, top_pos, 0.4 * 0.25 * update_rat * sec_size),
            dashSize: 0.05 * 0.25 * update_rat * sec_size,
            gapSize: 0.05 * 0.25 * update_rat * sec_size
        });
        axis_y.updateScale = [0, top_pos, 0];
        //



        let group_Nx = new THREE.Group();
        let group_Vy = new THREE.Group();
        let group_Vx = new THREE.Group();
        let group_My = new THREE.Group();
        let group_Mx = new THREE.Group();
        // let group_T = new THREE.Group();

        let group = new THREE.Group();
        group.name = "load";

        if (Nx != 0) {
            AxialLoadDraw({ group: group_Nx, sec_size: 0.15 * sec_size * update_rat, top_pos: top_pos, angle_x: (Nx > 0) ? 0 : 180, angle_z: 0, name: "Nx", value: Nx });
            group.add(group_Nx);
        }   // angle_x: 0 or 180
        if (Vy != 0) {
            AxialLoadDraw({ group: group_Vy, sec_size: 0.15 * sec_size * update_rat, top_pos: top_pos, angle_x: (Vy > 0) ? 90 : -90, angle_z: 0, name: "Vy", value: Vy });
            group.add(group_Vy);
        }                              // angle_x: 90 positive sign, or -90
        if (Vx != 0) {
            AxialLoadDraw({ group: group_Vx, sec_size: 0.15 * sec_size * update_rat, top_pos: top_pos, angle_x: 0, angle_z: (Vx > 0) ? 90 : -90, name: "Vx", value: Vx });
            group.add(group_Vx);
        }                               // angle_z: 90 positive sign, or -90
        if (My != 0) {
            BendingLoadDraw({ group: group_My, sec_size: 0.15 * sec_size * update_rat, top_pos: top_pos, angle_y: (My > 0) ? 0 : 180, name: "My", value: My });
            group.add(group_My);
        }                                         // angle_y: 0 positiov or 180 negative
        if (Mx != 0) {
            BendingLoadDraw({ group: group_Mx, sec_size: 0.15 * sec_size * update_rat, top_pos: top_pos, angle_y: (Mx > 0) ? 0 : 180, name: "Mx", value: Mx });
            group.add(group_Mx);
        }                                       // angle_y: 0 positiov or 180 negative
        // if (T != 0) {
        //     BendingLoadDraw({ group: group_T, sec_size: 0.15 * sec_size * update_rat, top_pos: top_pos, angle_y: (T > 0) ? 0 : 180, name: "T", value: T });
        //     group.add(group_T);
        // }                                            // angle_y: 0 positiov or 180 negative

        group.add(axis_x);
        group.add(axis_y);

        // Section view
        let group_highlight = new THREE.Group();
        group_highlight.name = "highlight-sec";
        let extrudeSettings = {steps: 1,depth: 0,bevelEnabled: false,bevelThickness: 0,bevelSize: 0,bevelOffset: 0,bevelSegments: 0};
        let geometry = new THREE.ExtrudeGeometry(COLUMN_PART.options.shape, extrudeSettings);
        let material = new THREE.MeshBasicMaterial({ color: 0xffff00, wireframe: false, side: THREE.DoubleSide });
        let mesh = new THREE.Mesh(geometry, material);
        mesh.position.y = COLUMN_PART.options.height + 2;
        // mesh.position.y = top_pos;
        mesh.rotation.x = -0.5 * Math.PI;
        group_highlight.add(mesh);
        // 


        set.scene.add(group);
        set.scene.add(group_highlight);

        set.controls.addEventListener('change', addLabelDisplayChange, false);

    };


    return {
        options: options,
        addLabelDisplayChange: addLabelDisplayChange,
        init: init
    };

})();