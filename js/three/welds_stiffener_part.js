



let WELDS_STIFFENER_PART = (function () {

    let options = {
        jsx_data : []
    };


    function drawWeldShape(w, g, h, up) {

        let shape = new THREE.Shape();
        shape.moveTo(w[0][0], w[0][1]);
        shape.lineTo(w[1][0], w[1][1]);
        shape.lineTo(w[2][0], w[2][1]);
        if (w.length == 4) shape.lineTo(w[3][0], w[3][1]);

        let extrudeSettings = {
            steps: 1,
            depth: h,
            bevelEnabled: false,
            bevelThickness: 0,
            bevelSize: 0,
            bevelOffset: 0,
            bevelSegments: 0
        };
        let geometry = new THREE.ExtrudeGeometry(shape, extrudeSettings);
        let material = new THREE.MeshBasicMaterial({ color: "red", side: THREE.DoubleSide });
        let mesh = new THREE.Mesh(geometry, material);
        mesh.castShadow = true;
		mesh.receiveShadow = true;
        mesh.rotation.x = -0.5 * Math.PI;
        mesh.position.y = up;

        if (w.length == 4) {
            mesh.geometry.vertices[4].z = 0;
            mesh.geometry.vertices[7].z = 0;
        }

        let edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
        edge.rotation.x = -0.5 * Math.PI;
        edge.position.y = up;

        g.add(mesh);
        g.add(edge);
    }

    let init = function (set) {

        let group = new THREE.Group();
        group.name = "welds_stiff";
        let is_add = false;
        options.jsx_data = [];

        let lf = (TAB_PROJECT_DETAILS.options.units == "metric") ? 1.0 : 25.4;
        let data = STIFFENER_PART.options;
        let table = TAB_WELDS.data.input_table_stiff;

        if (data.jsx_data.length > 0) {
            is_add = true;

            let b = parseFloat(jQuery("#stiffener-prop-b").val()) * lf;
            let bbot = parseFloat(jQuery("#stiffener-prop-bbot").val()) * lf;


            for (let i = 0; i < data.weld_data.length; i++) {

                let t = parseFloat(table[i][3]) * lf * 1.4142;

                if (table[i][2] == "left") {

                    // left bot
                    let delta_1_x = data.jsx_data[i][0][0] - data.jsx_data[i][1][0];
                    let delta_1_y = data.jsx_data[i][0][1] - data.jsx_data[i][1][1];
                    let delta_2_x = data.jsx_data[i][3][0] - data.jsx_data[i][2][0];
                    let delta_2_y = data.jsx_data[i][3][1] - data.jsx_data[i][2][1];
                    let weld_width = Math.sqrt(Math.pow(delta_1_x, 2) + Math.pow(delta_1_y, 2));
                    let factor = t / weld_width;
                    let poly = [data.jsx_data[i][0], [data.jsx_data[i][0][0] + delta_1_x * factor, data.jsx_data[i][0][1] + delta_1_y * factor], [data.jsx_data[i][3][0] + delta_2_x * factor, data.jsx_data[i][3][1] + delta_2_y * factor], data.jsx_data[i][3]];
                    options.jsx_data.push(poly);
                    drawWeldShape(poly, group, t, 1);

                    // left top
                    let delta_01_x = data.jsx_data_vert[i][0][0] - data.jsx_data_vert[i][1][0];
                    let delta_01_y = data.jsx_data_vert[i][0][1] - data.jsx_data_vert[i][1][1];
                    let delta_03_x = data.jsx_data_vert[i][0][0] - data.jsx_data_vert[i][3][0];
                    let delta_03_y = data.jsx_data_vert[i][0][1] - data.jsx_data_vert[i][3][1];
                    weld_width = Math.sqrt(Math.pow(delta_01_x, 2) + Math.pow(delta_01_y, 2));
                    factor = t / weld_width;
                    poly = [
                        data.jsx_data_vert[i][0],
                        [data.jsx_data_vert[i][0][0] + delta_01_x * factor, data.jsx_data_vert[i][0][1] + delta_01_y * factor],
                        [data.jsx_data_vert[i][0][0] - delta_03_x * factor, data.jsx_data_vert[i][0][1] - delta_03_y * factor],
                    ];
                    drawWeldShape(poly, group, b - bbot, 1 + bbot);

                }
                else if (table[i][2] == "right") {

                    // right bot
                    let delta_1_x = data.jsx_data[i][1][0] - data.jsx_data[i][0][0];
                    let delta_1_y = data.jsx_data[i][1][1] - data.jsx_data[i][0][1];
                    let delta_2_x = data.jsx_data[i][2][0] - data.jsx_data[i][3][0];
                    let delta_2_y = data.jsx_data[i][2][1] - data.jsx_data[i][3][1];
                    let weld_width = Math.sqrt(Math.pow(delta_1_x, 2) + Math.pow(delta_1_y, 2));
                    let factor = t / weld_width;
                    let poly = [data.jsx_data[i][1], [data.jsx_data[i][1][0] + delta_1_x * factor, data.jsx_data[i][1][1] + delta_1_y * factor], [data.jsx_data[i][2][0] + delta_2_x * factor, data.jsx_data[i][2][1] + delta_2_y * factor], data.jsx_data[i][2]];
                    options.jsx_data.push(poly);
                    drawWeldShape(poly, group, t, 1);

                    // right top
                    let delta_10_x = data.jsx_data_vert[i][1][0] - data.jsx_data_vert[i][0][0];
                    let delta_10_y = data.jsx_data_vert[i][1][1] - data.jsx_data_vert[i][0][1];
                    let delta_12_x = data.jsx_data_vert[i][1][0] - data.jsx_data_vert[i][2][0];
                    let delta_12_y = data.jsx_data_vert[i][1][1] - data.jsx_data_vert[i][2][1];
                    weld_width = Math.sqrt(Math.pow(delta_10_x, 2) + Math.pow(delta_10_y, 2));
                    factor = t / weld_width;
                    poly = [
                        data.jsx_data_vert[i][1],
                        [data.jsx_data_vert[i][1][0] + delta_10_x * factor, data.jsx_data_vert[i][1][1] + delta_10_y * factor],
                        [data.jsx_data_vert[i][1][0] - delta_12_x * factor, data.jsx_data_vert[i][1][1] - delta_12_y * factor],
                    ];
                    drawWeldShape(poly, group, b - bbot, 1 + bbot);

                }
                else if (table[i][2] == "both") {

                    // left bot
                    let delta_1_x = data.jsx_data[i][0][0] - data.jsx_data[i][1][0];
                    let delta_1_y = data.jsx_data[i][0][1] - data.jsx_data[i][1][1];
                    let delta_2_x = data.jsx_data[i][3][0] - data.jsx_data[i][2][0];
                    let delta_2_y = data.jsx_data[i][3][1] - data.jsx_data[i][2][1];
                    let weld_width = Math.sqrt(Math.pow(delta_1_x, 2) + Math.pow(delta_1_y, 2));
                    let factor = t / weld_width;
                    let poly = [data.jsx_data[i][0], [data.jsx_data[i][0][0] + delta_1_x * factor, data.jsx_data[i][0][1] + delta_1_y * factor], [data.jsx_data[i][3][0] + delta_2_x * factor, data.jsx_data[i][3][1] + delta_2_y * factor], data.jsx_data[i][3]];
                    options.jsx_data.push(poly);
                    drawWeldShape(poly, group, t, 1);

                    // right bot
                    delta_1_x = data.jsx_data[i][1][0] - data.jsx_data[i][0][0];
                    delta_1_y = data.jsx_data[i][1][1] - data.jsx_data[i][0][1];
                    delta_2_x = data.jsx_data[i][2][0] - data.jsx_data[i][3][0];
                    delta_2_y = data.jsx_data[i][2][1] - data.jsx_data[i][3][1];
                    poly = [data.jsx_data[i][1], [data.jsx_data[i][1][0] + delta_1_x * factor, data.jsx_data[i][1][1] + delta_1_y * factor], [data.jsx_data[i][2][0] + delta_2_x * factor, data.jsx_data[i][2][1] + delta_2_y * factor], data.jsx_data[i][2]];
                    options.jsx_data.push(poly);
                    drawWeldShape(poly, group, t, 1);

                    // left top
                    let delta_01_x = data.jsx_data_vert[i][0][0] - data.jsx_data_vert[i][1][0];
                    let delta_01_y = data.jsx_data_vert[i][0][1] - data.jsx_data_vert[i][1][1];
                    let delta_03_x = data.jsx_data_vert[i][0][0] - data.jsx_data_vert[i][3][0];
                    let delta_03_y = data.jsx_data_vert[i][0][1] - data.jsx_data_vert[i][3][1];
                    weld_width = Math.sqrt(Math.pow(delta_01_x, 2) + Math.pow(delta_01_y, 2));
                    factor = t / weld_width;
                    poly = [
                        data.jsx_data_vert[i][0],
                        [data.jsx_data_vert[i][0][0] + delta_01_x * factor, data.jsx_data_vert[i][0][1] + delta_01_y * factor],
                        [data.jsx_data_vert[i][0][0] - delta_03_x * factor, data.jsx_data_vert[i][0][1] - delta_03_y * factor],
                    ];
                    drawWeldShape(poly, group, b - bbot, 1 + bbot);

                    // right top
                    let delta_10_x = data.jsx_data_vert[i][1][0] - data.jsx_data_vert[i][0][0];
                    let delta_10_y = data.jsx_data_vert[i][1][1] - data.jsx_data_vert[i][0][1];
                    let delta_12_x = data.jsx_data_vert[i][1][0] - data.jsx_data_vert[i][2][0];
                    let delta_12_y = data.jsx_data_vert[i][1][1] - data.jsx_data_vert[i][2][1];
                    weld_width = Math.sqrt(Math.pow(delta_10_x, 2) + Math.pow(delta_10_y, 2));
                    factor = t / weld_width;
                    poly = [
                        data.jsx_data_vert[i][1],
                        [data.jsx_data_vert[i][1][0] + delta_10_x * factor, data.jsx_data_vert[i][1][1] + delta_10_y * factor],
                        [data.jsx_data_vert[i][1][0] - delta_12_x * factor, data.jsx_data_vert[i][1][1] - delta_12_y * factor],
                    ];
                    drawWeldShape(poly, group, b - bbot, 1 + bbot);

                }

            }
        }



        if (is_add) set.scene.add(group);

    };


    return {
        init: init,
        options: options
    };


})();