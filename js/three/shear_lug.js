



let SHEAR_LUG_PART = (function () {

    let options = {
        height: null
    };


    let init = function (set) {

        let group = new THREE.Group();
        group.name = "shear lug";

        let length_factor = (TAB_PROJECT_DETAILS.options.units == "imperial") ? 25.4 : 1.0;
        let thickness = parseFloat(jQuery("#steel-baseplate-thickness").val()) * length_factor;
        let angle = parseFloat(jQuery("#steel-shear-lug-rotation").dropdown("get value"));
        let sec = jQuery("#steel-shear-lug-shape").dropdown("get value");

        // Shape of section ------------------------------------------------------------------------------------
        let geometry = null;
        let material = null;
        let mesh = null;
        let edge = null;

        if (sec == "i-shape") {
            let shape_points = TAB_STEEL_JSX_DRAWINGS.getISecPoints(set.dims);
            TAB_STEEL_JSX_DRAWINGS.options.shear_dims = set.dims;
            let shape = new THREE.Shape();
            for (let i = 0; i < shape_points.length; i++) {
                let points = shape_points[i];
                if (i == 0) shape.moveTo(points[0] * length_factor, points[1] * length_factor);
                else shape.lineTo(points[0] * length_factor, points[1] * length_factor);
            }
            shape.lineTo(shape_points[0][0] * length_factor, shape_points[0][1] * length_factor);

            let extrudeSettings = {
                steps: 1,
                depth: parseFloat(jQuery("#steel-shear-lug-length").val()) * length_factor,
                bevelEnabled: false,
                bevelThickness: 0,
                bevelSize: 0,
                bevelOffset: 0,
                bevelSegments: 0
            };
            geometry = new THREE.ExtrudeGeometry(shape, extrudeSettings);
            material = new THREE.MeshBasicMaterial({ color: "#00B6FF", side: THREE.DoubleSide });
            mesh = new THREE.Mesh(geometry, material);
            mesh.rotation.x = 0.5 * Math.PI;
            mesh.rotation.z = THREE.Math.degToRad(angle);
            mesh.position.y = -1.05 * thickness;

            // edge
            edge = new THREE.LineSegments(
                new THREE.EdgesGeometry(geometry),
                new THREE.LineBasicMaterial({
                    color: '#404040',
                    linewidth: 1
                })
            );
            // edge.position.set(mesh.x, mesh.y, mesh.z);
            edge.rotation.x = 0.5 * Math.PI;
            edge.rotation.z = THREE.Math.degToRad(angle);
            edge.position.y = -1.05 * thickness;
            
            PANEL_INFO.options.info_shear_name = TAB_STEEL_MEMBERS.data["steel-shear-lug-profile"];
            PANEL_INFO.init();
        }
        else if (sec == "plate") {
            let plate_width = parseFloat(jQuery("#steel-shear-lug-width").val()) * length_factor;
            let plate_height = parseFloat(jQuery("#steel-shear-lug-length").val()) * length_factor;
            let plate_thickness = parseFloat(jQuery("#steel-shear-lug-thickness").val()) * length_factor;
            geometry = new THREE.BoxGeometry(plate_width, plate_height, plate_thickness);
            material = new THREE.MeshBasicMaterial({ color: "#00B6FF", side: THREE.DoubleSide });
            mesh = new THREE.Mesh(geometry, material);
            mesh.position.y = -0.5 * plate_height -1.05 * thickness;
            mesh.rotation.y = THREE.Math.degToRad(angle);
            // edge
            edge = new THREE.LineSegments(
                new THREE.EdgesGeometry(geometry),
                new THREE.LineBasicMaterial({
                    color: '#404040',
                    linewidth: 1
                })
            );
            edge.position.y = -0.5 * plate_height -1.05 * thickness;
            edge.rotation.y = THREE.Math.degToRad(angle);

            PANEL_INFO.options.info_shear_name = "plate";
            PANEL_INFO.init();
        }

        group.add(mesh);
        group.add(edge);
        set.scene.add(group);
        THREE_INIT.genWeldsShearPart();
    };


    return {
        init: init,
        options: options
    };


})();