



let WELDS_SHEAR_PART = (function () {

    let options = {};

    function drawWeldShape(weld, dir) {

        let shape = new THREE.Shape([new THREE.Vector2(weld[0][0], weld[0][1]), new THREE.Vector2(weld[1][0], weld[1][1]), new THREE.Vector2(weld[2][0], weld[2][1])]);
        let Spline = new THREE.CatmullRomCurve3([new THREE.Vector3(dir[0][0], dir[0][1], dir[0][2]), new THREE.Vector3(dir[1][0], dir[1][1], dir[1][2])]);
        let extrudeSettings = { steps: 1, bevelEnabled: false, extrudePath: Spline };
        let geometry = new THREE.ExtrudeBufferGeometry(shape, extrudeSettings);
        let material = new THREE.MeshBasicMaterial({ color: "red", wireframe: false });
        let mesh = new THREE.Mesh(geometry, material);
        return mesh;
    }


    let init = function (set) {

        let group = new THREE.Group();
        group.name = "welds_shear";
        let is_add = false;

        let factor = (TAB_PROJECT_DETAILS.options.units == "metric") ? 1.0 : 25.4;
        let dims = TAB_STEEL_JSX_DRAWINGS.options.shear_dims;
        let pos_y = -1 * parseFloat(TAB_STEEL_MEMBERS.data["steel-baseplate-thickness"]) * factor;

        if (TAB_STEEL_MEMBERS.data["steel-shear-lug-shape"] == "i-shape") {

            let input_data = TAB_WELDS.data.input_table_shear;
            let deg = parseFloat(TAB_STEEL_MEMBERS.data["steel-shear-lug-rotation"]);

            let TFw = dims.TFw * factor;
            let r = dims.r * factor;
            let TFt = dims.TFt * factor;
            let Wt = dims.Wt * factor;
            let h = dims.h * factor;

            // Top flange -----------------------------------------------------------------------------------------------------------------------------------------------
            let data = input_data[0];
            if (data[2] == "both") {
                // top
                let t = parseFloat(data[3]) * factor * 1.4142;
                let mesh = drawWeldShape(
                    [[0, 0], [t, 0], [0, -t]],
                    [[-0.5 * TFw, pos_y - 1, -0.5 * h], [0.5 * TFw, pos_y - 1, -0.5 * h]]
                );
                let edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                mesh.rotation.y = THREE.Math.degToRad(deg);
                edge.rotation.y = THREE.Math.degToRad(deg);
                group.add(mesh);
                group.add(edge);
                // left 
                mesh = drawWeldShape(
                    [[0, 0], [-t, 0], [0, -t]],
                    [[-0.5 * TFw, pos_y - 1, -0.5 * h + TFt], [-0.5 * Wt - r, pos_y - 1, -0.5 * h + TFt]]
                );
                edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                mesh.rotation.y = THREE.Math.degToRad(deg);
                edge.rotation.y = THREE.Math.degToRad(deg);
                group.add(mesh);
                group.add(edge);
                // right 
                mesh = drawWeldShape(
                    [[0, 0], [-t, 0], [0, -t]],
                    [[0.5 * Wt + r, pos_y - 1, -0.5 * h + TFt], [0.5 * TFw, pos_y - 1, -0.5 * h + TFt]]
                );
                edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                mesh.rotation.y = THREE.Math.degToRad(deg);
                edge.rotation.y = THREE.Math.degToRad(deg);
                group.add(mesh);
                group.add(edge);
                is_add = true;
            }
            else if (data[2] == "left") {
                let t = parseFloat(data[3]) * factor * 1.4142;
                let mesh = drawWeldShape(
                    [[0, 0], [t, 0], [0, -t]],
                    [[-0.5 * TFw, pos_y - 1, -0.5 * h], [0.5 * TFw, pos_y - 1, -0.5 * h]]
                );
                let edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                mesh.rotation.y = THREE.Math.degToRad(deg);
                edge.rotation.y = THREE.Math.degToRad(deg);
                group.add(mesh);
                group.add(edge);
                is_add = true;
            }
            else if (data[2] == "right") {
                let t = parseFloat(data[3]) * factor * 1.4142;
                // left 
                mesh = drawWeldShape(
                    [[0, 0], [-t, 0], [0, -t]],
                    [[-0.5 * TFw, pos_y - 1, -0.5 * h + TFt], [-0.5 * Wt - r, pos_y - 1, -0.5 * h + TFt]]
                );
                edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                mesh.rotation.y = THREE.Math.degToRad(deg);
                edge.rotation.y = THREE.Math.degToRad(deg);
                group.add(mesh);
                group.add(edge);
                // right 
                mesh = drawWeldShape(
                    [[0, 0], [-t, 0], [0, -t]],
                    [[0.5 * Wt + r, pos_y - 1, -0.5 * h + TFt], [0.5 * TFw, pos_y - 1, -0.5 * h + TFt]]
                );
                edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                mesh.rotation.y = THREE.Math.degToRad(deg);
                edge.rotation.y = THREE.Math.degToRad(deg);
                group.add(mesh);
                group.add(edge);
                is_add = true;
            }

            // Bottom flange ----------------------------------------------------------------------------------------------------------------------------------------
            data = input_data[1];
            if (data[2] == "both") {
                // top
                let t = parseFloat(data[3]) * factor * 1.4142;
                let mesh = drawWeldShape(
                    [[0, 0], [-t, 0], [0, -t]],
                    [[-0.5 * TFw, pos_y - 1, 0.5 * h], [0.5 * TFw, pos_y - 1, 0.5 * h]]
                );
                let edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                mesh.rotation.y = THREE.Math.degToRad(deg);
                edge.rotation.y = THREE.Math.degToRad(deg);
                group.add(mesh);
                group.add(edge);
                // left 
                mesh = drawWeldShape(
                    [[0, 0], [t, 0], [0, -t]],
                    [[-0.5 * TFw, pos_y - 1, 0.5 * h - TFt], [-0.5 * Wt - r, pos_y - 1, 0.5 * h - TFt]]
                );
                edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                mesh.rotation.y = THREE.Math.degToRad(deg);
                edge.rotation.y = THREE.Math.degToRad(deg);
                group.add(mesh);
                group.add(edge);
                // right 
                mesh = drawWeldShape(
                    [[0, 0], [t, 0], [0, -t]],
                    [[0.5 * Wt + r, pos_y - 1, 0.5 * h - TFt], [0.5 * TFw, pos_y - 1, 0.5 * h - TFt]]
                );
                edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                mesh.rotation.y = THREE.Math.degToRad(deg);
                edge.rotation.y = THREE.Math.degToRad(deg);
                group.add(mesh);
                group.add(edge);
                is_add = true;
            }
            else if (data[2] == "left") {
                let t = parseFloat(data[3]) * factor * 1.4142;
                // left 
                mesh = drawWeldShape(
                    [[0, 0], [t, 0], [0, -t]],
                    [[-0.5 * TFw, pos_y - 1, 0.5 * h - TFt], [-0.5 * Wt - r, pos_y - 1, 0.5 * h - TFt]]
                );
                edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                group.add(mesh);
                group.add(edge);
                // right 
                mesh = drawWeldShape(
                    [[0, 0], [t, 0], [0, -t]],
                    [[0.5 * Wt + r, pos_y - 1, 0.5 * h - TFt], [0.5 * TFw, pos_y - 1, 0.5 * h - TFt]]
                );
                edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                mesh.rotation.y = THREE.Math.degToRad(deg);
                edge.rotation.y = THREE.Math.degToRad(deg);
                group.add(mesh);
                group.add(edge);
                is_add = true;
            }
            else if (data[2] == "right") {

                let t = parseFloat(data[3]) * factor * 1.4142;
                let mesh = drawWeldShape(
                    [[0, 0], [-t, 0], [0, -t]],
                    [[-0.5 * TFw, pos_y - 1, 0.5 * h], [0.5 * TFw, pos_y - 1, 0.5 * h]]
                );
                let edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                mesh.rotation.y = THREE.Math.degToRad(deg);
                edge.rotation.y = THREE.Math.degToRad(deg);
                group.add(mesh);
                group.add(edge);
                is_add = true;
            }
            // Web --------------------------------------------------------------------------------------------------------------------------------------------------------------
            // left
            data = input_data[2];
            if (data[2] == "left") {
                // left
                let t = parseFloat(data[3]) * factor * 1.4142;
                let mesh = drawWeldShape(
                    [[0, 0], [0, -t], [t, 0]],
                    [[-0.5 * Wt, pos_y - 1, -0.5 * h + TFt + r], [-0.5 * Wt, pos_y - 1, 0.5 * h - TFt - r]]
                );
                let edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                mesh.rotation.y = THREE.Math.degToRad(deg);
                edge.rotation.y = THREE.Math.degToRad(deg);
                group.add(mesh);
                group.add(edge);
                is_add = true;
            }
            else if (data[2] == "right") {
                // left
                let t = parseFloat(data[3]) * factor * 1.4142;
                let mesh = drawWeldShape(
                    [[0, 0], [0, t], [t, 0]],
                    [[0.5 * Wt, pos_y - 1, -0.5 * h + TFt + r], [0.5 * Wt, pos_y - 1, 0.5 * h - TFt - r]]
                );
                let edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                mesh.rotation.y = THREE.Math.degToRad(deg);
                edge.rotation.y = THREE.Math.degToRad(deg);
                group.add(mesh);
                group.add(edge);
                is_add = true;
            }
            else if (data[2] == "both") {
                // left
                let t = parseFloat(data[3]) * factor * 1.4142;
                let mesh = drawWeldShape(
                    [[0, 0], [0, -t], [t, 0]],
                    [[-0.5 * Wt, pos_y - 1, -0.5 * h + TFt + r], [-0.5 * Wt, pos_y - 1, 0.5 * h - TFt - r]]
                );
                let edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                mesh.rotation.y = THREE.Math.degToRad(deg);
                edge.rotation.y = THREE.Math.degToRad(deg);
                group.add(mesh);
                group.add(edge);
                // left
                mesh = drawWeldShape(
                    [[0, 0], [0, t], [t, 0]],
                    [[0.5 * Wt, pos_y - 1, -0.5 * h + TFt + r], [0.5 * Wt, pos_y - 1, 0.5 * h - TFt - r]]
                );
                edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                mesh.rotation.y = THREE.Math.degToRad(deg);
                edge.rotation.y = THREE.Math.degToRad(deg);
                group.add(mesh);
                group.add(edge);
                is_add = true;
            }
        }
        else if (TAB_STEEL_MEMBERS.data["steel-shear-lug-shape"] == "plate") {

            is_add = true;
            let width = parseFloat(TAB_STEEL_MEMBERS.data["steel-shear-lug-width"]) * factor;
            let thickness = parseFloat(TAB_STEEL_MEMBERS.data["steel-shear-lug-thickness"]) * factor;
            let deg = parseFloat(TAB_STEEL_MEMBERS.data["steel-shear-lug-rotation"]);
            let t = parseFloat(TAB_WELDS.data.input_table_shear[0][3]) * factor * 1.4142;

            if (TAB_WELDS.data.input_table_shear[0][2] == "both") {
                // left
                let mesh = drawWeldShape(
                    [[0, 0], [t, 0], [0, -t]],
                    [[-0.5 * width, pos_y - 1, -0.5 * thickness], [0.5 * width, pos_y - 1, -0.5 * thickness]]
                );
                let edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                mesh.rotation.y = THREE.Math.degToRad(deg);
                edge.rotation.y = THREE.Math.degToRad(deg);
                group.add(mesh);
                group.add(edge);
                // right
                mesh = drawWeldShape(
                    [[0, 0], [-t, 0], [0, -t]],
                    [[-0.5 * width, pos_y - 1, 0.5 * thickness], [0.5 * width, pos_y - 1, 0.5 * thickness]]
                );
                edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                mesh.rotation.y = THREE.Math.degToRad(deg);
                edge.rotation.y = THREE.Math.degToRad(deg);
                group.add(mesh);
                group.add(edge);
            }
            else if (TAB_WELDS.data.input_table_shear[0][2] == "left") {
                // left
                let mesh = drawWeldShape(
                    [[0, 0], [t, 0], [0, -t]],
                    [[-0.5 * width, pos_y - 1, -0.5 * thickness], [0.5 * width, pos_y - 1, -0.5 * thickness]]
                );
                let edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                mesh.rotation.y = THREE.Math.degToRad(deg);
                edge.rotation.y = THREE.Math.degToRad(deg);
                group.add(mesh);
                group.add(edge);
            }
            else if (TAB_WELDS.data.input_table_shear[0][2] == "left") {
                // right
                let mesh = drawWeldShape(
                    [[0, 0], [-t, 0], [0, -t]],
                    [[-0.5 * width, pos_y - 1, 0.5 * thickness], [0.5 * width, pos_y - 1, 0.5 * thickness]]
                );
                let edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                mesh.rotation.y = THREE.Math.degToRad(deg);
                edge.rotation.y = THREE.Math.degToRad(deg);
                group.add(mesh);
                group.add(edge);
            }
        }

        if (is_add) set.scene.add(group);

    };


    return {
        init: init,
        options: options
    };


})();