



let WELDS_COLUMN_PART = (function () {

    let options = {
        welds: { bfl:0, bfr:0, tb:0, tf:0,  tfl:0, tfr:0, wl:0,wr:0,p:0}
    };


    function drawWeldShape(weld, dir) {

        let shape = new THREE.Shape([new THREE.Vector2(weld[0][0], weld[0][1]), new THREE.Vector2(weld[1][0], weld[1][1]), new THREE.Vector2(weld[2][0], weld[2][1])]);
        let Spline = new THREE.CatmullRomCurve3([new THREE.Vector3(dir[0][0], dir[0][1], dir[0][2]), new THREE.Vector3(dir[1][0], dir[1][1], dir[1][2])]);
        let extrudeSettings = { steps: 1, bevelEnabled: false, extrudePath: Spline };
        let geometry = new THREE.ExtrudeBufferGeometry(shape, extrudeSettings);
        let material = new THREE.MeshBasicMaterial({ color: "red", wireframe: false });
        let mesh = new THREE.Mesh(geometry, material);
        return mesh;
    }


    let init = function (set) {

        let group = new THREE.Group();
        group.name = "welds_col";
        let is_add = false;
        options.welds = { 
            bfl:0,
            bfr:0,
            tb:0,
            tf:0, 
            tfl:0,
            tfr:0,
            wl:0,
            wr:0,
            p:0
        };

        let factor = (TAB_PROJECT_DETAILS.options.units == "metric") ? 1.0 : 25.4;
        let dims = TAB_STEEL_JSX_DRAWINGS.options.dims;

        if (TAB_STEEL_JSX_DRAWINGS.options.sec == "i-shape") {

            let input_data = TAB_WELDS.data.input_table_col;

            let TFw = dims.TFw * factor;
            let r = dims.r * factor;
            let TFt = dims.TFt * factor;
            let Wt = dims.Wt * factor;
            let h = dims.h * factor;

            // Top flange -----------------------------------------------------------------------------------------------------------------------------------------------
            let data = input_data[0];
            if (data[2] == "both") {
                // top
                let t = parseFloat(data[3]) * factor * 1.4142;
                options.welds.tf = t;
                options.welds.tfl = t;
                options.welds.tfr = t;
                let mesh = drawWeldShape(
                    [[0, 0], [t, 0], [0, t]],
                    [[-0.5 * TFw, 1, -0.5 * h], [0.5 * TFw, 1, -0.5 * h]]
                );
                mesh.castShadow = true;
		        mesh.receiveShadow = true;
                let edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                group.add(mesh);
                group.add(edge);
                // left 
                mesh = drawWeldShape(
                    [[0, 0], [-t, 0], [0, t]],
                    [[-0.5 * TFw, 1, -0.5 * h + TFt], [-0.5 * Wt - r, 1, -0.5 * h + TFt]]
                );
                mesh.castShadow = true;
		        mesh.receiveShadow = true;
                edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                group.add(mesh);
                group.add(edge);
                is_add = true;
                // right 
                mesh = drawWeldShape(
                    [[0, 0], [-t, 0], [0, t]],
                    [[0.5 * Wt + r, 1, -0.5 * h + TFt], [0.5 * TFw, 1, -0.5 * h + TFt]]
                );
                mesh.castShadow = true;
		        mesh.receiveShadow = true;
                edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                group.add(mesh);
                group.add(edge);
                is_add = true;
            }
            else if (data[2] == "left") {
                let t = parseFloat(data[3]) * factor * 1.4142;
                options.welds.tf = t;
                let mesh = drawWeldShape(
                    [[0, 0], [t, 0], [0, t]],
                    [[-0.5 * TFw, 1, -0.5 * h], [0.5 * TFw, 1, -0.5 * h]]
                );
                mesh.castShadow = true;
		        mesh.receiveShadow = true;
                let edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                group.add(mesh);
                group.add(edge);
                is_add = true;
            }
            else if (data[2] == "right") {
                let t = parseFloat(data[3]) * factor * 1.4142;
                options.welds.tfl = t;
                options.welds.tfr = t;
                // left 
                mesh = drawWeldShape(
                    [[0, 0], [-t, 0], [0, t]],
                    [[-0.5 * TFw, 1, -0.5 * h + TFt], [-0.5 * Wt - r, 1, -0.5 * h + TFt]]
                );
                mesh.castShadow = true;
		        mesh.receiveShadow = true;
                edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                group.add(mesh);
                group.add(edge);
                // right 
                mesh = drawWeldShape(
                    [[0, 0], [-t, 0], [0, t]],
                    [[0.5 * Wt + r, 1, -0.5 * h + TFt], [0.5 * TFw, 1, -0.5 * h + TFt]]
                );
                mesh.castShadow = true;
		        mesh.receiveShadow = true;
                edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                group.add(mesh);
                group.add(edge);
                is_add = true;
            }

            // Bottom flange ----------------------------------------------------------------------------------------------------------------------------------------
            data = input_data[1];
            if (data[2] == "both") {
                // top
                let t = parseFloat(data[3]) * factor * 1.4142;
                options.welds.tb = t;
                options.welds.bfl = t;
                options.welds.bfr = t;
                let mesh = drawWeldShape(
                    [[0, 0], [-t, 0], [0, t]],
                    [[-0.5 * TFw, 1, 0.5 * h], [0.5 * TFw, 1, 0.5 * h]]
                );
                mesh.castShadow = true;
		        mesh.receiveShadow = true;
                let edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                group.add(mesh);
                group.add(edge);
                // left 
                mesh = drawWeldShape(
                    [[0, 0], [t, 0], [0, t]],
                    [[-0.5 * TFw, 1, 0.5 * h - TFt], [-0.5 * Wt - r, 1, 0.5 * h - TFt]]
                );
                mesh.castShadow = true;
		        mesh.receiveShadow = true;
                edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                group.add(mesh);
                group.add(edge);
                is_add = true;
                // right 
                mesh = drawWeldShape(
                    [[0, 0], [t, 0], [0, t]],
                    [[0.5 * Wt + r, 1, 0.5 * h - TFt], [0.5 * TFw, 1, 0.5 * h - TFt]]
                );
                mesh.castShadow = true;
		        mesh.receiveShadow = true;
                edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                group.add(mesh);
                group.add(edge);
                is_add = true;
            }
            else if (data[2] == "left") {
                let t = parseFloat(data[3]) * factor * 1.4142;
                options.welds.bfl = t;
                options.welds.bfr = t;
                // left 
                mesh = drawWeldShape(
                    [[0, 0], [t, 0], [0, t]],
                    [[-0.5 * TFw, 1, 0.5 * h - TFt], [-0.5 * Wt - r, 1, 0.5 * h - TFt]]
                );
                mesh.castShadow = true;
		        mesh.receiveShadow = true;
                edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                group.add(mesh);
                group.add(edge);
                // right 
                mesh = drawWeldShape(
                    [[0, 0], [t, 0], [0, t]],
                    [[0.5 * Wt + r, 1, 0.5 * h - TFt], [0.5 * TFw, 1, 0.5 * h - TFt]]
                );
                mesh.castShadow = true;
		        mesh.receiveShadow = true;
                edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                group.add(mesh);
                group.add(edge);
                is_add = true;
            }
            else if (data[2] == "right") {

                let t = parseFloat(data[3]) * factor * 1.4142;
                options.welds.tb = t;
                let mesh = drawWeldShape(
                    [[0, 0], [-t, 0], [0, t]],
                    [[-0.5 * TFw, 1, 0.5 * h], [0.5 * TFw, 1, 0.5 * h]]
                );
                mesh.castShadow = true;
		        mesh.receiveShadow = true;
                let edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                group.add(mesh);
                group.add(edge);
                is_add = true;
            }
            // Web --------------------------------------------------------------------------------------------------------------------------------------------------------------
            // left
            data = input_data[2];
            if (data[2] == "left") {
                // left
                let t = parseFloat(data[3]) * factor * 1.4142;
                options.welds.wl = t;
                let mesh = drawWeldShape(
                    [[0, 0], [0, -t], [-t, 0]],
                    [[-0.5 * Wt, 1, -0.5 * h + TFt + r], [-0.5 * Wt, 1, 0.5 * h - TFt - r]]
                );
                mesh.castShadow = true;
		        mesh.receiveShadow = true;
                let edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                group.add(mesh);
                group.add(edge);
                is_add = true;
            }
            else if (data[2] == "right") {
                // left
                let t = parseFloat(data[3]) * factor * 1.4142;
                options.welds.wr = t;
                let mesh = drawWeldShape(
                    [[0, 0], [0, t], [-t, 0]],
                    [[0.5 * Wt, 1, -0.5 * h + TFt + r], [0.5 * Wt, 1, 0.5 * h - TFt - r]]
                );
                mesh.castShadow = true;
		        mesh.receiveShadow = true;
                let edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                group.add(mesh);
                group.add(edge);
                is_add = true;
            }
            else if (data[2] == "both") {
                // left
                let t = parseFloat(data[3]) * factor * 1.4142;
                options.welds.wl = t;
                options.welds.wr = t;
                let mesh = drawWeldShape(
                    [[0, 0], [0, -t], [-t, 0]],
                    [[-0.5 * Wt, 1, -0.5 * h + TFt + r], [-0.5 * Wt, 1, 0.5 * h - TFt - r]]
                );
                mesh.castShadow = true;
		        mesh.receiveShadow = true;
                let edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                group.add(mesh);
                group.add(edge);
                // left
                mesh = drawWeldShape(
                    [[0, 0], [0, t], [-t, 0]],
                    [[0.5 * Wt, 1, -0.5 * h + TFt + r], [0.5 * Wt, 1, 0.5 * h - TFt - r]]
                );
                mesh.castShadow = true;
		        mesh.receiveShadow = true;
                edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                group.add(mesh);
                group.add(edge);
                is_add = true;
            }
        }
        else if (["rectangular", "pipe"].indexOf(TAB_STEEL_JSX_DRAWINGS.options.sec) != -1) {
            let points = TAB_STEEL_JSX_DRAWINGS.options.points;
            let t = parseFloat(jQuery("#box-weld-throat").val()) * factor * 1.4142;
            options.welds.p = t;
            let new_points = [];
            if (TAB_STEEL_JSX_DRAWINGS.options.sec == "rectangular") new_points = TAB_STEEL_JSX_DRAWINGS.getRectPoints(dims, -t, factor);
            else if (TAB_STEEL_JSX_DRAWINGS.options.sec == "pipe") new_points = TAB_STEEL_JSX_DRAWINGS.getPipePoints(dims, t, factor);

            for (let i = 0; i < new_points.length; i++) {
                let shape = new THREE.Shape();
                if (i == new_points.length - 1) {
                    shape.moveTo(new_points[0][0], new_points[0][1]);
                    shape.lineTo(new_points[i][0], new_points[i][1]);
                    shape.lineTo(points[i][0], points[i][1]);
                    shape.lineTo(points[0][0], points[0][1]);
                }
                else {
                    shape.moveTo(new_points[i][0], new_points[i][1]);
                    shape.lineTo(new_points[i + 1][0], new_points[i + 1][1]);
                    shape.lineTo(points[i + 1][0], points[i + 1][1]);
                    shape.lineTo(points[i][0], points[i][1]);
                }

                let extrudeSettings = {
                    steps: 1,
                    depth: t,
                    bevelEnabled: false,
                    bevelThickness: 0,
                    bevelSize: 0,
                    bevelOffset: 0,
                    bevelSegments: 0
                };
                let geometry = new THREE.ExtrudeGeometry(shape, extrudeSettings);
                let material = new THREE.MeshBasicMaterial({ color: "red", side: THREE.DoubleSide });
                let mesh = new THREE.Mesh(geometry, material);
                mesh.castShadow = true;
		        mesh.receiveShadow = true;
                //
                if (TAB_STEEL_JSX_DRAWINGS.options.sec == "rectangular") {
                    if (mesh.geometry.vertices.length > 4) {
                        if (i == new_points.length - 1) {
                            mesh.geometry.vertices[6].z = 0;
                            mesh.geometry.vertices[7].z = 0;
                        }
                        else {
                            mesh.geometry.vertices[4].z = 0;
                            mesh.geometry.vertices[5].z = 0;
                        }
                    }
                }
                else if (TAB_STEEL_JSX_DRAWINGS.options.sec == "pipe") {
                    if (mesh.geometry.vertices.length > 4) {
                        mesh.geometry.vertices[6].z = 0;
                        mesh.geometry.vertices[7].z = 0;
                    }
                }
                //
                mesh.rotation.x = -0.5 * Math.PI;
                mesh.position.y = 1;
                if (mesh.geometry.vertices.length > 4) group.add(mesh);
                let edge = new THREE.LineSegments(new THREE.EdgesGeometry(mesh.geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
                edge.rotation.x = -0.5 * Math.PI;
                edge.position.y = 1;
                if (mesh.geometry.vertices.length > 4) group.add(edge);
            }
            is_add = true;
        }
        if (is_add) {
            set.scene.add(group);
        }

    };


    return {
        init: init,
        options: options
    };


})();