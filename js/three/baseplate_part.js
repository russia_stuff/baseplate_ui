



let BASEPLATE_PART = (function () {

    let options = {

    };


    let init = function (set) {

        let length_factor = (TAB_PROJECT_DETAILS.options.units == "imperial") ? 25.4 : 1.0;


        let width =  parseFloat(jQuery("#steel-baseplate-width").val()) * length_factor;
        let height = parseFloat(jQuery("#steel-baseplate-height").val()) * length_factor;
        let thickness = parseFloat(jQuery("#steel-baseplate-thickness").val()) * length_factor;
        let grout_thickness = parseFloat(jQuery("#steel-baseplate-grouting-thickness").val()) * length_factor;

   
        let group = new THREE.Group();
        group.name = "baseplate";

        // Base plate ------------------------------------------------------------------------------------------
        let geometry = new THREE.BoxGeometry(width, height, thickness);
        let mesh = new THREE.Mesh(geometry,new THREE.MeshBasicMaterial({wireframe:false,color: "#80dfff",}));
        mesh.castShadow = true;
		mesh.receiveShadow = true;
        mesh.rotation.x = -0.5 * Math.PI;
        mesh.position.y = -0.5 * thickness;

        // edge
        let edge = new THREE.LineSegments(
            new THREE.EdgesGeometry(geometry),
            new THREE.LineBasicMaterial({
                color: '#404040',
                linewidth: 1
            })
        );
        edge.rotation.x = -0.5 * Math.PI;
        edge.position.y = -0.5 * thickness;

        group.add(mesh);
        group.add(edge);

        // Grouting --------------------------------------------------------------------------------------------------
        geometry = new THREE.BoxGeometry(width, height, grout_thickness);
        mesh = new THREE.Mesh(
            geometry,
            new THREE.MeshPhongMaterial({
                color: 0xcccccc,
                transparent: true,
                opacity: 0.5
            })
        );

        edge = new THREE.LineSegments(
            new THREE.EdgesGeometry(geometry),
            new THREE.LineBasicMaterial({
                color: 'black',
                linewidth: 1
            })
        );
        mesh.rotation.x = -0.5 * Math.PI;
        mesh.position.y = -thickness - 0.5 * grout_thickness;
        edge.rotation.x = -0.5 * Math.PI;
        edge.position.y = -thickness - 0.5 * grout_thickness;
        group.add(mesh);
        group.add(edge);

        set.scene.add(group);
    };


    return {
        init: init,
    };


})();