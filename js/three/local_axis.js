


let LOCAL_AXIS = (function () {

    let options = {
        renderer: null,
        scene:null,
        camera:null
    };

    let init = function () {

        options.renderer = new THREE.WebGLRenderer({
            antialias: true,
            alpha: true,
            preserveDrawingBuffer: true
        });

        options.renderer.setSize(200, 200);

        document.getElementById("three-axis").appendChild(options.renderer.domElement);

        options.scene = new THREE.Scene();
        // scene.background = new THREE.Color(0xe6e6e6);
        options.camera = new THREE.PerspectiveCamera(45, 200 / 200, 10, 5000);
        // renderer_small.setClearColor('white', 1.0);           // background color

        let geom_box = new THREE.CubeGeometry(100, 100, 100);
        let mat_box = new THREE.MeshBasicMaterial({
            color: 'white',
            wireframe: false,
            transparent: true,
            opacity: 1.0
        });
        let mesh_box = new THREE.Mesh(geom_box, mat_box);
        options.scene.add(mesh_box);
        let mesh_box_edge = new THREE.LineSegments(
            new THREE.EdgesGeometry(geom_box),
            new THREE.LineBasicMaterial({
                color: 'black',
                linewidth: 2
            })
        );
        mesh_box_edge.position.set(mesh_box.position.x, mesh_box.position.y, mesh_box.position.z);
        options.scene.add(mesh_box_edge);

        let geom_ring = new THREE.RingGeometry(90, 120, 32);
        let mat_ring = new THREE.MeshBasicMaterial({ color: "white", transparent: true, opacity: 0.5, side: THREE.DoubleSide });
        let mesh_ring = new THREE.Mesh(geom_ring, mat_ring);
        mesh_ring.rotation.x = THREE.Math.degToRad(90);
        options.scene.add(mesh_ring);
        let mesh_ring_edge = new THREE.LineSegments(
            new THREE.EdgesGeometry(geom_ring),
            new THREE.LineBasicMaterial({
                color: 'black',
                linewidth: 2
            })
        );
        mesh_ring_edge.rotation.x = THREE.Math.degToRad(90);
        mesh_ring_edge.position.set(mesh_ring.position.x, mesh_ring.position.y, mesh_ring.position.z);
        options.scene.add(mesh_ring_edge);

        addSmallAxisNames(98, 0.1, 10, -90, 0, "Z");
        addSmallAxisNames(-110, 0.1, 10, -90, 0, "Z");
        addSmallAxisNames(10, 0.1, 118, 90, 180, "Y");
        addSmallAxisNames(-10, 0.1, -92, -90, 0, "Y");

        function addSmallAxisNames(x, y, z, angle1, angle2, name) {
            var loader = new THREE.FontLoader();
            loader.load('https://cdn.rawgit.com/redwavedesign/ccb20f24e7399f3d741e49fbe23add84/raw/402bcf913c55ad6b12ecfdd20c52e3047ff26ace/bebas_regular.typeface.js', function (font) {
                var textGeo = new THREE.TextGeometry(name, {
                    font: font,
                    size: 20,
                    height: 0,
                    curveSegments: 0,
                    bevelEnabled: true,
                    bevelThickness: 0,
                    bevelSize: 0,
                    bevelOffset: 0,
                    bevelSegments: 0
                });
                var textMaterial = new THREE.MeshPhongMaterial({ color: 'black' });
                mesh = new THREE.Mesh(textGeo, textMaterial);
                mesh.position.set(x, y, z);
                mesh.rotation.x = THREE.Math.degToRad(angle1);
                mesh.rotation.z = THREE.Math.degToRad(angle2);
                options.scene.add(mesh);
            });
        }

        
    };


    return {
        options: options,
        init: init
    };

})();