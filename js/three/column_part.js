



let COLUMN_PART = (function () {

    let options = {
        height: null,
        shape: null
    };


    let init = function (set) {

        let group = new THREE.Group();
        group.name = "column";

        let length_factor = (TAB_PROJECT_DETAILS.options.units == "imperial") ? 25.4 : 1.0;

        // Shape of section ------------------------------------------------------------------------------------
        let shape = new THREE.Shape();
        for (let i = 0; i < TAB_STEEL_JSX_DRAWINGS.options.points.length; i++) {
            let points = TAB_STEEL_JSX_DRAWINGS.options.points[i];
            if (i == 0) shape.moveTo(points[0] * length_factor, points[1] * length_factor);
            else shape.lineTo(points[0] * length_factor, points[1] * length_factor);
        }
        shape.lineTo(TAB_STEEL_JSX_DRAWINGS.options.points[0][0] * length_factor, TAB_STEEL_JSX_DRAWINGS.options.points[0][1] * length_factor);

        if (["rectangular", "pipe"].indexOf(TAB_STEEL_JSX_DRAWINGS.options.sec) != -1) {
            let hole = new THREE.Shape();
            for (let i = 0; i < TAB_STEEL_JSX_DRAWINGS.options.sub_points.length; i++) {
                let points = TAB_STEEL_JSX_DRAWINGS.options.sub_points[i];
                if (i == 0) hole.moveTo(points[0] * length_factor, points[1] * length_factor);
                else hole.lineTo(points[0] * length_factor, points[1] * length_factor);
            }
            hole.lineTo(TAB_STEEL_JSX_DRAWINGS.options.sub_points[0][0] * length_factor, TAB_STEEL_JSX_DRAWINGS.options.sub_points[0][1] * length_factor);
            shape.holes.push(hole);
        }

        options.shape = shape;
        let height_factor = 1.5;
        options.height = height_factor * TAB_STEEL_JSX_DRAWINGS.options.size * length_factor;

        let extrudeSettings = {
            steps: 1,
            depth: options.height,
            bevelEnabled: false,
            bevelThickness: 0,
            bevelSize: 0,
            bevelOffset: 0,
            bevelSegments: 0
        };

        
        let geometry = new THREE.ExtrudeGeometry(shape, extrudeSettings);
        let material = new THREE.MeshBasicMaterial({ color: "#00B6FF", wireframe: false, side: THREE.DoubleSide });
        let mesh = new THREE.Mesh(geometry, material);
        mesh.castShadow = true;
        mesh.receiveShadow = true;
        mesh.rotation.x = -0.5 * Math.PI;
        mesh.position.y = 1;

        // edge --------------------------------------------------------------------------------------------------
        let edge = new THREE.LineSegments(
            new THREE.EdgesGeometry(geometry),
            new THREE.LineBasicMaterial({
                color: '#404040',
                linewidth: 1
            })
        );
        // edge.position.set(mesh.x, mesh.y, mesh.z);
        edge.rotation.x = -0.5 * Math.PI;
        edge.position.y = 1;

        group.add(mesh);
        group.add(edge);
        group.castShadow = true;
        group.receiveShadow = true;
        set.scene.add(group);

    };


    return {
        init: init,
        options: options
    };


})();