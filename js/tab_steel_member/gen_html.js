




let GEN_HTML_STEEL_PART = (function () {


    let init = function (id) {

        let div = jQuery("#" + id);
        div.append(`
        <div id="left-panel-steel-box" style="display:none;padding: 20px 50px 0px 50px">
            <!-- TOP TABS -->
            <div class="ui top attached tabular menu">
                <a class="item active" data-tab="steel-tabs-column">Column</a>
                <a class="item" data-tab="steel-tabs-base">Base plate</a>
                <a class="item" data-tab="steel-tabs-lug">Shear Lug</a>
                <a class="item" data-tab="steel-foundation">Foundation</a>
            </div>
            <!-- Column Tab -->
            <div class="ui bottom attached tab segment active" data-tab="steel-tabs-column">
                <form class="ui form" style="padding: 20px;">
                    <div class="field">
                        <h3>Column Member Properties</h3>
                    </div>
                    <div class="w3-container w3-center" style="width:100%">
                        <div class="steel-tab-column-dimmer-place"></div>
                        <div id="steel-column-jsx" style="width:250px;height:250px;"></div>
                    </div>
                    <div class="field" style="margin-top: 30px;">
                        <!-- Row -->
                        <div class="two fields">
                            <div class="field" style="padding-right: 15px;">
                                <label>Shape</label>
                                <div id="steel-column-shape" class="ui fluid selection dropdown">
                                    <input type="hidden" name="gender">
                                    <i class="dropdown icon"></i>
                                    <div class="default text"></div>
                                    <div class="menu">
                                        <div class="item" data-value="i-shape">I-Shape</div>
                                        <div class="item" data-value="rectangular">Rectangular</div>
                                        <div class="item" data-value="pipe">Pipe</div>
                                    </div>
                                </div>
                            </div>
                            <div class="field" style="padding-left: 15px;">
                                <label>Database</label>
                                <div id="steel-column-database" class="ui fluid selection dropdown">
                                    <input type="hidden" name="gender">
                                    <i class="dropdown icon"></i>
                                    <div class="default text"></div>
                                    <div class="menu">
                                        <div class="item" data-value="imperial">Imperial</div>
                                        <div class="item" data-value="metric">Metric</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Row -->
                        <div class="two fields">
                            <div class="field" style="padding-right: 15px;">
                                <label>Profile</label>
                                <div id="steel-column-profile" class="ui fluid selection dropdown">
                                    <input type="hidden" name="gender">
                                    <i class="dropdown icon"></i>
                                    <div class="default text"></div>
                                    <div class="menu">
                                        <div class="item" data-value="imperial">Imperial</div>
                                        <div class="item" data-value="metric">Metric</div>
                                    </div>
                                </div>
                            </div>
                            <div class="field" style="padding-left: 15px;">
                                <label>Steel Material</label>
                                <div id="steel-column-material" class="ui fluid selection dropdown">
                                    <input type="hidden" name="gender">
                                    <i class="dropdown icon"></i>
                                    <div class="default text"></div>
                                    <div class="menu">
                                        <div class="item" data-value="imperial">Imperial</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- Base Tab -->
            <div class="ui bottom attached tab segment" data-tab="steel-tabs-base">
                <form class="ui form" style="padding: 20px;">
                    <div class="field">
                        <h3>Base Plate Properties</h3>
                    </div>
                    <div class="w3-container w3-center" style="width:100%">
                        <img id="steel-column-img" style="width:250px;height:250px;"
                            src="img/steel-baseplate.png"></img>
                    </div>
                    <div class="field" style="margin-top: 30px;">
                        <!-- Row -->
                        <div class="two fields">
                            <div class="field" style="padding-right: 15px;">
                                <label>Plate Width <sup class="panel-unit-length"></sup></label>
                                <input id="steel-baseplate-width" type="text" autocomplete="off">
                            </div>
                            <div class="field" style="padding-left: 15px;">
                                <label>Plate Height <sup class="panel-unit-length"></sup></label>
                                <input id="steel-baseplate-height" type="text" autocomplete="off">
                            </div>
                        </div>
                        <!-- Row -->
                        <div class="two fields">
                            <div class="field" style="padding-right: 15px;">
                                <label>Plate Thickness <sup class="panel-unit-length"></sup></label>
                                <input id="steel-baseplate-thickness" type="text" autocomplete="off">
                            </div>
                            <div class="field" style="padding-left: 15px;">
                                <label>Steel Material</label>
                                <div id="steel-baseplate-material" class="ui fluid selection dropdown">
                                    <input type="hidden" name="gender">
                                    <i class="dropdown icon"></i>
                                    <div class="default text"></div>
                                    <div class="menu">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Row -->
                        <div class="two fields">
                            <div class="field" style="padding-right: 15px;">
                                <label>Grouting Thickness <sup class="panel-unit-length"></sup></label>
                                <input id="steel-baseplate-grouting-thickness" type="text" autocomplete="off">
                            </div>
                            <div class="field" style="padding-left: 15px;">
                                <label>Grouting Material</label>
                                <div id="steel-grouting-material" class="ui fluid selection dropdown">
                                    <input type="hidden" name="gender">
                                    <i class="dropdown icon"></i>
                                    <div class="default text"></div>
                                    <div class="menu">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- Shear lug Tab -->
            <div class="ui bottom attached tab segment" data-tab="steel-tabs-lug">
                <div class="steel-tab-column-dimmer-place"></div>    
                <form class="ui form" style="padding: 20px;">
                    <div class="field">
                        <h3>Base Shear Lug Properties</h3>
                    </div>
                    <div class="w3-container w3-center" style="width:100%">
                        <img id="steel-column-img" style="width:250px;height:250px;"
                            src="img/steel-shear-lug.png"></img>
                    </div>
                    <div class="field" style="margin-top: 30px;">
                        <!-- Row -->
                        <div class="two fields">
                            <div class="field" style="padding-right: 15px;">
                                <label>Shape</label>
                                <div id="steel-shear-lug-shape" class="ui fluid selection dropdown">
                                    <input type="hidden" name="gender">
                                    <i class="dropdown icon"></i>
                                    <div class="default text"></div>
                                    <div class="menu">
                                    <div class="item" data-value="i-shape">I-Shape</div>
                                    <div class="item" data-value="plate">Plate</div>
                                    <div class="item" data-value="none">None</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Row -->
                        <div id="steel-shear-lug-dims-container">
                        </div>     
                    </div>
                </form>
            </div>
            <!-- Foundation Tab -->
            <div class="ui bottom attached tab segment" data-tab="steel-foundation">
                <form class="ui form" style="padding: 20px;">
                    <div class="field">
                        <h3>Foundation Properties</h3>
                    </div>
                    <div class="w3-container w3-center" style="width:100%">
                        <img id="steel-column-img" style="width:250px;height:250px;"
                            src="img/steel-foundation.png"></img>
                    </div>
                    <div class="field" style="margin-top: 30px;">
                        <!-- Row -->
                        <div class="two fields">
                            <div class="field" style="padding-right: 15px;">
                                <label>Width <sup class="panel-unit-length"></sup></label>
                                <input id="steel-foundation-width" type="text" autocomplete="off">
                            </div>
                            <div class="field" style="padding-left: 15px;">
                                <label>Height <sup class="panel-unit-length"></sup></label>
                                <input id="steel-foundation-height" type="text" autocomplete="off">
                            </div>
                        </div>
                        <!-- Row -->
                        <div class="two fields">
                            <div class="field" style="padding-right: 15px;">
                                <label>Thickness <sup class="panel-unit-length"></sup></label>
                                <input id="steel-foundation-thickness" type="text" autocomplete="off">
                            </div>
                            <div class="field" style="padding-left: 15px;">
                                <label>Concrete Material</label>
                                <div id="steel-foundation-material" class="ui fluid selection dropdown">
                                    <input type="hidden" name="gender">
                                    <i class="dropdown icon"></i>
                                    <div class="default text"></div>
                                    <div class="menu">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        `);
    };


    return {
        init: init
    };

})();