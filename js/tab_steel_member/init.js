



let TAB_STEEL_MEMBERS = (function () {

    let options = {
        main_dimmer: true,
        first_init: true
    };

    let data = {
        "steel-column-shape": "i-shape",
        "steel-column-database": "HP shapes",
        "steel-column-profile": "HP8x36",
        "steel-column-material": "A36",
        "steel-baseplate-width": "14",
        "steel-baseplate-height": "14",
        "steel-baseplate-thickness": "0.8",
        "steel-baseplate-grouting-thickness": "2.0",
        "steel-baseplate-material": "A36",
        "steel-grouting-material": "3000",
        "steel-shear-lug-shape": "none",
        "steel-shear-lug-database": "M shapes",
        "steel-shear-lug-profile": "M3x2.9",
        "steel-shear-lug-rotation": "0",
        "steel-shear-lug-material": "A36",
        "steel-shear-lug-width": "4",
        "steel-shear-lug-length": "4",
        "steel-shear-lug-thickness": "0.45",
        "steel-foundation-width": "30",
        "steel-foundation-height": "30",
        "steel-foundation-thickness": "15",
        "steel-foundation-material": "3000"
    };



    let profiles = {
        "i-shape": {
            "HP shapes": {},
            "M shapes": {},
            "S shapes": {},
            "W shapes": {}
        },
        "rectangular": {
            "Rectangular HSS": {},
            "Rectangular tubes (obsolete)": {},
            "Square HSS": {},
            "Square tubes (obsolete)": {}
        },
        "pipe": {
            "Pipes (obsolete)": {},
            "Round HSS": {}
        }
    };


    let resizePanel = function () {
        let div_width = jQuery("#left-panel-steel-box").width() - 100;
        let padding = (div_width - 250) * 0.5 + "px";
        jQuery("#steel-column-jsx").css({
            "margin-left": padding,
        });
    };

    let getProfileDims = function (database, profile, dims_only, update) {

        jQuery(".steel-tab-column-dimmer-place").empty().append(`
        <div class="ui active inverted dimmer">
            <div class="ui large text loader">Loading profiles</div>
        </div>
        `);
        SB.CONTEXT = "headless";
        SB.unit_system = TAB_PROJECT_DETAILS.options.units;
        SB.library.getLibraryFileSelect(['American', 'AISC', database, profile], profile, function (sec_data) {

            if (dims_only) {
                THREE_INIT.genShearLugPart({ dims: sec_data.dimensions });
            }
            else {
                TAB_STEEL_JSX_DRAWINGS.init({
                    sec: data["steel-column-shape"],
                    dims: sec_data.dimensions
                });

                THREE_INIT.genColumnPart();
                THREE_INIT.genLoadsPart({ id: 0, Mx: 0, My: 0, Vx: 0, Vy: 0, Nx: 0, load_type: "DL" });
                TAB_STIFFENERS.setInputbasedSection(update);
                THREE_INIT.genStiffenerPart({ update: update });

                if (options.first_init) {
                    options.first_init = false;
                    THREE_INIT.genbaseplatePart();
                    THREE_INIT.genFoundationPart();
                    THREE_INIT.updateCameraPos();
                    TAB_STIFFENERS.setInputbasedSection(false);
                    TAB_WELDS.initTables(false);
                    jQuery("#main-dimmer-place").empty();
                }
                else {
                    TAB_WELDS.initTables(update);
                }
                PANEL_INFO.options.info_col_name = profile;
                PANEL_INFO.init();
            }
            jQuery(".steel-tab-column-dimmer-place").empty();
        });
    };


    let fillProfilsDataBase = function (database, prof_id, data_prof_id, data_db_id, dims_only) {

        SB.CONTEXT = "headless";
        SB.unit_system = TAB_PROJECT_DETAILS.options.units;
        SB.library.getSectionLibrary(function (tree_data) {
            let profiles = tree_data["American"]["AISC"][database];
            COMMON_FUNC.fillDropData(prof_id, Object.keys(profiles), Object.keys(profiles)[0]);
            data[prof_id] = jQuery("#" + prof_id).dropdown("get value");
            jQuery("#" + prof_id).dropdown({
                onChange: function (profile) {
                    data_prof_id = profile;
                    getProfileDims(data_db_id, profile, dims_only, false);
                }
            });
            if (!options.first_init) getProfileDims(data_db_id, data[prof_id], dims_only, false);
            if (options.main_dimmer) {
                options.main_dimmer = false;
            }
        });
    };



    let shearLugDims = function (shape) {

        let div = jQuery("#steel-shear-lug-dims-container");
        div.empty();

        if (shape == "none") {
            div.empty();
            THREE_INIT.childsRemove("shear lug");
            THREE_INIT.update();
            PANEL_INFO.options.info_shear_name = "";
            PANEL_INFO.options.info_shear_mat = "";
            PANEL_INFO.init();
            THREE_INIT.genWeldsShearPart()
        }
        else if (shape == "i-shape") {
            div.append(`
            <div class="two fields">
                <div class="field" style="padding-right: 15px;">
                    <label>Database</label>
                    <div id="steel-shear-lug-database" class="ui fluid selection dropdown">
                        <input type="hidden" name="gender">
                        <i class="dropdown icon"></i>
                        <div class="default text"></div>
                        <div class="menu">
                        </div>
                    </div>
                </div>
                <div class="field" style="padding-left: 15px;">
                    <label>Profile</label>
                    <div id="steel-shear-lug-profile" class="ui fluid selection dropdown">
                        <input type="hidden" name="gender">
                        <i class="dropdown icon"></i>
                        <div class="default text"></div>
                        <div class="menu">
                        </div>
                    </div>
                </div>
            </div>
            <div class="two fields">
                <div class="field" style="padding-right: 15px;">
                    <label>Length <sup>(`+ TAB_PROJECT_DETAILS.options.units_length + `)</sup></label>
                    <input id="steel-shear-lug-length" type="text" autocomplete="off" value="`+ data["steel-shear-lug-length"].toFixed(2) + `">
                </div>
            </div>
            `);
            COMMON_FUNC.fillDropData("steel-shear-lug-database", Object.keys(profiles['i-shape']), data["steel-shear-lug-database"]);
            jQuery("#steel-shear-lug-database").dropdown({
                onChange: function (database) {
                    data["steel-shear-lug-database"] = database;
                    fillProfilsDataBase(database, "steel-shear-lug-profile", data["steel-shear-lug-profile"], data["steel-shear-lug-database"], true);
                    jQuery("#steel-shear-lug-profile").dropdown({
                        onChange: function (profile) {
                            data["steel-shear-lug-profile"] = profile;
                            getProfileDims(database, profile, true, false);
                        }
                    });
                }
            });
            fillProfilsDataBase(jQuery("#steel-shear-lug-database").dropdown("get value"), "steel-shear-lug-profile", data["steel-shear-lug-profile"], data["steel-shear-lug-database"], true);
            getProfileDims(data["steel-shear-lug-database"], data["steel-shear-lug-profile"], true, false);
            jQuery("#steel-shear-lug-profile").dropdown({
                onChange: function (profile) {
                    data["steel-shear-lug-profile"] = profile;
                    getProfileDims(data["steel-shear-lug-database"], profile, true, false);
                }
            });
        }
        else if (shape == "plate") {
            div.append(`
            <div class="two fields">
                <div class="field" style="padding-right: 15px;">
                    <label>Width <sup>(`+ TAB_PROJECT_DETAILS.options.units_length + `)</sup></label>
                    <input id="steel-shear-lug-width" type="text" autocomplete="off" value="`+ data["steel-shear-lug-width"].toFixed(2) + `">
                </div>
                <div class="field" style="padding-left: 15px;">
                    <label>Length <sup>(`+ TAB_PROJECT_DETAILS.options.units_length + `)</sup></label>
                    <input id="steel-shear-lug-length" type="text" autocomplete="off" value="`+ data["steel-shear-lug-length"].toFixed(2) + `">
                </div>    
            </div>
            <div class="two fields">
                <div class="field" style="padding-right: 15px;">
                    <label>Thickness <sup>(`+ TAB_PROJECT_DETAILS.options.units_length + `)</sup></label>
                    <input id="steel-shear-lug-thickness" type="text" autocomplete="off" value="`+ data["steel-shear-lug-thickness"].toFixed(2) + `">
                </div>  
            </div>
            `);
        }
        if (["i-shape", "plate"].indexOf(shape) != -1) {
            div.append(`
            <div class="two fields">
                <div class="field" style="padding-right: 15px;">
                    <label>Rotation <sup>(deg.)</sup></label>
                    <div id="steel-shear-lug-rotation" class="ui fluid selection dropdown">
                        <input type="hidden" name="gender">
                        <i class="dropdown icon"></i>
                        <div class="default text"></div>
                        <div class="menu">
                            <div class="item" data-value="0">0</div>
                            <div class="item" data-value="90">90</div>
                        </div>
                    </div>
                </div>
                <div class="field" style="padding-left: 15px;">
                    <label>Steel Material</label>
                    <div id="steel-shear-lug-material" class="ui fluid selection dropdown">
                        <input type="hidden" name="gender">
                        <i class="dropdown icon"></i>
                        <div class="default text"></div>
                        <div class="menu">
                        </div>
                    </div>
                </div>
            </div>
            `);
            jQuery('#left-panel-steel-box input[type="text"]').on('change', onChangeRedraw);
            jQuery('#left-panel input[type="text"]').on('change', validationInput);
            COMMON_FUNC.fillDropData("steel-shear-lug-material", COMMON_FUNC.steel_mat, data["steel-shear-lug-material"]);
            jQuery("#steel-shear-lug-rotation").dropdown();
            jQuery("#steel-shear-lug-rotation").dropdown("set selected", data["steel-shear-lug-rotation"]);
            jQuery("#steel-shear-lug-rotation").dropdown({
                onChange: function (deg) {
                    data["steel-shear-lug-rotation"] = deg;
                    getProfileDims(data["steel-shear-lug-database"], data["steel-shear-lug-profile"], true, false);
                }
            });
            jQuery("#steel-shear-lug-material").dropdown({
                onChange: function(name){
                    PANEL_INFO.options.info_shear_mat = name;
                    PANEL_INFO.init();
                }
            });
            PANEL_INFO.options.info_shear_mat = data["steel-shear-lug-material"];
            PANEL_INFO.init();
        }
        TAB_WELDS.initShearLugTable();
        
    
    };


    let updateBasedOnUnits = function (length_factor) {

        data["steel-baseplate-width"] = parseFloat(data["steel-baseplate-width"]) * length_factor;
        data["steel-baseplate-height"] = parseFloat(data["steel-baseplate-height"]) * length_factor;
        data["steel-baseplate-thickness"] = parseFloat(data["steel-baseplate-thickness"]) * length_factor;
        data["steel-baseplate-grouting-thickness"] = parseFloat(data["steel-baseplate-grouting-thickness"]) * length_factor;
        data["steel-foundation-width"] = parseFloat(data["steel-foundation-width"]) * length_factor;
        data["steel-foundation-height"] = parseFloat(data["steel-foundation-height"]) * length_factor;
        data["steel-foundation-thickness"] = parseFloat(data["steel-foundation-thickness"]) * length_factor;
        data["steel-shear-lug-width"] = parseFloat(data["steel-shear-lug-width"]) * length_factor;
        data["steel-shear-lug-length"] = parseFloat(data["steel-shear-lug-length"]) * length_factor;
        data["steel-shear-lug-thickness"] = parseFloat(data["steel-shear-lug-thickness"]) * length_factor;


        jQuery("#steel-baseplate-width").val(data["steel-baseplate-width"].toFixed(2));
        jQuery("#steel-baseplate-height").val(data["steel-baseplate-height"].toFixed(2));
        jQuery("#steel-baseplate-thickness").val(data["steel-baseplate-thickness"].toFixed(2));
        jQuery("#steel-baseplate-grouting-thickness").val(data["steel-baseplate-grouting-thickness"].toFixed(2));
        jQuery("#steel-foundation-width").val(data["steel-foundation-width"].toFixed(2));
        jQuery("#steel-foundation-height").val(data["steel-foundation-height"].toFixed(2));
        jQuery("#steel-foundation-thickness").val(data["steel-foundation-thickness"].toFixed(2));
        jQuery("#steel-shear-lug-width").val(data["steel-shear-lug-width"].toFixed(2));
        jQuery("#steel-shear-lug-length").val(data["steel-shear-lug-length"].toFixed(2));
        jQuery("#steel-shear-lug-thickness").val(data["steel-shear-lug-thickness"].toFixed(2));

        getProfileDims(data["steel-column-database"], data["steel-column-profile"], false, true);
    };


    let init = function () {

        // Info Box -------------------------------------------------------------------------------------------------------------------------------------------------
        PANEL_INFO.options.info_col_mat = data["steel-column-material"];
        PANEL_INFO.options.info_base_mat = data["steel-baseplate-material"];
        PANEL_INFO.options.info_foundation_mat = data["steel-foundation-material"];
        PANEL_INFO.options.info_grout_mat = data["steel-grouting-material"];
       

        jQuery("#steel-column-shape").dropdown();
        jQuery("#steel-shear-lug-shape").dropdown();
        jQuery("#steel-shear-lug-rotation").dropdown();

        // Default data -----------------------------------------------------------------------------------------------------------------------------------------------
        updateBasedOnUnits(TAB_PROJECT_DETAILS.options.length_factor);

        jQuery("#steel-column-shape").dropdown("set selected", data["steel-column-shape"]);
        COMMON_FUNC.fillDropData("steel-column-database", Object.keys(profiles[data["steel-column-shape"]]), data["steel-column-database"]);
        fillProfilsDataBase(data["steel-column-database"], "steel-column-profile", data["steel-column-profile"], data["steel-column-database"], false);

        COMMON_FUNC.fillDropData("steel-column-material", COMMON_FUNC.steel_mat, data["steel-column-material"]);
        COMMON_FUNC.fillDropData("steel-baseplate-material", COMMON_FUNC.steel_mat, data["steel-baseplate-material"]);
        COMMON_FUNC.fillDropData("steel-shear-lug-material", COMMON_FUNC.steel_mat, data["steel-shear-lug-material"]);
        COMMON_FUNC.fillDropData("steel-foundation-material", COMMON_FUNC.concrete_mat, data["steel-foundation-material"]);
        COMMON_FUNC.fillDropData("steel-grouting-material", COMMON_FUNC.concrete_mat, data["steel-grouting-material"]);

        jQuery("#steel-shear-lug-shape").dropdown("set selected", data["steel-shear-lug-shape"]);
        jQuery("#steel-shear-lug-rotation").dropdown("set selected", data["steel-shear-lug-rotation"]);


        // Dropdown settings ----------------------------------------------------------------------------------------------------------------

        jQuery("#steel-column-shape").dropdown({
            onChange: function (shape) {

                COMMON_FUNC.fillDropData("steel-column-database", Object.keys(profiles[shape]), Object.keys(profiles[shape])[0]);
                data["steel-column-shape"] = shape;
                data["steel-column-database"] = jQuery("#steel-column-database").dropdown("get value");
                fillProfilsDataBase(jQuery("#steel-column-database").dropdown("get value"), "steel-column-profile", data["steel-column-profile"], data["steel-column-database"], false);

                // Update event listener for database drop
                jQuery("#steel-column-database").dropdown({
                    onChange: function (database) {
                        data["steel-column-database"] = database;
                        fillProfilsDataBase(database, "steel-column-profile", data["steel-column-profile"], data["steel-column-database"], false);
                    }
                });
                TAB_STIFFENERS.setInputbasedSection();
            }
        });

        jQuery("#steel-column-database").dropdown({
            onChange: function (database) {
                data["steel-column-database"] = database;
                fillProfilsDataBase(database, "steel-column-profile", data["steel-column-profile"], data["steel-column-database"], false);
            }
        });


        jQuery("#steel-shear-lug-shape").dropdown({
            onChange: function (shape) {
                data["steel-shear-lug-shape"] = shape;
                shearLugDims(shape);
                if (["i-shape", "plate"].indexOf(shape) != -1) {
                    getProfileDims(data["steel-shear-lug-database"], data["steel-shear-lug-profile"], true, false);
                }

            }
        });

        jQuery("#steel-column-material").dropdown({
            onChange: function(name){
                PANEL_INFO.options.info_col_mat = name;
                PANEL_INFO.init();
            }
        });

        jQuery("#steel-baseplate-material").dropdown({
            onChange: function(name){
                PANEL_INFO.options.info_base_mat = name;
                PANEL_INFO.init();
            }
        });

        jQuery("#steel-foundation-material").dropdown({
            onChange: function(name){
                PANEL_INFO.options.info_foundation_mat = name;
                PANEL_INFO.init();
            }
        });

        jQuery("#steel-grouting-material").dropdown({
            onChange: function(name){
                PANEL_INFO.options.info_grout_mat = name;
                PANEL_INFO.init();
            }
        });


        // Validation --------------------------------------------------------------------------------------------------------------
        jQuery('#left-panel input[type="text"]').on('change', validationInput);

        // Update ------------------------------------------------------------------------------------------------------------------
        jQuery('#left-panel-steel-box input[type="text"]').on('change', onChangeRedraw);

    };

    let validationInput = function (e) {
        if (e.target.id == "") return;
        // if (e.target.id == "steel-baseplate-grouting-thickness") VALID.checkInput(e.target.id, 2, "panel-errors-box");
        if ([
            "steel-baseplate-grouting-thickness",
            "stiffener-prop-atop",
            "stiffener-prop-btop",
            "stiffener-prop-abot",
            "stiffener-prop-bbot",
            "stiffener-position-deg"
        ].indexOf(e.target.id) != -1) VALID.checkInput(e.target.id, 2, "panel-errors-box");
        else if (e.target.id == "stiffener-position-deg") VALID.checkInput(e.target.id, 0, "panel-errors-box");
        else VALID.checkInput(e.target.id, 1, "panel-errors-box");
    };

    let onChangeRedraw = function (e) {

        data[e.target.id] = jQuery("#" + e.target.id).val();

        if (["steel-baseplate-width", "steel-baseplate-height"].indexOf(e.target.id) != -1) {
            THREE_INIT.genbaseplatePart();
            THREE_INIT.genFoundationPart();
        }
        else if (["steel-baseplate-thickness", "steel-baseplate-grouting-thickness"].indexOf(e.target.id) != -1) {
            THREE_INIT.genbaseplatePart();
            THREE_INIT.genFoundationPart();
        }
        else if (["steel-foundation-width", "steel-foundation-height", "steel-foundation-thickness"].indexOf(e.target.id) != -1) {
            THREE_INIT.genFoundationPart();
        }
        else if (["steel-shear-lug-length"].indexOf(e.target.id) != -1) {
            getProfileDims(data["steel-shear-lug-database"], data["steel-shear-lug-profile"], true, false);
        }
    };




    return {
        init: init,
        resizePanel: resizePanel,
        getProfileDims: getProfileDims,
        updateBasedOnUnits: updateBasedOnUnits,
        data: data
    };


})();