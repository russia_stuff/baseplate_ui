


let TAB_STEEL_JSX_DRAWINGS = (function () {

    let options = {
        sec: "",
        points: [],
        sub_points: [],
        shearlug_points: [],
        size: null,
        welds_data: {},
    };

    let board = null;


    let weld_tf = function (t, f) {
        let dims = options.dims;
        let wt = t;
        return [[-0.5 * dims.TFw * f, 0.5 * dims.h * f], [0.5 * dims.TFw * f, 0.5 * dims.h * f], [0.5 * dims.TFw * f, (0.5 * dims.h + wt) * f], [-0.5 * dims.TFw * f, (0.5 * dims.h + wt) * f]];
    };

    let weld_tb = function (t, f) {
        let dims = options.dims;
        let wt = t;
        return [[-0.5 * dims.TFw * f, -0.5 * dims.h * f], [0.5 * dims.TFw * f, -0.5 * dims.h * f], [0.5 * dims.TFw * f, (-0.5 * dims.h - wt) * f], [-0.5 * dims.TFw * f, (-0.5 * dims.h - wt) * f]];
    };

    let weld_tfr = function (t, f) {
        let dims = options.dims;
        let wt = t;
        return [[0.5 * dims.TFw * f, (0.5 * dims.h - dims.TFt) * f], [(0.5 * dims.Wt + dims.r) * f, (0.5 * dims.h - dims.TFt) * f], [(0.5 * dims.Wt + dims.r) * f, (0.5 * dims.h - dims.TFt - wt) * f], [0.5 * dims.TFw * f, (0.5 * dims.h - dims.TFt - wt) * f]];
    };

    let weld_bfr = function (t, f) {
        let dims = options.dims;
        let wt = t;
        return [[0.5 * dims.TFw * f, (-0.5 * dims.h + dims.TFt) * f], [(0.5 * dims.Wt + dims.r) * f, (-0.5 * dims.h + dims.TFt) * f], [(0.5 * dims.Wt + dims.r) * f, (-0.5 * dims.h + dims.TFt + wt) * f], [0.5 * dims.TFw * f, (-0.5 * dims.h + dims.TFt + wt) * f]];
    };

    let weld_tfl = function (t, f) {
        let dims = options.dims;
        let wt = t;
        return [[-0.5 * dims.TFw * f, (0.5 * dims.h - dims.TFt) * f], [(-0.5 * dims.Wt - dims.r) * f, (0.5 * dims.h - dims.TFt) * f], [(-0.5 * dims.Wt - dims.r) * f, (0.5 * dims.h - dims.TFt - wt) * f], [-0.5 * dims.TFw * f, (0.5 * dims.h - dims.TFt - wt) * f]];
    };

    let weld_bfl = function (t, f) {
        let dims = options.dims;
        let wt = t;
        return [[-0.5 * dims.TFw * f, (-0.5 * dims.h + dims.TFt) * f], [(-0.5 * dims.Wt - dims.r) * f, (-0.5 * dims.h + dims.TFt) * f], [(-0.5 * dims.Wt - dims.r) * f, (-0.5 * dims.h + dims.TFt + wt) * f], [-0.5 * dims.TFw * f, (-0.5 * dims.h + dims.TFt + wt) * f]];
    };

    let weld_wr = function (t, f) {
        let dims = options.dims;
        let wt = t;
        return [[0.5 * dims.Wt * f, (0.5 * dims.h - dims.TFt - dims.r) * f], [0.5 * dims.Wt * f, (-0.5 * dims.h + dims.TFt + dims.r) * f], [(0.5 * dims.Wt + wt) * f, (-0.5 * dims.h + dims.TFt + dims.r) * f], [(0.5 * dims.Wt + wt) * f, (0.5 * dims.h - dims.TFt - dims.r) * f]];
    };

    let weld_wl = function (t, f) {
        let dims = options.dims;
        let wt = t;
        return [[-0.5 * dims.Wt * f, (0.5 * dims.h - dims.TFt - dims.r) * f], [-0.5 * dims.Wt * f, (-0.5 * dims.h + dims.TFt + dims.r) * f], [(-0.5 * dims.Wt - wt) * f, (-0.5 * dims.h + dims.TFt + dims.r) * f], [(-0.5 * dims.Wt - wt) * f, (0.5 * dims.h - dims.TFt - dims.r) * f]];
    };


    let getISecPoints = function (dims) {

        let wt = 0.75 * dims.r;
        let wtr = (dims.r - wt) / dims.r;

        // Section main points
        let points = [];
        // top right
        points.push([0, 0.5 * dims.h]);
        points.push([0.5 * dims.TFw, 0.5 * dims.h]);
        points.push([0.5 * dims.TFw, 0.5 * dims.h - dims.TFt]);

        let temp = [];
        temp.push([0.5 * dims.Wt + dims.r, 0.5 * dims.h - dims.TFt]);
        temp.push(COMMON_FUNC.rotate(0.5 * dims.Wt + dims.r, 0.5 * dims.h - dims.TFt - dims.r, 0.5 * dims.Wt, 0.5 * dims.h - dims.TFt - dims.r, -75));
        temp.push(COMMON_FUNC.rotate(0.5 * dims.Wt + dims.r, 0.5 * dims.h - dims.TFt - dims.r, 0.5 * dims.Wt, 0.5 * dims.h - dims.TFt - dims.r, -60));
        temp.push(COMMON_FUNC.rotate(0.5 * dims.Wt + dims.r, 0.5 * dims.h - dims.TFt - dims.r, 0.5 * dims.Wt, 0.5 * dims.h - dims.TFt - dims.r, -45));
        temp.push(COMMON_FUNC.rotate(0.5 * dims.Wt + dims.r, 0.5 * dims.h - dims.TFt - dims.r, 0.5 * dims.Wt, 0.5 * dims.h - dims.TFt - dims.r, -30));
        temp.push(COMMON_FUNC.rotate(0.5 * dims.Wt + dims.r, 0.5 * dims.h - dims.TFt - dims.r, 0.5 * dims.Wt, 0.5 * dims.h - dims.TFt - dims.r, -15));
        temp.push([0.5 * dims.Wt, 0.5 * dims.h - dims.TFt - dims.r]);
        // weld round top right
        if (dims.is_col != undefined && dims.is_col == true) {
            options.welds_data.tfrr = [];
            for (let i = 0; i < temp.length; i++) {
                points.push(temp[i]);
                options.welds_data.tfrr.push(temp[i]);
            }
            if (wt > dims.r) options.welds_data.tfrr.push([0.5 * dims.Wt + dims.r, 0.5 * dims.h - dims.TFt - dims.r]);
            else {
                temp = temp.reverse();
                for (let i = 0; i < temp.length; i++) {
                    let center_x = 0.5 * dims.Wt + dims.r;
                    let center_y = 0.5 * dims.h - dims.TFt - dims.r;
                    let step_x = Math.abs(center_x - temp[i][0]) * wtr;
                    let step_y = Math.abs(center_y - temp[i][1]) * wtr;
                    options.welds_data.tfrr.push([center_x - step_x, center_y + step_y]);
                }
            }
        }
        else for (let i = 0; i < temp.length; i++)  points.push(temp[i]);
        //
        points.push([0.5 * dims.Wt, 0]);
        // bot right
        temp = [];
        temp.push([0.5 * dims.Wt, -0.5 * dims.h + dims.BFt + dims.r]);
        temp.push(COMMON_FUNC.rotate(0.5 * dims.Wt + dims.r, -0.5 * dims.h + dims.TFt + dims.r, 0.5 * dims.Wt, -0.5 * dims.h + dims.TFt + dims.r, 15));
        temp.push(COMMON_FUNC.rotate(0.5 * dims.Wt + dims.r, -0.5 * dims.h + dims.TFt + dims.r, 0.5 * dims.Wt, -0.5 * dims.h + dims.TFt + dims.r, 30));
        temp.push(COMMON_FUNC.rotate(0.5 * dims.Wt + dims.r, -0.5 * dims.h + dims.TFt + dims.r, 0.5 * dims.Wt, -0.5 * dims.h + dims.TFt + dims.r, 45));
        temp.push(COMMON_FUNC.rotate(0.5 * dims.Wt + dims.r, -0.5 * dims.h + dims.TFt + dims.r, 0.5 * dims.Wt, -0.5 * dims.h + dims.TFt + dims.r, 60));
        temp.push(COMMON_FUNC.rotate(0.5 * dims.Wt + dims.r, -0.5 * dims.h + dims.TFt + dims.r, 0.5 * dims.Wt, -0.5 * dims.h + dims.TFt + dims.r, 75));
        temp.push([0.5 * dims.Wt + dims.r, -0.5 * dims.h + dims.BFt]);
        // weld bot right round
        // weld round top right
        if (dims.is_col != undefined && dims.is_col == true) {
            options.welds_data.bfrr = [];
            for (let i = 0; i < temp.length; i++) {
                points.push(temp[i]);
                options.welds_data.bfrr.push(temp[i]);
            }
            if (wt > dims.r) options.welds_data.bfrr.push([0.5 * dims.Wt + dims.r, -0.5 * dims.h + dims.TFt + dims.r]);
            else {
                temp = temp.reverse();
                for (let i = 0; i < temp.length; i++) {
                    let center_x = 0.5 * dims.Wt + dims.r;
                    let center_y = -0.5 * dims.h + dims.TFt + dims.r;
                    let step_x = Math.abs(center_x - temp[i][0]) * wtr;
                    let step_y = Math.abs(center_y - temp[i][1]) * wtr;
                    options.welds_data.bfrr.push([center_x - step_x, center_y - step_y]);
                }
            }

        }
        else for (let i = 0; i < temp.length; i++)  points.push(temp[i]);
        // 
        points.push([0.5 * dims.BFw, -0.5 * dims.h + dims.BFt]);
        points.push([0.5 * dims.BFw, -0.5 * dims.h]);
        points.push([0, -0.5 * dims.h]);
        // bot left
        points.push([0, -0.5 * dims.h]);
        points.push([-0.5 * dims.BFw, -0.5 * dims.h]);
        points.push([-0.5 * dims.BFw, -0.5 * dims.h + dims.BFt]);
        temp = [];
        temp.push([-0.5 * dims.Wt - dims.r, -0.5 * dims.h + dims.BFt]);
        temp.push(COMMON_FUNC.rotate(-0.5 * dims.Wt - dims.r, -0.5 * dims.h + dims.TFt + dims.r, -0.5 * dims.Wt, -0.5 * dims.h + dims.TFt + dims.r, -75));
        temp.push(COMMON_FUNC.rotate(-0.5 * dims.Wt - dims.r, -0.5 * dims.h + dims.TFt + dims.r, -0.5 * dims.Wt, -0.5 * dims.h + dims.TFt + dims.r, -60));
        temp.push(COMMON_FUNC.rotate(-0.5 * dims.Wt - dims.r, -0.5 * dims.h + dims.TFt + dims.r, -0.5 * dims.Wt, -0.5 * dims.h + dims.TFt + dims.r, -45));
        temp.push(COMMON_FUNC.rotate(-0.5 * dims.Wt - dims.r, -0.5 * dims.h + dims.TFt + dims.r, -0.5 * dims.Wt, -0.5 * dims.h + dims.TFt + dims.r, -30));
        temp.push(COMMON_FUNC.rotate(-0.5 * dims.Wt - dims.r, -0.5 * dims.h + dims.TFt + dims.r, -0.5 * dims.Wt, -0.5 * dims.h + dims.TFt + dims.r, -15));
        temp.push([-0.5 * dims.Wt, -0.5 * dims.h + dims.BFt + dims.r]);
        // weld bot left round
        if (dims.is_col != undefined && dims.is_col == true) {
            options.welds_data.bflr = [];
            for (let i = 0; i < temp.length; i++) {
                points.push(temp[i]);
                options.welds_data.bflr.push(temp[i]);
            }
            if (wt > dims.r) options.welds_data.bflr.push([-0.5 * dims.Wt - dims.r, -0.5 * dims.h + dims.TFt + dims.r]);
            else {
                temp = temp.reverse();
                for (let i = 0; i < temp.length; i++) {
                    let center_x = -0.5 * dims.Wt - dims.r;
                    let center_y = -0.5 * dims.h + dims.TFt + dims.r;
                    let step_x = Math.abs(center_x - temp[i][0]) * wtr;
                    let step_y = Math.abs(center_y - temp[i][1]) * wtr;
                    options.welds_data.bflr.push([center_x + step_x, center_y - step_y]);
                }
            }

        }
        else for (let i = 0; i < temp.length; i++)  points.push(temp[i]);
        //
        points.push([-0.5 * dims.Wt, 0]);
        // top left
        temp = [];
        temp.push([-0.5 * dims.Wt, 0.5 * dims.h - dims.TFt - dims.r]);
        temp.push(COMMON_FUNC.rotate(-0.5 * dims.Wt - dims.r, 0.5 * dims.h - dims.TFt - dims.r, -0.5 * dims.Wt, 0.5 * dims.h - dims.TFt - dims.r, 15));
        temp.push(COMMON_FUNC.rotate(-0.5 * dims.Wt - dims.r, 0.5 * dims.h - dims.TFt - dims.r, -0.5 * dims.Wt, 0.5 * dims.h - dims.TFt - dims.r, 30));
        temp.push(COMMON_FUNC.rotate(-0.5 * dims.Wt - dims.r, 0.5 * dims.h - dims.TFt - dims.r, -0.5 * dims.Wt, 0.5 * dims.h - dims.TFt - dims.r, 45));
        temp.push(COMMON_FUNC.rotate(-0.5 * dims.Wt - dims.r, 0.5 * dims.h - dims.TFt - dims.r, -0.5 * dims.Wt, 0.5 * dims.h - dims.TFt - dims.r, 60));
        temp.push(COMMON_FUNC.rotate(-0.5 * dims.Wt - dims.r, 0.5 * dims.h - dims.TFt - dims.r, -0.5 * dims.Wt, 0.5 * dims.h - dims.TFt - dims.r, 75));
        temp.push([-0.5 * dims.Wt - dims.r, 0.5 * dims.h - dims.TFt]);
        // weld round top left
        if (dims.is_col != undefined && dims.is_col == true) {
            options.welds_data.tflr = [];
            for (let i = 0; i < temp.length; i++) {
                points.push(temp[i]);
                options.welds_data.tflr.push(temp[i]);
            }
            if (wt > dims.r) options.welds_data.tflr.push([-0.5 * dims.Wt - dims.r, 0.5 * dims.h - dims.TFt - dims.r]);
            else {
                temp = temp.reverse();
                for (let i = 0; i < temp.length; i++) {
                    let center_x = -0.5 * dims.Wt - dims.r;
                    let center_y = 0.5 * dims.h - dims.TFt - dims.r;
                    let step_x = Math.abs(center_x - temp[i][0]) * wtr;
                    let step_y = Math.abs(center_y - temp[i][1]) * wtr;
                    options.welds_data.tflr.push([center_x + step_x, center_y + step_y]);
                }
            }

        }
        else for (let i = 0; i < temp.length; i++)  points.push(temp[i]);
        //
        points.push([-0.5 * dims.TFw, 0.5 * dims.h - dims.TFt]);
        points.push([-0.5 * dims.TFw, 0.5 * dims.h]);
        points.push([0, 0.5 * dims.h]);

        return points;
    };



    let getPipePoints = function (dims, t, f) {

        let steps = 30;
        let angle_step = 360 / steps;
        let angle = 0;
        let result = [];

        for (let i = 0; i <= steps; i++) {
            result.push(COMMON_FUNC.rotate(0, 0, 0, 0.5 * dims.D * f + t, angle));
            angle += angle_step;
        }
        return result;
    };


    let getRectPoints = function (dims, t, f) {

        let points = [];
        let dt = dims.t * f;
        let r = dims.r * f;
        let b = dims.b * f;
        let h = dims.h * f;

        // top left
        points.push([-0.5 * b + t, 0.5 * h - dt - r]);
        points.push(COMMON_FUNC.rotate(-0.5 * b + dt + r, 0.5 * h - dt - r, -0.5 * b + t, 0.5 * h - dt - r, -15));
        points.push(COMMON_FUNC.rotate(-0.5 * b + dt + r, 0.5 * h - dt - r, -0.5 * b + t, 0.5 * h - dt - r, -30));
        points.push(COMMON_FUNC.rotate(-0.5 * b + dt + r, 0.5 * h - dt - r, -0.5 * b + t, 0.5 * h - dt - r, -45));
        points.push(COMMON_FUNC.rotate(-0.5 * b + dt + r, 0.5 * h - dt - r, -0.5 * b + t, 0.5 * h - dt - r, -60));
        points.push(COMMON_FUNC.rotate(-0.5 * b + dt + r, 0.5 * h - dt - r, -0.5 * b + t, 0.5 * h - dt - r, -75));
        points.push([-0.5 * b + dt + r, 0.5 * h - t]);
        points.push([0, 0.5 * h - t]);
        // top right
        points.push([0, 0.5 * h - t]);
        points.push([0.5 * b - dt - r, 0.5 * h - t]);
        points.push(COMMON_FUNC.rotate(0.5 * b - dt - r, 0.5 * h - dt - r, 0.5 * b - t, 0.5 * h - dt - r, 75));
        points.push(COMMON_FUNC.rotate(0.5 * b - dt - r, 0.5 * h - dt - r, 0.5 * b - t, 0.5 * h - dt - r, 60));
        points.push(COMMON_FUNC.rotate(0.5 * b - dt - r, 0.5 * h - dt - r, 0.5 * b - t, 0.5 * h - dt - r, 45));
        points.push(COMMON_FUNC.rotate(0.5 * b - dt - r, 0.5 * h - dt - r, 0.5 * b - t, 0.5 * h - dt - r, 30));
        points.push(COMMON_FUNC.rotate(0.5 * b - dt - r, 0.5 * h - dt - r, 0.5 * b - t, 0.5 * h - dt - r, 15));
        points.push([0.5 * b - t, 0.5 * h - dt - r]);
        points.push([0.5 * b - t, 0]);
        // bot right
        points.push([0.5 * b - t, -0.5 * h + dt + r]);
        points.push(COMMON_FUNC.rotate(0.5 * b - dt - r, -0.5 * h + dt + r, 0.5 * b - t, -0.5 * h + dt + r, -15));
        points.push(COMMON_FUNC.rotate(0.5 * b - dt - r, -0.5 * h + dt + r, 0.5 * b - t, -0.5 * h + dt + r, -30));
        points.push(COMMON_FUNC.rotate(0.5 * b - dt - r, -0.5 * h + dt + r, 0.5 * b - t, -0.5 * h + dt + r, -45));
        points.push(COMMON_FUNC.rotate(0.5 * b - dt - r, -0.5 * h + dt + r, 0.5 * b - t, -0.5 * h + dt + r, -60));
        points.push(COMMON_FUNC.rotate(0.5 * b - dt - r, -0.5 * h + dt + r, 0.5 * b - t, -0.5 * h + dt + r, -75));
        points.push([0.5 * b - dt - r, -0.5 * h + t]);
        points.push([0, -0.5 * h + t]);
        // bot left
        points.push([-0.5 * b + dt + r, -0.5 * h + t]);
        points.push(COMMON_FUNC.rotate(-0.5 * b + dt + r, -0.5 * h + dt + r, -0.5 * b + dt + r, -0.5 * h + t, -15));
        points.push(COMMON_FUNC.rotate(-0.5 * b + dt + r, -0.5 * h + dt + r, -0.5 * b + dt + r, -0.5 * h + t, -30));
        points.push(COMMON_FUNC.rotate(-0.5 * b + dt + r, -0.5 * h + dt + r, -0.5 * b + dt + r, -0.5 * h + t, -45));
        points.push(COMMON_FUNC.rotate(-0.5 * b + dt + r, -0.5 * h + dt + r, -0.5 * b + dt + r, -0.5 * h + t, -60));
        points.push(COMMON_FUNC.rotate(-0.5 * b + dt + r, -0.5 * h + dt + r, -0.5 * b + dt + r, -0.5 * h + t, -75));
        points.push([-0.5 * b + t, -0.5 * h + dt + r]);
        points.push([-0.5 * b + t, 0]);

        return points;
    };


    let init = function (set) {

        options.points = [];
        options.sub_points = [];

        board = JXG.JSXGraph.initBoard('steel-column-jsx', { showNavigation: false, showCopyright: false, axis: false, grid: false, });

        let dims = set.dims;
        options.dims = dims;

        function drawDims(data) {
            board.create('line', data.line, { straightFirst: false, straightLast: false, strokeWidth: 1, strokeColor: 'black', highlight: false, fixed: true });
            board.create('line', data.left_arrow, { straightFirst: false, straightLast: false, strokeWidth: 1, strokeColor: 'black', highlight: false, fixed: true });
            board.create('line', data.right_arrow, { straightFirst: false, straightLast: false, strokeWidth: 1, strokeColor: 'black', highlight: false, fixed: true });
            board.create('text', data.text, { rotate: data.rotate, strokeColor: 'black', anchorX: data.anchorX, anchorY: data.anchorY, fontSize: 12, display: 'internal', highlight: false, fixed: true });
        }

        options.sec = set.sec;

        if (set.sec == "i-shape") {
            // refresh board
            let size = Math.max(dims.h, dims.TFw);
            options.size = size;
            let BB = [-0.75 * size, 0.75 * size, 0.75 * size, -0.75 * size];
            left_gap = 0.75 * size - 0.5 * dims.TFw;
            top_gap = 0.75 * size - 0.5 * dims.h;
            let gap = Math.min(left_gap, top_gap);
            board.setBoundingBox(BB);
            // Section main points
            options.welds_data = {};
            dims.is_col = true;
            let points = getISecPoints(dims);
            options.points = points;
            // Shape
            board.create('polygon', points, { fillColor: '#00B6FF', highlight: false, fixed: true, fillOpacity: 0.5, vertices: { visible: false }, borders: { fixed: true, strokeColor: "black", highlight: false, strokeWidth: 1 }, });

            drawDims({
                line: [[-0.5 * dims.TFw, 0.5 * dims.h + 0.3 * gap], [0.5 * dims.TFw, 0.5 * dims.h + 0.3 * gap]],
                left_arrow: [[-0.5 * dims.TFw, 0.5 * dims.h + 0.2 * gap], [-0.5 * dims.TFw, 0.5 * dims.h + 0.4 * gap]],
                right_arrow: [[0.5 * dims.TFw, 0.5 * dims.h + 0.2 * gap], [0.5 * dims.TFw, 0.5 * dims.h + 0.4 * gap]],
                text: [0, 0.5 * dims.h + 0.3 * gap, dims.TFw.toFixed(1)],
                anchorX: "middle",
                anchorY: "bottom",
                rotate: 0
            });

            drawDims({
                line: [[-0.5 * dims.Wt, -0.5 * dims.h - 0.3 * gap], [0.5 * dims.Wt, -0.5 * dims.h - 0.3 * gap]],
                left_arrow: [[-0.5 * dims.Wt, -0.5 * dims.h - 0.2 * gap], [-0.5 * dims.Wt, -0.5 * dims.h - 0.4 * gap]],
                right_arrow: [[0.5 * dims.Wt, -0.5 * dims.h - 0.2 * gap], [0.5 * dims.Wt, -0.5 * dims.h - 0.4 * gap]],
                text: [0, -0.5 * dims.h - 0.4 * gap, dims.Wt.toFixed(1)],
                anchorX: "middle",
                anchorY: "top",
                rotate: 0
            });

            drawDims({
                line: [[-0.5 * dims.TFw - 0.3 * gap, -0.5 * dims.h], [-0.5 * dims.TFw - 0.3 * gap, 0.5 * dims.h]],
                left_arrow: [[-0.5 * dims.TFw - 0.2 * gap, -0.5 * dims.h], [-0.5 * dims.TFw - 0.4 * gap, -0.5 * dims.h]],
                right_arrow: [[-0.5 * dims.TFw - 0.2 * gap, 0.5 * dims.h], [-0.5 * dims.TFw - 0.4 * gap, 0.5 * dims.h]],
                text: [-0.5 * dims.TFw - 0.3 * gap, 0, dims.h.toFixed(1)],
                anchorX: "middle",
                anchorY: "bottom",
                rotate: 90
            });

            drawDims({
                line: [[0.5 * dims.TFw + 0.3 * gap, 0.5 * dims.h], [0.5 * dims.TFw + 0.3 * gap, 0.5 * dims.h - dims.TFt]],
                left_arrow: [[0.5 * dims.TFw + 0.2 * gap, 0.5 * dims.h], [0.5 * dims.TFw + 0.4 * gap, 0.5 * dims.h]],
                right_arrow: [[0.5 * dims.TFw + 0.2 * gap, 0.5 * dims.h - dims.TFt], [0.5 * dims.TFw + 0.4 * gap, 0.5 * dims.h - dims.TFt]],
                text: [0.5 * dims.TFw + 0.4 * gap, 0.5 * dims.h - 0.5 * dims.TFt, dims.TFt.toFixed(1)],
                anchorX: "middle",
                anchorY: "top",
                rotate: 90
            });

            drawDims({
                line: [[0.5 * dims.TFw + 0.3 * gap, -0.5 * dims.h], [0.5 * dims.TFw + 0.3 * gap, -0.5 * dims.h + dims.TFt]],
                left_arrow: [[0.5 * dims.TFw + 0.2 * gap, -0.5 * dims.h], [0.5 * dims.TFw + 0.4 * gap, -0.5 * dims.h]],
                right_arrow: [[0.5 * dims.TFw + 0.2 * gap, -0.5 * dims.h + dims.TFt], [0.5 * dims.TFw + 0.4 * gap, -0.5 * dims.h + dims.TFt]],
                text: [0.5 * dims.TFw + 0.4 * gap, -0.5 * dims.h + 0.5 * dims.TFt, dims.TFt.toFixed(1)],
                anchorX: "middle",
                anchorY: "top",
                rotate: 90
            });
        }
        else if (set.sec == "rectangular") {
            // refresh board
            let size = Math.max(dims.h, dims.b);
            options.size = size;
            let BB = [-0.75 * size, 0.75 * size, 0.75 * size, -0.75 * size];
            left_gap = 0.75 * size - 0.5 * dims.b;
            top_gap = 0.75 * size - 0.5 * dims.h;
            let gap = Math.min(left_gap, top_gap);
            board.setBoundingBox(BB);

            // outer shape
            options.points = getRectPoints(dims, 0.0, 1.0);
            board.create('polygon', options.points, { fillColor: '#00B6FF', highlight: false, fixed: true, fillOpacity: 0.5, vertices: { visible: false }, borders: { fixed: true, strokeColor: "black", highlight: false, strokeWidth: 1 }, });

            // iner shape
            options.sub_points = getRectPoints(dims, dims.t, 1.0);
            board.create('polygon', options.sub_points, { fillColor: '#FFFFFF', highlight: false, fixed: true, fillOpacity: 1, vertices: { visible: false }, borders: { fixed: true, strokeColor: "black", highlight: false, strokeWidth: 1 }, });

            drawDims({
                line: [[-0.5 * dims.b, 0.5 * dims.h + 0.3 * gap], [0.5 * dims.b, 0.5 * dims.h + 0.3 * gap]],
                left_arrow: [[-0.5 * dims.b, 0.5 * dims.h + 0.2 * gap], [-0.5 * dims.b, 0.5 * dims.h + 0.4 * gap]],
                right_arrow: [[0.5 * dims.b, 0.5 * dims.h + 0.2 * gap], [0.5 * dims.b, 0.5 * dims.h + 0.4 * gap]],
                text: [0, 0.5 * dims.h + 0.3 * gap, dims.b.toFixed(1)],
                anchorX: "middle",
                anchorY: "bottom",
                rotate: 0
            });

            drawDims({
                line: [[0.5 * dims.b - dims.t, -0.5 * dims.h - 0.3 * gap], [0.5 * dims.b, -0.5 * dims.h - 0.3 * gap]],
                left_arrow: [[0.5 * dims.b - dims.t, -0.5 * dims.h - 0.2 * gap], [0.5 * dims.b - dims.t, -0.5 * dims.h - 0.4 * gap]],
                right_arrow: [[0.5 * dims.b, -0.5 * dims.h - 0.2 * gap], [0.5 * dims.b, -0.5 * dims.h - 0.4 * gap]],
                text: [0.5 * dims.b - 0.5 * dims.t, -0.5 * dims.h - 0.4 * gap, dims.t.toFixed(1)],
                anchorX: "middle",
                anchorY: "top",
                rotate: 0
            });

            drawDims({
                line: [[-0.5 * dims.b - 0.3 * gap, -0.5 * dims.h], [-0.5 * dims.b - 0.3 * gap, 0.5 * dims.h]],
                left_arrow: [[-0.5 * dims.b - 0.2 * gap, -0.5 * dims.h], [-0.5 * dims.b - 0.4 * gap, -0.5 * dims.h]],
                right_arrow: [[-0.5 * dims.b - 0.2 * gap, 0.5 * dims.h], [-0.5 * dims.b - 0.4 * gap, 0.5 * dims.h]],
                text: [-0.5 * dims.b - 0.3 * gap, 0, dims.h.toFixed(1)],
                anchorX: "middle",
                anchorY: "bottom",
                rotate: 90
            });
        }

        else if (set.sec == "pipe") {
            // refresh board
            let size = dims.D;
            options.size = size;
            let BB = [-0.75 * size, 0.75 * size, 0.75 * size, -0.75 * size];
            left_gap = 0.75 * size - 0.5 * dims.D;
            top_gap = 0.75 * size - 0.5 * dims.D;
            let gap = Math.min(left_gap, top_gap);
            board.setBoundingBox(BB);

            // Section main points
            board.create('circle', [[0, 0], 0.5 * dims.D], { strokeWidth: 1.0, strokeColor: 'black', fillColor: '#00B6FF', fillOpacity: 0.5, fixed: true, highlight: false });
            board.create('circle', [[0, 0], 0.5 * dims.D - dims.t], { strokeWidth: 1.0, strokeColor: 'black', fillColor: '#FFFFFF', fillOpacity: 1.0, fixed: true, highlight: false });

            options.points = getPipePoints(dims, 0.0, 1.0);
            options.sub_points = getPipePoints(dims, -dims.t, 1.0);

            drawDims({
                line: [[-0.5 * dims.D - 0.3 * gap, -0.5 * dims.D], [-0.5 * dims.D - 0.3 * gap, 0.5 * dims.D]],
                left_arrow: [[-0.5 * dims.D - 0.2 * gap, -0.5 * dims.D], [-0.5 * dims.D - 0.4 * gap, -0.5 * dims.D]],
                right_arrow: [[-0.5 * dims.D - 0.2 * gap, 0.5 * dims.D], [-0.5 * dims.D - 0.4 * gap, 0.5 * dims.D]],
                text: [-0.5 * dims.D - 0.3 * gap, 0, dims.D.toFixed(1)],
                anchorX: "middle",
                anchorY: "bottom",
                rotate: 90
            });

            drawDims({
                line: [[0.5 * dims.D + 0.3 * gap, 0.5 * dims.D], [0.5 * dims.D + 0.3 * gap, 0.5 * dims.D - dims.t]],
                left_arrow: [[0.5 * dims.D + 0.2 * gap, 0.5 * dims.D], [0.5 * dims.D + 0.4 * gap, 0.5 * dims.D]],
                right_arrow: [[0.5 * dims.D + 0.2 * gap, 0.5 * dims.D - dims.t], [0.5 * dims.D + 0.4 * gap, 0.5 * dims.D - dims.t]],
                text: [0.5 * dims.D + 0.4 * gap, 0.5 * dims.D - 0.5 * dims.t, dims.t.toFixed(1)],
                anchorX: "middle",
                anchorY: "top",
                rotate: 90
            });
        }
    };


    return {
        init: init,
        options: options,
        getISecPoints: getISecPoints,
        getRectPoints: getRectPoints,
        getPipePoints: getPipePoints,
        weld_bfl: weld_bfl,
        weld_bfr: weld_bfr,
        weld_tb: weld_tb,
        weld_tf: weld_tf,
        weld_tfl: weld_tfl,
        weld_tfr: weld_tfr,
        weld_wl: weld_wl,
        weld_wr: weld_wr
    };

})();