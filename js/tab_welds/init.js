



let TAB_WELDS = (function () {

    let options = {
        input_table_col: null,
        input_table_stiff: null,
        jsx_check: false,
        stiff_tab_check: false
    };


    let data = {
        "welds-column-mat": "E60xx",
        "welds-stiffener-mat": "E60xx",
        "welds-shear-lug-mat": "E60xx",
        "box-weld-throat": "0.2",
        "default-throat": 0.2,
        "input_table_col": [],
        "input_table_stiff": [],
        "input_table_shear": []
    };

    // let resizePanel = function () {
    //     let div_width = jQuery("#left-panel-welds-box").width() - 100;
    //     let padding = (div_width - jQuery("#welds-tab-jsx").width()) * 0.5 + "px";
    //     jQuery("#welds-tab-jsx").css({
    //         "margin-left": padding
    //     });
    // };

    let updateBasedOnUnits = function (length_factor) {

        data["box-weld-throat"] = parseFloat(data["box-weld-throat"]) * length_factor;

        if (data.input_table_col.length > 0) {
            for (let i = 0; i < data.input_table_col.length; i++) {
                data.input_table_col[i][3] = (data.input_table_col[i][3] * length_factor).toFixed(4);
            }
            options.input_table_col.setData(data.input_table_col, false, true);
        }
        if (data.input_table_stiff.length > 0) {
            for (let i = 0; i < data.input_table_stiff.length; i++) {
                data.input_table_stiff[i][3] = (data.input_table_stiff[i][3] * length_factor).toFixed(4);
            }
            options.input_table_stiff.setData(data.input_table_stiff, false, true);
        }
        if (data.input_table_shear.length > 0) {
            for (let i = 0; i < data.input_table_shear.length; i++) {
                data.input_table_shear[i][3] = (data.input_table_shear[i][3] * length_factor).toFixed(4);
            }
            options.input_table_shear.setData(data.input_table_shear, false, true);
        }

    };


    let resizePanel = function () {
        let ratio = MAIN_JSX_DRAWINGS.options.base_ratio;
        let jsx_div = jQuery("#welds-tab-jsx");
        if (ratio < 1.0) jsx_div.height(jsx_div.width() * ratio);
        else jsx_div.height(jsx_div.width());
        MAIN_JSX_DRAWINGS.initWeldsBase();
        let parent_div = jsx_div.parent();
        jsx_div.css("margin-left", 0.5 * (parent_div.width() - jsx_div.width()) + "px");
    };


    let tableRowTemplates = function (set) {
        if (set.case == "id") {
            return {
                "id": "id",
                "title": "ID",
                //"tooltip": "Where is the rebar measured from?",
                "default_value": 1,
                "type": "integer",
                "cell_type": "input_text",
                "cell_width": set.width,
                "disabled": true,
            };
        }
        else if (set.case == "number") {
            return {
                "id": set.id,
                "title": set.title,
                // "tooltip": "Dead Load",
                "default_value": 0,
                "type": "number",
                "cell_type": "input_text",
                "cell_width": set.width,
                "disabled": set.disabled,
                "change": set.func
            };
        }
        else if (set.case == "checkbox") {
            return {
                "id": set.id,
                "title": set.title,
                // "tooltip": "Dead Load",
                "default_value": 0,
                "cell_type": "checkbox",
                "cell_width": set.width,
                "disabled": false,
                "change": function (value, row_index, col_index) {
                }
            };
        }
        else if (set.case == "dropdown") {
            return {
                "id": set.id,
                "title": set.title,
                //"tooltip": "Where is the rebar measured from?",
                "default_value": "both",
                "type": "string",
                "cell_type": "dropdown",
                "cell_dropdown_values": ["none", "left", "right", "both"],
                "cell_dropdown_labels": ["none", "left", "right", "both"],
                "cell_width": set.width,
                "disabled": false,
                "change": set.func,
            };
        }
    };

    let eventColumn = function (value, row_index, col_index) {
        data.input_table_col = options.input_table_col.getData(false);
        THREE_INIT.genWeldsColumnPart();
    };

    let eventStiff = function (value, row_index, col_index) {
        data.input_table_stiff = options.input_table_stiff.getData(false);
        THREE_INIT.genWeldsStiffPart();
    };

    let eventShearLug = function (value, row_index, col_index) {
        data.input_table_shear = options.input_table_shear.getData(false);
        THREE_INIT.genWeldsShearPart();
    };



    // Column welds tables ------------------------------------------------------------------------------------------------------------------------

    // I shape
    let initISecColWeldTable = function () {


        jQuery("#welds-tabs-column-box").empty().append(`
            <div class="two fields">
                <div class="field" style="padding-right: 15px;">
                    <label>Steel Material</label>
                    <div id="welds-column-mat" class="ui fluid selection dropdown">
                        <input type="hidden" name="gender">
                        <i class="dropdown icon"></i>
                        <div class="default text"></div>
                        <div class="menu">
                        </div>
                    </div>
                </div>
            </div>
            <label style="margin-top: 30px;">Column to base welding</label>
            <div id="tab_column_welds_entry"></div>
        `);

        COMMON_FUNC.fillDropData("welds-column-mat", COMMON_FUNC.weld_mat, data["welds-column-mat"]);
        jQuery("#welds-column-mat").dropdown({ onChange: function (value) { data["welds-column-mat"] = value; } });

        let column_names = ["top flange", "bottom flange", "web"];

        jQuery("#tab_column_welds_entry").empty().append('<div id="welds-input-column" style="width:100%"></div>');

        let width = jQuery("#left-panel").width();
        let columns = [];
        let columns_data = [
            { case: "id", width: 0.1 * width },
            { case: "number", disabled: true, id: "detail", title: "Detail", width: 0.3 * width },
            { case: "dropdown", id: "side", title: "Side", width: 0.3 * width, func: eventColumn },
            { case: "number", disabled: false, id: "weld_throat", title: "Weld throat (" + TAB_PROJECT_DETAILS.options.units_length + ")", width: 0.3 * width, func: eventColumn },
        ];
        for (let i = 0; i < columns_data.length; i++) columns.push(tableRowTemplates(columns_data[i]));
        options.input_table_col = new ENTRY_TABLE({
            'selector': '#welds-input-column',
            'onDelete': function (row_index) { },
            'columns': columns
        });
        let temp = [];
        let count = 1;
        let factor = (TAB_PROJECT_DETAILS.options.units == "metric") ? 25.4 : 1.0;

        for (let i in column_names) {
            temp.push([count, column_names[i], "both", factor * data["default-throat"] ]);
            count++;
        }
        options.input_table_col.setData(temp, false, true);
        data.input_table_col = temp;
        THREE_INIT.genWeldsColumnPart();
    };

    // Box shape
    let initBoxColWeldTable = function () {

        let factor = (TAB_PROJECT_DETAILS.options.units == "metric") ? 25.4 : 1.0;

        jQuery("#welds-tabs-column-box").empty().append(`
            <div class="two fields">
                <div class="field" style="padding-right: 15px;">
                    <label>Steel Material</label>
                    <div id="welds-column-mat" class="ui fluid selection dropdown">
                        <input type="hidden" name="gender">
                        <i class="dropdown icon"></i>
                        <div class="default text"></div>
                        <div class="menu">
                        </div>
                    </div>
                </div>
                <div class="field" style="padding-left: 15px;">
                    <label>Weld throat <sup class="panel-unit-length"></sup></label>
                    <input id="box-weld-throat" type="text" autocomplete="off" value="`+ factor * data["default-throat"] + `">
                </div>
            </div>
        `);
        jQuery("#box-weld-throat").change(e => { 
            data[e.target.id] = e.target.value; 
            THREE_INIT.genWeldsColumnPart();
        });

        COMMON_FUNC.fillDropData("welds-column-mat", COMMON_FUNC.weld_mat, data["welds-column-mat"]);
        jQuery("#welds-column-mat").dropdown({ onChange: function (value) { data["welds-column-mat"] = value; } });
        THREE_INIT.genWeldsColumnPart();
    };

    // Stiffener -------------------------------------------------------------------------------------------------------------------------------------
    let initColStiffTable = function () {

        COMMON_FUNC.fillDropData("welds-stiffener-mat", COMMON_FUNC.weld_mat, data["welds-stiffener-mat"]);
        jQuery("#welds-stiffener-mat").dropdown({ onChange: function (value) { data["welds-stiffener-mat"] = value; } });

        jQuery("#tab_stiffener_welds_entry").empty().append('<div id="welds-input-stiff" style="width:100%"></div>');
        let width = jQuery("#left-panel").width();
        let columns = [];
        let columns_data = [
            { case: "id", width: 0.1 * width },
            { case: "number", disabled: true, id: "detail", title: "Detail", width: 0.3 * width },
            { case: "dropdown", id: "side", title: "Side", width: 0.3 * width, func: eventStiff },
            { case: "number", disabled: false, id: "weld_throat", title: "Weld throat (" + TAB_PROJECT_DETAILS.options.units_length + ")", width: 0.3 * width, func: eventStiff },
        ];
        for (let i = 0; i < columns_data.length; i++) {
            columns.push(tableRowTemplates(columns_data[i]));
        }
        options.input_table_stiff = new ENTRY_TABLE({
            'selector': '#welds-input-stiff',
            'onDelete': function (row_index) { },
            'columns': columns
        });
        temp = [];
        let factor = (TAB_PROJECT_DETAILS.options.units == "metric") ? 25.4 : 1.0;
        for (let i = 0; i < STIFFENER_PART.options.jsx_data.length; i++) {
            temp.push([i + 1, "stiffener s" + (i + 1), "both", factor * data["default-throat"]]);
        }
        options.input_table_stiff.setData(temp, false, true);
        data.input_table_stiff = temp;
        THREE_INIT.genWeldsStiffPart();
    };

    // Shear lug ------------------------------------------------------------------------------------------------------------------------------------
    let initShearLugTable = function () {


        COMMON_FUNC.fillDropData("welds-shear-lug-mat", COMMON_FUNC.weld_mat, data["welds-shear-lug-mat"]);
        jQuery("#welds-shear-lug-mat").dropdown({ onChange: function (value) { data["welds-shear-lug-mat"] = value; } });

        jQuery("#tab_shear_lug_welds_entry").empty().append('<div id="welds-input-shear" style="width:100%"></div>');
        let width = jQuery("#left-panel").width();
        let columns = [];
        let columns_data = [
            { case: "id", width: 0.1 * width },
            { case: "number", disabled: true, id: "detail", title: "Detail", width: 0.3 * width },
            { case: "dropdown", id: "side", title: "Side", width: 0.3 * width, func: eventShearLug },
            { case: "number", disabled: false, id: "weld_throat", title: "Weld throat (" + TAB_PROJECT_DETAILS.options.units_length + ")", width: 0.3 * width, func: eventShearLug },
        ];
        for (let i = 0; i < columns_data.length; i++) {
            columns.push(tableRowTemplates(columns_data[i]));
        }

        options.input_table_shear = new ENTRY_TABLE({
            'selector': '#welds-input-shear',
            'onDelete': function (row_index) { },
            'columns': columns
        });

        let factor = (TAB_PROJECT_DETAILS.options.units == "metric") ? 25.4 : 1.0;

        if (TAB_STEEL_MEMBERS.data["steel-shear-lug-shape"] == "i-shape") {
            options.input_table_shear.setData([
                [1, "top flange", "both", factor * data["default-throat"]],
                [2, "bottom flange", "both", factor * data["default-throat"]],
                [3, "web", "both", factor * data["default-throat"]],
            ], false, true);
            data.input_table_shear = options.input_table_shear.getData(false);
        }
        else if (TAB_STEEL_MEMBERS.data["steel-shear-lug-shape"] == "plate") {
            options.input_table_shear.setData([
                [1, "plate", "both", factor * data["default-throat"]],
            ], false, true);
            data.input_table_shear = options.input_table_shear.getData(false);
        }
        else {
            options.input_table_shear.setData([], false, true);
        }
    };


    let initTables = function (update) {

        if (update) updateBasedOnUnits(TAB_PROJECT_DETAILS.options.length_factor);
        else {
            if (TAB_STEEL_JSX_DRAWINGS.options.sec == "i-shape") {
                initISecColWeldTable(update);
            }
            else if (TAB_STEEL_JSX_DRAWINGS.options.sec == "rectangular") {
                initBoxColWeldTable(update);
            }
            else if (TAB_STEEL_JSX_DRAWINGS.options.sec == "pipe") {
                initBoxColWeldTable(update);
            }
    
            initColStiffTable();
            initShearLugTable(); 
        }
        
    };



    let init = function () {

        jQuery("#welds-tabs").click(function (e) {
            let check = jQuery("#welds-tab-check-jsx").hasClass("active");
            options.stiff_tab_check = check;
            if (check && STIFFENER_PART.options.jsx_data.length > 0) {
                options.jsx_check = true;
                jQuery("#toggle-welds-pos-jsx").append('<div id="welds-tab-jsx" style="width:50%;margin-top: 20px;margin-bottom: 0px;"></div>');
                TAB_WELDS.resizePanel();
            }
            else options.jsx_check = false;
        });

    };


    return {
        init: init,
        options: options,
        data: data,
        resizePanel: resizePanel,
        initTables: initTables,
        initColStiffTable: initColStiffTable,
        initShearLugTable: initShearLugTable,
        updateBasedOnUnits: updateBasedOnUnits
    };


})();