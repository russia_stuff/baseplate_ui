
let GEN_HTML_WELDS = (function () {


    let init = function (id) {

        let div = jQuery("#" + id);
        div.append(`
        <div id="left-panel-welds-box" style="display:none;padding: 20px 50px 0px 50px">
            <!-- TOP TABS -->
            <div id="welds-tabs" class="ui top attached tabular menu">
                <a class="item active" data-tab="welds-tabs-column">Column</a>
                <a class="item" data-tab="welds-tabs-stiffener">Stiffener</a>
                <a class="item" data-tab="welds-tabs-shear-lug">Shear-lug</a>
            </div>
            <!-- Column Tab -->
            <div class="ui bottom attached tab segment active" data-tab="welds-tabs-column">
                <form class="ui form" style="padding: 20px;">
                    <div class="field">
                        <h3>Column to base welding</h3>
                    </div>
                    <div id="welds-tabs-column-box" class="field">
                    </div>
                </form>
            </div>
            <!-- Stiffener Tab -->
            <div id="welds-tab-check-jsx" class="ui bottom attached tab segment" data-tab="welds-tabs-stiffener">
                <form class="ui form" style="padding: 20px;">
                    <div class="field">
                        <h3>Stiffener to column and base welding</h3>
                    </div>
                    <div class="field">
                        <div id="toggle-welds-pos-jsx"></div>
                    </div>
                    <div class="field">
                        <div class="two fields">
                            <div class="field" style="padding-right: 15px;">
                                <label>Steel Material</label>
                                <div id="welds-stiffener-mat" class="ui fluid selection dropdown">
                                    <input type="hidden" name="gender">
                                    <i class="dropdown icon"></i>
                                    <div class="default text"></div>
                                    <div class="menu">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <label style="margin-top: 30px;">Stiffener to column and base welding</label>
                        <div id="tab_stiffener_welds_entry"></div>
                    </div>
                </form>
            </div>
            <!-- Shear-lug Tab -->
            <div class="ui bottom attached tab segment" data-tab="welds-tabs-shear-lug">
                <form class="ui form" style="padding: 20px;">
                    <div class="field">
                        <h3>To base welding</h3>
                    </div>
                    <div class="field">
                        <div class="two fields">
                            <div class="field" style="padding-right: 15px;">
                                <label>Steel Material</label>
                                <div id="welds-shear-lug-mat" class="ui fluid selection dropdown">
                                    <input type="hidden" name="gender">
                                    <i class="dropdown icon"></i>
                                    <div class="default text"></div>
                                    <div class="menu">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <label style="margin-top: 30px;">To base welding</label>
                        <div id="tab_shear_lug_welds_entry"></div>
                    </div>
                </form>
            </div>
        </div>
        `);
    };


    return {
        init: init
    };

})();