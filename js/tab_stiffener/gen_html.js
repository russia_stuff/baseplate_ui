
let GEN_HTML_STIFFENERS_PART = (function () {


    let init = function (id) {

        let div = jQuery("#" + id);
        div.append(`
        <div id="left-panel-stiffener-box" style="display:none;padding: 20px 50px 0px 50px">
            <!-- TOP TABS -->
            <div id="stiffener-tabs" class="ui top attached tabular menu">
                <a class="item active" data-tab="stiffener-tabs-properties">Properties</a>
                <a class="item" data-tab="stiffener-tabs-position">Position</a>
            </div>
            <!-- Anchor Tab -->
            <div class="ui bottom attached tab segment active" data-tab="stiffener-tabs-properties">
                <form class="ui form" style="padding: 20px;">
                    <div class="field">
                        <h3>Stiffener Properties</h3>
                        <div class="w3-container w3-center">
                            <img src="img/stiffener.png" style="width:300px;height:250px" alt="">
                        </div>
                    </div>
                    <!-- Row -->
                    <div class="two fields" style="margin-top: 30px;">
                        <div class="field" style="padding-right: 15px;">
                            <label>Width, a <sup class="panel-unit-length"></sup></label>
                            <input id="stiffener-prop-a" type="text" autocomplete="off">
                        </div>
                        <div class="field" style="padding-left: 15px;">
                            <label>Height, b <sup class="panel-unit-length"></sup></label>
                            <input id="stiffener-prop-b" type="text" autocomplete="off">
                        </div>
                    </div>
                    <!-- Row -->
                    <div class="two fields">
                        <div class="field" style="padding-right: 15px;">
                            <label>Thickness, t <sup class="panel-unit-length"></sup></label>
                            <input id="stiffener-prop-t" type="text" autocomplete="off">
                        </div>
                        <div class="field" style="padding-left: 15px;">
                            <label>Steel Material</label>
                            <div id="stiffener-prop-material" class="ui fluid selection dropdown">
                                <input type="hidden" name="gender">
                                <i class="dropdown icon"></i>
                                <div class="default text"></div>
                                <div class="menu">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Row -->
                    <div class="two fields">
                        <div class="field" style="padding-right: 15px;">
                            <label>a,top <sup class="panel-unit-length"></sup></label>
                            <input id="stiffener-prop-atop" type="text" autocomplete="off">
                        </div>
                        <div class="field" style="padding-left: 15px;">
                            <label>b,top <sup class="panel-unit-length"></sup></label>
                            <input id="stiffener-prop-btop" type="text" autocomplete="off">
                        </div>
                    </div>
                    <!-- Row -->
                    <div class="two fields">
                        <div class="field" style="padding-right: 15px;">
                            <label>a,bot <sup class="panel-unit-length"></sup></label>
                            <input id="stiffener-prop-abot" type="text" autocomplete="off">
                        </div>
                        <div class="field" style="padding-left: 15px;">
                            <label>b,bot <sup class="panel-unit-length"></sup></label>
                            <input id="stiffener-prop-bbot" type="text" autocomplete="off">
                        </div>
                    </div>
                </form>
            </div>
            <!-- Position Tab -->
            <div class="ui bottom attached tab segment" data-tab="stiffener-tabs-position">
                <form class="ui form" style="padding: 20px;">
                    <div class="field">
                        <h3>Stiffener Position</h3>
                        <div id="stiffener-position-guide" class="w3-container w3-center"></div>
                    </div>
                    <div id="stiffener-position-input-box">

                    </div>
                </form>
            </div>
        </div>
        `);
    };


    return {
        init: init
    };

})();