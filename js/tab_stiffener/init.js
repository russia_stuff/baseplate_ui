



let TAB_STIFFENERS = (function () {

    let options = {

    };


    let data = {
        "stiffener-prop-a": "2.55906",
        "stiffener-prop-b": "2.55906",
        "stiffener-prop-t": "0.393701",
        "stiffener-prop-material": "A36",
        "stiffener-prop-atop": "2.55906",
        "stiffener-prop-btop": "2.55906",
        "stiffener-prop-abot": "0",
        "stiffener-prop-bbot": "0"
    };


    let updateBasedOnUnits = function (length_factor) {

        data["stiffener-prop-a"] = parseFloat(data["stiffener-prop-a"]) * length_factor;
        data["stiffener-prop-b"] = parseFloat(data["stiffener-prop-b"]) * length_factor;
        data["stiffener-prop-t"] = parseFloat(data["stiffener-prop-t"]) * length_factor;
        data["stiffener-prop-atop"] = parseFloat(data["stiffener-prop-atop"]) * length_factor;
        data["stiffener-prop-btop"] = parseFloat(data["stiffener-prop-btop"]) * length_factor;
        data["stiffener-prop-abot"] = parseFloat(data["stiffener-prop-abot"]) * length_factor;
        data["stiffener-prop-bbot"] = parseFloat(data["stiffener-prop-bbot"]) * length_factor;

        jQuery("#stiffener-prop-a").val(data["stiffener-prop-a"].toFixed(2));
        jQuery("#stiffener-prop-b").val(data["stiffener-prop-b"].toFixed(2));
        jQuery("#stiffener-prop-t").val(data["stiffener-prop-t"].toFixed(2));
        jQuery("#stiffener-prop-atop").val(data["stiffener-prop-atop"].toFixed(2));
        jQuery("#stiffener-prop-btop").val(data["stiffener-prop-btop"].toFixed(2));
        jQuery("#stiffener-prop-abot").val(data["stiffener-prop-abot"].toFixed(2));
        jQuery("#stiffener-prop-bbot").val(data["stiffener-prop-bbot"].toFixed(2));
    };

    let setInputbasedSection = function (update) {

        if (!update) {
            if (TAB_STEEL_MEMBERS.data["steel-column-shape"] == "pipe") {
                // jQuery("#stiffener-position-guide").empty().append("<img src='img/stiffener-pipe.png' style='width:50%;' alt=''>");
                jQuery("#stiffener-position-guide").empty().append("<img src='img/stiffener-pipe.png' style='width:220px;height:220px;;' alt=''>");
                jQuery("#stiffener-position-input-box").empty().append(`
                    <!-- Row -->
                    <div class="two fields" style="margin-top: 30px;">
                        <div class="field" style="padding-right: 15px;">
                            <label>Num</label>
                            <input id="stiffener-position-num" value="0" type="number" autocomplete="off">
                        </div>
                        <div class="field" style="padding-left: 15px;">
                            <label>Rotation <sup>(deg.)</sup></label>
                            <input id="stiffener-position-deg" value="0" type="text" autocomplete="off">
                        </div>
                    </div>
                `);
                jQuery("#stiffener-position-num").change(e => { THREE_INIT.genStiffenerPart({ update: false }); });
                jQuery("#stiffener-position-deg").change(e => { THREE_INIT.genStiffenerPart({ update: false }); });
            }
            else if (TAB_STEEL_MEMBERS.data["steel-column-shape"] == "rectangular") {
                // jQuery("#stiffener-position-guide").empty().append("<img src='img/stiffener-box.png' style='width:50%;' alt=''>");
                jQuery("#stiffener-position-guide").empty().append("<img src='img/stiffener-box.png' style='width:220px;height:220px;;' alt=''>");
                jQuery("#stiffener-position-input-box").empty().append(`
                <!-- Row -->
                    <div class="two fields" style="margin-top: 30px;">
                        <div class="field" style="padding-right: 15px;">
                            <label>Top side num.</label>
                            <input id="stiffener-position-topside" value="0" type="number" autocomplete="off">
                        </div>
                        <div class="field" style="padding-left: 15px;">
                            <label>Bot side num.</label>
                            <input id="stiffener-position-botside" value="0" type="number" autocomplete="off">
                        </div>
                    </div>
                    <!-- Row -->
                    <div class="two fields">
                        <div class="field" style="padding-right: 15px;">
                            <label>Left side num.</label>
                            <input id="stiffener-position-leftside" value="0" type="number" autocomplete="off">
                        </div>
                        <div class="field" style="padding-left: 15px;">
                            <label>Right side num.</label>
                            <input id="stiffener-position-rightside" value="0" type="number" autocomplete="off">
                        </div>
                    </div>
                `);
                jQuery("#stiffener-position-topside").change(e => { THREE_INIT.genStiffenerPart({ update: false }); });
                jQuery("#stiffener-position-botside").change(e => { THREE_INIT.genStiffenerPart({ update: false }); });
                jQuery("#stiffener-position-leftside").change(e => { THREE_INIT.genStiffenerPart({ update: false }); });
                jQuery("#stiffener-position-rightside").change(e => { THREE_INIT.genStiffenerPart({ update: false }); });
            }
            else if (TAB_STEEL_MEMBERS.data["steel-column-shape"] == "i-shape") {
                // jQuery("#stiffener-position-guide").empty().append("<img src='img/stiffener-i.png' style='width:60%;' alt=''>");
                jQuery("#stiffener-position-guide").empty().append("<img src='img/stiffener-i.png' style='width:280px;height:220px;' alt=''>");
                jQuery("#stiffener-position-input-box").empty().append(`
                    <!-- Row -->
                    <div class="two fields" style="margin-top: 30px;">
                        <div class="field" style="padding-right: 15px;">
                            <label>Top top num.</label>
                            <input id="stiffener-position-toptop" value="0" type="number" autocomplete="off">
                        </div>
                        <div class="field" style="padding-left: 15px;">
                            <label>Bot bot num.</label>
                            <input id="stiffener-position-botbot" value="0" type="number" autocomplete="off">
                        </div>
                    </div>
                    <!-- Row -->
                    <div class="two fields">
                        <div class="field" style="padding-right: 15px;">
                            <label>Web left num.</label>
                            <input id="stiffener-position-webleft" value="0" type="number" autocomplete="off">
                        </div>
                        <div class="field" style="padding-left: 15px;">
                            <label>Web right num.</label>
                            <input id="stiffener-position-webright" value="0" type="number" autocomplete="off">
                        </div>
                    </div>
                    <!-- Row -->
                    <div class="two fields">
                        <div class="field" style="padding-right: 15px;">
                            <div id="stiffener-position-topleft" class="ui checkbox">
                                <input type="checkbox" name="">
                                <label>Top left</label>
                            </div>
                        </div>
                        <div class="field" style="padding-left: 15px;">
                            <div id="stiffener-position-topright" class="ui checkbox">
                                <input type="checkbox" name="">
                                <label>Top right</label>
                            </div>
                        </div>
                    </div>
                    <!-- Row -->
                    <div class="two fields">
                        <div class="field" style="padding-right: 15px;">
                            <div id="stiffener-position-botleft" class="ui checkbox">
                                <input type="checkbox" name="">
                                <label>Bot left</label>
                            </div>
                        </div>
                        <div class="field" style="padding-left: 15px;">
                            <div id="stiffener-position-botright" class="ui checkbox">
                                <input type="checkbox" name="">
                                <label>Bot right</label>
                            </div>
                        </div>
                    </div>
                `);
                jQuery("#stiffener-position-toptop").change(e => { THREE_INIT.genStiffenerPart({ update: false }); });
                jQuery("#stiffener-position-botbot").change(e => { THREE_INIT.genStiffenerPart({ update: false }); });
                jQuery("#stiffener-position-topleft").checkbox({ onChange: function (e) { THREE_INIT.genStiffenerPart({ update: false }); } });
                jQuery("#stiffener-position-topright").checkbox({ onChange: function (e) { THREE_INIT.genStiffenerPart({ update: false }); } });
                jQuery("#stiffener-position-botleft").checkbox({ onChange: function (e) { THREE_INIT.genStiffenerPart({ update: false }); } });
                jQuery("#stiffener-position-botright").checkbox({ onChange: function (e) { THREE_INIT.genStiffenerPart({ update: false }); } });
                jQuery("#stiffener-position-webleft").change(e => { THREE_INIT.genStiffenerPart({ update: false }); });
                jQuery("#stiffener-position-webright").change(e => { THREE_INIT.genStiffenerPart({ update: false }); });
            }
        }

    };


    let init = function () {

        //  Default data------------------------------------------------------------------------------------

        updateBasedOnUnits(TAB_PROJECT_DETAILS.options.length_factor);
        COMMON_FUNC.fillDropData("stiffener-prop-material", COMMON_FUNC.steel_mat, data["stiffener-prop-material"]);

        jQuery("#stiffener-prop-a").change(function (e) {
            THREE_INIT.genStiffenerPart({ update: false });
            data[e.target.id] = e.target.value;
        });
        jQuery("#stiffener-prop-b").change(function (e) {
            THREE_INIT.genStiffenerPart({ update: false });
            data[e.target.id] = e.target.value;
        });
        jQuery("#stiffener-prop-t").change(function (e) {
            THREE_INIT.genStiffenerPart({ update: false });
            data[e.target.id] = e.target.value;
        });
        jQuery("#stiffener-prop-atop").change(function (e) {
            THREE_INIT.genStiffenerPart({ update: false });
            data[e.target.id] = e.target.value;
        });
        jQuery("#stiffener-prop-btop").change(function (e) {
            THREE_INIT.genStiffenerPart({ update: false });
            data[e.target.id] = e.target.value;
        });
        jQuery("#stiffener-prop-abot").change(function (e) {
            THREE_INIT.genStiffenerPart({ update: false });
            data[e.target.id] = e.target.value;
        });
        jQuery("#stiffener-prop-bbot").change(function (e) {
            THREE_INIT.genStiffenerPart({ update: false });
            data[e.target.id] = e.target.value;
        });

        jQuery("#stiffener-prop-material").dropdown({
            onChange: function (name) {
                PANEL_INFO.options.info_stiff_mat = name;
                PANEL_INFO.init();
            }
        });

    };



    return {
        init: init,
        options: options,
        data: data,
        setInputbasedSection: setInputbasedSection,
        updateBasedOnUnits: updateBasedOnUnits
    };


})();