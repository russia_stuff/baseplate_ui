
let GEN_HTML_ANCHORS = (function () {


    let init = function (id) {

        let div = jQuery("#" + id);
        div.append(`
        <div id="left-panel-anchor-box" style="display:none;padding: 20px 50px 0px 50px">
            <!-- TOP TABS -->
            <div id="anchor-tabs" class="ui top attached tabular menu">
                <a class="item active" data-tab="anchor-tabs-properties">Properties</a>
                <a class="item" data-tab="anchor-tabs-position">Position</a>
            </div>
            <!-- Anchor Tab -->
            <div class="ui bottom attached tab segment active" data-tab="anchor-tabs-properties">
                <form class="ui form" style="padding: 20px;">
                    <div class="field">
                        <h3>Anchor Properties</h3>
                    </div>
                    <!-- Row -->
                    <div class="two fields">
                        <div class="field" style="padding-right: 15px;">
                            <label>Type</label>
                            <div id="anchor-prop-diam" class="ui fluid selection dropdown">
                                <input type="hidden" name="gender">
                                <i class="dropdown icon"></i>
                                <div class="default text"></div>
                                <div class="menu">
                                </div>
                            </div>
                        </div>
                        <div class="field" style="padding-left: 15px;">
                            <label>Steel Material</label>
                            <div id="anchor-prop-material" class="ui fluid selection dropdown">
                                <input type="hidden" name="gender">
                                <i class="dropdown icon"></i>
                                <div class="default text"></div>
                                <div class="menu">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Row -->
                    <div class="two fields">
                        <div class="field" style="padding-right: 15px;">
                            <label>Ending</label>
                            <div id="anchor-prop-ending" class="ui fluid selection dropdown">
                                <input type="hidden" name="gender">
                                <i class="dropdown icon"></i>
                                <div class="default text"></div>
                                <div class="menu">
                                    <div class="item" data-value="straight">Straight</div>
                                    <div class="item" data-value="rectangle">Rectangular</div>
                                    <div class="item" data-value="circular">Circular</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="anchor-prop-guide-img" class="w3-container w3-center"></div>
                    <!-- Row -->
                    <div class="two fields" style="margin-top:15px">
                        <div class="field" style="padding-right: 15px;">
                            <label>Anchor Lemgth, L <sup class="panel-unit-length"></sup></label>
                            <input id="anchor-prop-length" type="text" autocomplete="off">
                        </div>
                        <div id="anchor-prop-sizes-box"></div>
                    </div>
                </form>
            </div>
            <!-- Position Tab -->
            <div class="ui bottom attached tab segment" data-tab="anchor-tabs-position">
                <form class="ui form" style="padding: 20px;">
                    <div class="field">
                        <h3>Anchor Base Position</h3>
                    </div>
                    <div class="field">
                        <label>Input method</label>
                        <div class="ui form">
                            <div id="anchor-input-method-checks" class="grouped fields">
                                <div id="anchot-input-single-set" class="field">
                                    <div class="ui toggle checkbox">
                                        <input type="radio" name="anchor-input-method"
                                            checked="checked">
                                        <label>Single set <i class="info circle icon anchor-pos-method-1-tooltip"
                                                style="color:#D9D9D9" data-html="" data-position="right center"></i></label>
                                    </div>
                                </div>
                                <div id="anchot-input-frame" class="field">
                                    <div class="ui toggle checkbox">
                                        <input type="radio" name="anchor-input-method">
                                        <label>Two point frame <i
                                                class="info circle icon anchor-pos-method-2-tooltip"
                                                style="color:#D9D9D9" data-html='' data-position="right center"></i></label>
                                    </div>
                                </div>
                                <div id="anchot-input-line" class="field">
                                    <div class="ui toggle checkbox">
                                        <input type="radio" name="anchor-input-method">
                                        <label>Two point line <i
                                                class="info circle icon anchor-pos-method-3-tooltip"
                                                style="color:#D9D9D9" data-html='' data-position="right center"></i></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field" style="margin-top:30px">
                        <label>Input Data</label>
                        <div class="w3-bar">
                            <div class="w3-bar-item" style="padding: 0;">
                                <div id="anchor-input-model-point"
                                    class="ui grey basic circular icon button">
                                    <i class="mouse pointer icon"></i>
                                </div>
                            </div>
                            <div class="w3-bar-item" style="padding: 0;">
                                <div id="anchor-input-add-row" class="ui grey basic circular icon button">
                                    <i class="plus icon"></i>
                                </div>
                                <div id="anchor-input-remove-row"
                                    class="ui grey basic circular icon button">
                                    <i class="minus icon"></i>
                                </div>
                            </div>
                        </div>
                        <div id="anchor-input-method-support-inputs">
                        </div>
                    </div>
                    <div class="field">
                        <div id="anchor-input-table" style="width:100%"></div>
                        <div id="anchor-input-convert-box"></div>
                    </div>
                </form>
            </div>
        </div>
        `);
    };


    return {
        init: init
    };

})();