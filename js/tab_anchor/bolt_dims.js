


let TAB_ANCHOR_DIMS = (function () {

    // units metric (mm)
    let bolt_dims = {
        '1/2': {
            d: 13,
            hdd: 26,
            nt: 12,
            wt: 5
        },
        '5/8': {
            d: 16,
            hdd: 31,
            nt: 15,
            wt: 5
        },
        '3/4': {
            d: 19,
            hdd: 37,
            nt: 19,
            wt: 5
        },
        '7/8': {
            d: 22,
            hdd: 42,
            nt: 22,
            wt: 5
        },
        '1': {
            d: 25,
            hdd: 48,
            nt: 25,
            wt: 5
        },
        '1-1/8': {
            d: 29,
            hdd: 53,
            nt: 28,
            wt: 5
        },
        '1-1/4': {
            d: 32,
            hdd: 59,
            nt: 31,
            wt: 5
        },
        '1-3/8': {
            d: 35,
            hdd: 64,
            nt: 34,
            wt: 5
        },
        '1-1/2': {
            d: 38,
            hdd: 70,
            nt: 37,
            wt: 5
        }
    };
    


    return bolt_dims;

})();