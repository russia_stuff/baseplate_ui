


let TAB_ANCHOR_THREE = (function () {

    let options = {
        jsx_coord_single: [],
        jsx_coord_frame: [],
        jsx_coord_line: [],
        active_method: "",
        to_move: [],
        p1_to_move: null,
        frame_clicks: 0,
        line_clicks: 0
    };

    let win_width = null;
    let win_height = null;
    let win_x_pos = null;
    let win_y_pos = null;
    let raycaster = null;
    let mouse = null;
    let temp_anchors = [];



    let drawAnchors = function (set) {

        let factor = (TAB_PROJECT_DETAILS.options.units == "metric") ? 1.0 : 25.4;

        function setSingleAnchor(data) {
            let anchor_group = genAnchor({ input: data.input });
            anchor_group.position.set(data.p1_x, 0, data.p1_y);
            anchor_group.name = data.name;
            THREE_INIT.options.scene.add(anchor_group);
            if (data.name == "anchor_single") options.jsx_coord_single.push([data.p1_x, -data.p1_y]);
            else if (data.name == "anchor_frame") options.jsx_coord_frame.push([data.p1_x, -data.p1_y]);
            else if (data.name == "anchor_line") options.jsx_coord_line.push([data.p1_x, -data.p1_y]);
        }

        function setInternalAnchors(data) {
            let step_x = parseFloat(data.p1_x);
            let step_y = parseFloat(data.p1_y);
            let delta_step_x = parseFloat(data.p2_x) - parseFloat(data.p1_x);
            let delta_step_y = parseFloat(data.p2_y) - parseFloat(data.p1_y);
            let step_x_add = delta_step_x / (data.num + 1);
            let step_y_add = delta_step_y / (data.num + 1);
            for (let inter = 0; inter < data.num; inter++) {
                step_x += step_x_add;
                step_y += step_y_add;
                let anchor_group = genAnchor({ input: data.input });
                anchor_group.position.set(step_x, 0, step_y);
                anchor_group.name = data.name;
                THREE_INIT.options.scene.add(anchor_group);
                if (data.name == "anchor_single") options.jsx_coord_single.push([step_x, -step_y]);
                else if (data.name == "anchor_frame") options.jsx_coord_frame.push([step_x, -step_y]);
                else if (data.name == "anchor_line") options.jsx_coord_line.push([step_x, -step_y]);
            }
        }

        if (set.type == "single") {
            options.jsx_coord_single = [];
            THREE_INIT.childsRemove(["anchor_single"]);
            for (let i in TAB_ANCHORS.options.single_added_anchors) {
                let data = TAB_ANCHORS.options.single_added_anchors[i];
                setSingleAnchor({ input: "table", p1_x: factor * data[1], p1_y: -factor * data[2], name: "anchor_single" });
                THREE_INIT.update();
            }
        }
        else if (set.type == "frame") {
            options.jsx_coord_frame = [];
            THREE_INIT.childsRemove(["anchor_frame"]);
            for (let i in TAB_ANCHORS.options.frame_added_anchors) {
                let data = TAB_ANCHORS.options.frame_added_anchors[i];
                // p1
                setSingleAnchor({ input: "table", p1_x: factor * data[1], p1_y: -factor * data[2], name: "anchor_frame" });
                // p2
                setSingleAnchor({ input: "table", p1_x: factor * data[3], p1_y: -factor * data[2], name: "anchor_frame" });
                // p3
                setSingleAnchor({ input: "table", p1_x: factor * data[3], p1_y: -factor * data[4], name: "anchor_frame" });
                // p4
                setSingleAnchor({ input: "table", p1_x: factor * data[1], p1_y: -factor * data[4], name: "anchor_frame" });
                // Internal X
                let num_x = parseInt(data[5]);
                let num_y = parseInt(data[6]);
                if (num_x > 0) {
                    setInternalAnchors({ p1_x: factor * data[1], p1_y: -factor * data[2], p2_x: factor * data[3], p2_y: -factor * data[2], num: num_x, input: "table", name: "anchor_frame" });
                    setInternalAnchors({ p1_x: factor * data[1], p1_y: -factor * data[4], p2_x: factor * data[3], p2_y: -factor * data[4], num: num_x, input: "table", name: "anchor_frame" });
                }
                if (num_y > 0) {
                    setInternalAnchors({ p1_x: factor * data[1], p1_y: -factor * data[2], p2_x: factor * data[1], p2_y: -factor * data[4], num: num_y, input: "table", name: "anchor_frame" });
                    setInternalAnchors({ p1_x: factor * data[3], p1_y: -factor * data[4], p2_x: factor * data[3], p2_y: -factor * data[2], num: num_y, input: "table", name: "anchor_frame" });
                }
            }
            THREE_INIT.update();
        }
        else if (set.type == "line") {
            options.jsx_coord_line = [];
            THREE_INIT.childsRemove(["anchor_line"]);
            for (let i in TAB_ANCHORS.options.line_added_anchors) {
                let data = TAB_ANCHORS.options.line_added_anchors[i];
                // p1
                if (data[5]) setSingleAnchor({ input: "table", p1_x: factor * data[1], p1_y: -factor * data[2], name: "anchor_line" });
                // p2
                if (data[6]) setSingleAnchor({ input: "table", p1_x: factor * data[3], p1_y: -factor * data[4], name: "anchor_line" });
                // Internal 
                let num = parseInt(data[7]);
                if (num > 0) {
                    setInternalAnchors({ p1_x: factor * data[1], p1_y: -factor * data[2], p2_x: factor * data[3], p2_y: -factor * data[4], num: num, input: "table", name: "anchor_line" });
                }
                THREE_INIT.update();
            }
        }
        MAIN_JSX_DRAWINGS.initBase();
    };


    let resizePanel = function () {
        win_width = jQuery("#right-panel").width();
        win_height = jQuery("#right-panel").height();
        win_x_pos = jQuery("#right-panel").position().left;
        win_y_pos = jQuery("#right-panel").position().top;
    };


    let genBasePlateSurface = function () {
        let length_factor = (TAB_PROJECT_DETAILS.options.units == "imperial") ? 25.4 : 1.0;
        let width = parseFloat(jQuery("#steel-baseplate-width").val()) * length_factor;
        let height = parseFloat(jQuery("#steel-baseplate-height").val()) * length_factor;
        let geometry = new THREE.PlaneGeometry(width, height, 1);
        let material = new THREE.MeshBasicMaterial({ color: "#80dfff", side: THREE.DoubleSide });
        let mesh = new THREE.Mesh(geometry, material);
        mesh.name = "bp_surface";
        mesh.rotation.x = 0.5 * Math.PI;
        mesh.visible = true;
        THREE_INIT.options.scene.add(mesh);
        THREE_INIT.update();
    };


    let genAnchor = function (set) {

        let length_factor = (TAB_PROJECT_DETAILS.options.units == "imperial") ? 25.4 : 1.0;
        let anchor_group = new THREE.Group();
        let bolt_data = TAB_ANCHOR_DIMS[jQuery("#anchor-prop-diam").dropdown("get value")];
        let anchor_length = parseFloat(jQuery("#anchor-prop-length").val()) * length_factor;
        let ending = jQuery("#anchor-prop-ending").dropdown("get value");



        // Top bolt thread ------------------------------------------------------------------------------------------------------------------
        let geometry = new THREE.CylinderGeometry(0.5 * bolt_data.d, 0.5 * bolt_data.d, bolt_data.wt + 1.5 * bolt_data.nt, 20);
        let material = new THREE.MeshBasicMaterial({ color: "#ff9933" });
        let mesh = new THREE.Mesh(geometry, material);
        let pos_y = 0.5 * (bolt_data.wt + 1.5 * bolt_data.nt);
        mesh.position.y = pos_y;
        anchor_group.add(mesh);
        // edge
        let edge = new THREE.LineSegments(new THREE.EdgesGeometry(geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
        edge.position.y = pos_y;
        anchor_group.add(edge);

        // Wharer -----------------------------------------------------------------------------------------------------------------------------
        geometry = new THREE.CylinderGeometry(0.6 * bolt_data.hdd, 0.6 * bolt_data.hdd, bolt_data.wt, 20);
        mesh = new THREE.Mesh(geometry, material);
        pos_y = 0.5 * bolt_data.wt;
        mesh.position.y = pos_y;
        anchor_group.add(mesh);
        // edge
        edge = new THREE.LineSegments(new THREE.EdgesGeometry(geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
        edge.position.y = pos_y;
        anchor_group.add(edge);

        // Nut -----------------------------------------------------------------------------------------------------------------------------------
        geometry = new THREE.CylinderGeometry(0.5 * bolt_data.hdd, 0.5 * bolt_data.hdd, bolt_data.nt, 6);
        mesh = new THREE.Mesh(geometry, material);
        pos_y = 0.52 * bolt_data.nt + bolt_data.wt;
        mesh.position.y = pos_y;
        anchor_group.add(mesh);
        // edge
        edge = new THREE.LineSegments(new THREE.EdgesGeometry(geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
        edge.position.y = pos_y;
        anchor_group.add(edge);

        if (set.input == "hover") return anchor_group;

        // Bot thread -------------------------------------------------------------------------------------------------------------------------------
        geometry = new THREE.CylinderGeometry(0.5 * bolt_data.d, 0.5 * bolt_data.d, anchor_length, 20);
        mesh = new THREE.Mesh(geometry, material);
        pos_y = -0.5 * anchor_length;
        mesh.position.y = pos_y;
        anchor_group.add(mesh);
        // edge
        edge = new THREE.LineSegments(new THREE.EdgesGeometry(geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
        edge.position.y = pos_y;
        anchor_group.add(edge);

        // Ending ------------------------------------------------------------------------------------------------------------------------------------

        if (["rectangle", "circular"].indexOf(ending) != -1) {
            let side_size = parseFloat(jQuery("#anchor-prop-side-dim").val()) * length_factor;
            if (ending == "rectangle") geometry = new THREE.BoxGeometry(side_size, side_size, 0.5 * bolt_data.d);
            else if (ending == "circular") geometry = new THREE.CylinderGeometry(0.5 * side_size, 0.5 * side_size, 0.5 * bolt_data.d, 20);
            mesh = new THREE.Mesh(geometry, material);
            pos_y = -anchor_length - 0.25 * bolt_data.d;
            mesh.position.y = pos_y;
            if (ending == "rectangle") mesh.rotation.x = 0.5 * Math.PI;
            anchor_group.add(mesh);
            // edge
            edge = new THREE.LineSegments(new THREE.EdgesGeometry(geometry), new THREE.LineBasicMaterial({ color: '#404040', linewidth: 1 }));
            edge.position.y = pos_y;
            if (ending == "rectangle") edge.rotation.x = 0.5 * Math.PI;
            anchor_group.add(edge);
        }

        if (set.input == "table") return anchor_group;
    };


    let gentempAnchor = function (set) {

        if (set.type == "single") {
            temp_anchors = [];
            temp_anchors.push(genAnchor({ input: "hover" }));
            temp_anchors[0].name = 'anchor_temp';
            temp_anchors[0].visible = false;
            THREE_INIT.options.scene.add(temp_anchors[0]);
        }
        else if (set.type == "frame") {
            temp_anchors = [];
            let total_num = 4 + 2 * TAB_ANCHORS.options.pick_frame_data.num_x + 2 * TAB_ANCHORS.options.pick_frame_data.num_y;
            for (let i = 0; i < total_num; i++) {
                temp_anchors.push(genAnchor({ input: "hover" }));
                temp_anchors[i].name = 'anchor_temp';
                temp_anchors[i].visible = false;
                THREE_INIT.options.scene.add(temp_anchors[i]);
            }
        }
        else if (set.type == "line") {
            temp_anchors = [];
            let total_num = 2;
            total_num += TAB_ANCHORS.options.pick_line_data.num;
            for (let i = 0; i < total_num; i++) {
                temp_anchors.push(genAnchor({ input: "hover" }));
                temp_anchors[i].name = 'anchor_temp';
                temp_anchors[i].visible = false;
                THREE_INIT.options.scene.add(temp_anchors[i]);
            }
        }
    };

    let onMouseDown = function (e) {

        let factor = (TAB_PROJECT_DETAILS.options.units == "metric") ? 1.0 : 0.0393701;

        if (options.active_method == "single" && e.which === 1) {
            let x = Number((options.to_move[0].x * factor).toFixed(2));
            let y = -Number((options.to_move[0].z * factor).toFixed(2));
            TAB_ANCHORS.options.single_input_table.addRow([++TAB_ANCHORS.options.last_id_single, x, y], true);
            TAB_ANCHORS.options.single_added_anchors["single_anchor_" + TAB_ANCHORS.options.last_id_single] = [TAB_ANCHORS.options.last_id_single, x, y];
            TAB_ANCHOR_THREE.drawAnchors({ type: "single" });
        }
        else if (options.active_method == "frame" && e.which === 1) {
            if (options.frame_clicks == 0) {
                options.frame_clicks++;
            }
            else if (options.frame_clicks == 1) {
                let x1 = Number((options.p1_to_move.x * factor).toFixed(2));
                let y1 = -Number((options.p1_to_move.z * factor).toFixed(2));
                let x2 = Number((options.to_move[1].x * factor).toFixed(2));
                let y2 = -Number((options.to_move[1].z * factor).toFixed(2));
                let num_x = TAB_ANCHORS.options.pick_frame_data.num_x;
                let num_y = TAB_ANCHORS.options.pick_frame_data.num_x;
                TAB_ANCHORS.options.frame_input_table.addRow({ id: ++TAB_ANCHORS.options.last_id_frame, x1: x1, y1: y1, x2: x2, y2: y2, num_x: num_x, num_y: num_y }, true);
                TAB_ANCHORS.options.frame_added_anchors["frame_anchor_" + TAB_ANCHORS.options.last_id_frame] = [TAB_ANCHORS.options.last_id_frame, x1, y1, x2, y2, num_x, num_y];
                options.frame_clicks = 0;
                for (let i = 0; i < temp_anchors.length; i++) temp_anchors[i].visible = false;
                options.to_move = [];
                options.p1_to_move = null;
                TAB_ANCHOR_THREE.drawAnchors({ type: "frame" });
            }
        }
        else if (options.active_method == "line" && e.which === 1) {
            if (options.line_clicks == 0) {
                options.line_clicks++;
            }
            else if (options.line_clicks == 1) {
                let x1 = Number((options.p1_to_move.x * factor).toFixed(2));
                let y1 = -Number((options.p1_to_move.z * factor).toFixed(2));
                let x2 = Number((options.to_move[1].x * factor).toFixed(2));
                let y2 = -Number((options.to_move[1].z * factor).toFixed(2));
                let num = TAB_ANCHORS.options.pick_line_data.num;
                let is_p1 = TAB_ANCHORS.options.pick_line_data.is_p1;
                let is_p2 = TAB_ANCHORS.options.pick_line_data.is_p2;
                TAB_ANCHORS.options.line_input_table.addRow({ id: ++TAB_ANCHORS.options.last_id_line, x1: x1, y1: y1, x2: x2, y2: y2, P1: is_p1, P2: is_p2, num: num }, true);
                TAB_ANCHORS.options.line_added_anchors["line_anchor_" + TAB_ANCHORS.options.last_id_line] = [TAB_ANCHORS.options.last_id_line, x1, y1, x2, y2, is_p1, is_p2, num];
                options.line_clicks = 0;
                for (let i = 0; i < temp_anchors.length; i++) temp_anchors[i].visible = false;
                options.to_move = [];
                options.p1_to_move = null;
                TAB_ANCHOR_THREE.drawAnchors({ type: "line" });
            }
        }
    };


    function singlePick(event) {
        mouse.x = ((event.clientX - win_x_pos) / win_width) * 2 - 1;
        mouse.y = -((event.clientY - win_y_pos) / win_height) * 2 + 1;
        raycaster.setFromCamera(mouse, THREE_INIT.options.camera);
        let intersects = raycaster.intersectObjects(THREE_INIT.options.scene.children);
        if (intersects.length > 0) {
            options.to_move = [];
            temp_anchors[0].visible = true;
            options.to_move.push(intersects[0].point.clone());
            temp_anchors[0].position.copy(options.to_move[0]);
            jQuery("#right-panel").css("cursor", "pointer");
            THREE_INIT.update();
        }
        else {
            temp_anchors[0].visible = false;
            jQuery("#right-panel").css("cursor", "auto");
            THREE_INIT.update();
        }
    }

    function setInternalAnchors(data) {
        let start_index = data.start_index;
        let step_x = data.p1_x;
        let step_y = data.p1_y;
        let delta_step_x = data.p2_x - data.p1_x;
        let delta_step_y = data.p2_y - data.p1_y;
        let step_x_add = delta_step_x / (data.num + 1);
        let step_y_add = delta_step_y / (data.num + 1);
        for (let inter = 0; inter < data.num; inter++) {
            step_x += step_x_add;
            step_y += step_y_add;
            temp_anchors[start_index].visible = true;
            options.to_move.push({ x: step_x, y: options.p1_to_move.y, z: step_y });
            temp_anchors[start_index].position.copy(options.to_move[start_index]);
            start_index++;
        }
    }

    function framePick(event) {

        mouse.x = ((event.clientX - win_x_pos) / win_width) * 2 - 1;
        mouse.y = -((event.clientY - win_y_pos) / win_height) * 2 + 1;
        raycaster.setFromCamera(mouse, THREE_INIT.options.camera);
        let intersects = raycaster.intersectObjects(THREE_INIT.options.scene.children);
        if (options.frame_clicks == 0) {
            if (intersects.length > 0) {
                options.to_move = [];
                temp_anchors[0].visible = true;
                options.to_move.push(intersects[0].point.clone());
                temp_anchors[0].position.copy(options.to_move[0]);
                options.p1_to_move = options.to_move[0];
                jQuery("#right-panel").css("cursor", "pointer");
                THREE_INIT.update();
            }
            else {
                temp_anchors[0].visible = false;
                jQuery("#right-panel").css("cursor", "auto");
                THREE_INIT.update();
            }
        }
        else if (options.frame_clicks == 1) {
            if (intersects.length > 0) {
                options.to_move = [];
                // p1
                temp_anchors[0].visible = true;
                options.to_move.push(options.p1_to_move);
                temp_anchors[0].position.copy(options.p1_to_move);
                // p3
                temp_anchors[1].visible = true;
                options.to_move.push(intersects[0].point.clone());
                temp_anchors[1].position.copy(options.to_move[1]);
                // p2
                temp_anchors[2].visible = true;
                options.to_move.push({ x: options.to_move[1].x, y: options.p1_to_move.y, z: options.p1_to_move.z });
                temp_anchors[2].position.copy(options.to_move[2]);
                // p4
                temp_anchors[3].visible = true;
                options.to_move.push({ x: options.p1_to_move.x, y: options.p1_to_move.y, z: options.to_move[1].z });
                temp_anchors[3].position.copy(options.to_move[3]);
                // Internal
                let last_id = 4;
                if (TAB_ANCHORS.options.pick_frame_data.num_x > 0) {
                    setInternalAnchors({ p1_x: options.p1_to_move.x, p1_y: options.p1_to_move.z, p2_x: options.to_move[1].x, p2_y: options.p1_to_move.z, start_index: last_id, num: TAB_ANCHORS.options.pick_frame_data.num_x });
                    last_id += TAB_ANCHORS.options.pick_frame_data.num_x;
                    setInternalAnchors({ p1_x: options.p1_to_move.x, p1_y: options.to_move[1].z, p2_x: options.to_move[1].x, p2_y: options.to_move[1].z, start_index: last_id, num: TAB_ANCHORS.options.pick_frame_data.num_x });
                    last_id += TAB_ANCHORS.options.pick_frame_data.num_x;
                }
                if (TAB_ANCHORS.options.pick_frame_data.num_y > 0) {
                    last_id = (last_id != 4) ? last_id++ : last_id;
                    setInternalAnchors({ p1_x: options.p1_to_move.x, p1_y: options.p1_to_move.z, p2_x: options.p1_to_move.x, p2_y: options.to_move[1].z, start_index: last_id, num: TAB_ANCHORS.options.pick_frame_data.num_y });
                    last_id += TAB_ANCHORS.options.pick_frame_data.num_y;
                    setInternalAnchors({ p1_x: options.to_move[1].x, p1_y: options.to_move[1].z, p2_x: options.to_move[1].x, p2_y: options.p1_to_move.z, start_index: last_id, num: TAB_ANCHORS.options.pick_frame_data.num_y });
                }
                THREE_INIT.update();
            }
            else {
                for (let i = 0; i < temp_anchors.length; i++) temp_anchors[i].visible = false;
                jQuery("#right-panel").css("cursor", "auto");
                THREE_INIT.update();
            }
        }
    }


    function linePick(event) {

        mouse.x = ((event.clientX - win_x_pos) / win_width) * 2 - 1;
        mouse.y = -((event.clientY - win_y_pos) / win_height) * 2 + 1;
        raycaster.setFromCamera(mouse, THREE_INIT.options.camera);
        let intersects = raycaster.intersectObjects(THREE_INIT.options.scene.children);
        if (options.line_clicks == 0) {
            if (intersects.length > 0) {
                options.to_move = [];
                temp_anchors[0].visible = true;
                options.to_move.push(intersects[0].point.clone());
                temp_anchors[0].position.copy(options.to_move[0]);
                options.p1_to_move = options.to_move[0];
                jQuery("#right-panel").css("cursor", "pointer");
                THREE_INIT.update();
            }
            else {
                temp_anchors[0].visible = false;
                jQuery("#right-panel").css("cursor", "auto");
                THREE_INIT.update();
            }
        }
        else if (options.line_clicks == 1) {
            if (intersects.length > 0) {
                options.to_move = [];
                // p1
                temp_anchors[0].visible = true;
                options.to_move.push(options.p1_to_move);
                temp_anchors[0].position.copy(options.p1_to_move);
                // p3
                temp_anchors[1].visible = true;
                options.to_move.push(intersects[0].point.clone());
                temp_anchors[1].position.copy(options.to_move[1]);
                // Internal
                let last_id = 2;
                if (TAB_ANCHORS.options.pick_line_data.num > 0) {
                    setInternalAnchors({ p1_x: options.p1_to_move.x, p1_y: options.p1_to_move.z, p2_x: options.to_move[1].x, p2_y: options.to_move[1].z, start_index: last_id, num: TAB_ANCHORS.options.pick_line_data.num });
                }
                THREE_INIT.update();
            }
            else {
                for (let i = 0; i < temp_anchors.length; i++) temp_anchors[i].visible = false;
                jQuery("#right-panel").css("cursor", "auto");
                THREE_INIT.update();
            }
        }

    }




    let init = function (set) {

        resizePanel();
        raycaster = new THREE.Raycaster();
        mouse = new THREE.Vector2();
        anchor_temp = new THREE.Group();
        genBasePlateSurface();

        let column = THREE_INIT.options.scene.getObjectByName("column");
        column.children[0].visible = false;
        column.children[1].material.color.setHex(0x404040);
        THREE_INIT.update();

        gentempAnchor({ type: set.method });
        options.active_method = set.method;

        if (set.method == "single") document.getElementById("right-panel").addEventListener("mousemove", singlePick, false);
        else if (set.method == "frame") document.getElementById("right-panel").addEventListener("mousemove", framePick, false);
        else if (set.method == "line") document.getElementById("right-panel").addEventListener("mousemove", linePick, false);
        document.getElementById("right-panel").addEventListener("mousedown", onMouseDown, false);

    };


    let disable = function () {

        let column = THREE_INIT.options.scene.getObjectByName("column");
        column.children[0].visible = true;
        column.children[1].material.color.setHex(0x404040);
        THREE_INIT.update();

        if (options.active_method == "single") document.getElementById("right-panel").removeEventListener("mousemove", singlePick, false);
        else if (options.active_method == "frame") document.getElementById("right-panel").removeEventListener("mousemove", framePick, false);
        else if (options.active_method == "line") document.getElementById("right-panel").removeEventListener("mousemove", linePick, false);
        document.getElementById("right-panel").removeEventListener("mousedown", onMouseDown, false);
        THREE_INIT.childsRemove(['bp_surface', 'anchor_temp']);

        if (options.active_method == "frame") options.frame_clicks = 0;
        else if (options.active_method == "line") options.line_clicks = 0;
        options.active_method = "";
        temp_anchors = [];
    };




    return {
        init: init,
        disable: disable,
        drawAnchors: drawAnchors,
        resizePanel: resizePanel,
        options: options
    };


})();