



let TAB_ANCHORS = (function () {

    let options = {
        id: "anchot-input-single-set",
        input_method: "anchot-input-single-set",
        prev_input_method: "anchot-input-single-set",
        single_input_table: null,
        frame_input_table: null,
        line_input_table: null,
        last_id_single: 0,
        last_id_frame: 0,
        last_id_line: 0,
        single_added_anchors: {},
        frame_added_anchors: {},
        line_added_anchors: {},
        pick_frame_data: { num_x: 0, num_y: 0, },
        pick_line_data: { is_p1: true, is_p2: true, num: 0 },
        is_pick: false
    };


    let data = {
        "anchor-prop-length": 11.81,
        "anchor-prop-side-dim": 2.0,
    };

    let updateBasedOnUnits = function (length_factor) {

        data["anchor-prop-length"] = parseFloat(data["anchor-prop-length"]) * length_factor;
        data["anchor-prop-side-dim"] = parseFloat(data["anchor-prop-side-dim"]) * length_factor;

        jQuery("#anchor-prop-length").val(data["anchor-prop-length"].toFixed(2));
        jQuery("#anchor-prop-side-dim").val(data["anchor-prop-side-dim"].toFixed(2));

        if (Object.keys(options.single_added_anchors).length > 0) {

            let data = Object.values(options.single_added_anchors);
            let index = 0;
            let temp = {};
            for (let i in options.single_added_anchors) {
                data[index][1] = (data[index][1] * length_factor).toFixed(3);
                data[index][2] = (data[index][2] * length_factor).toFixed(3);
                temp[i] = data[index];
                index++;
            }
            options.single_added_anchors = temp;
        }

        if (Object.keys(options.frame_added_anchors).length > 0) {

            let data = Object.values(options.frame_added_anchors)
            let index = 0;
            let temp = {};
            for (let i in options.frame_added_anchors) {
                data[index][1] = (data[index][1] * length_factor).toFixed(3);
                data[index][2] = (data[index][2] * length_factor).toFixed(3);
                data[index][3] = (data[index][3] * length_factor).toFixed(3);
                data[index][4] = (data[index][4] * length_factor).toFixed(3);
                temp[i] = data[index];
                index++;
            }
            options.frame_added_anchors = temp;
        }

        if (Object.keys(options.line_added_anchors).length > 0) {

            let data = Object.values(options.line_added_anchors);
            let index = 0;
            let temp = {};
            for (let i in options.line_added_anchors) {
                data[index][1] = (data[index][1] * length_factor).toFixed(3);
                data[index][2] = (data[index][2] * length_factor).toFixed(3);
                data[index][3] = (data[index][3] * length_factor).toFixed(3);
                data[index][4] = (data[index][4] * length_factor).toFixed(3);
                temp[i] = data[index];
                index++;
            }
            options.line_added_anchors = temp;
        }


        if (options.id == "anchot-input-single-set") {
            genSingleInputTable();
            options.single_input_table.setData(Object.values(options.single_added_anchors), false, true);
        }
        else if (options.id == "anchot-input-frame") {
            genFrameInputTable();
            options.frame_input_table.setData(Object.values(options.frame_added_anchors), false, true);
        }
        else if (options.id == "anchot-input-line") {
            genLineInputTable();
            options.line_input_table.setData(Object.values(options.line_added_anchors), false, true);
        }

    };


    let tableRowTemplates = function (set) {
        if (set.case == "id") {
            return {
                "id": "id",
                "title": "ID",
                //"tooltip": "Where is the rebar measured from?",
                "default_value": 1,
                "type": "integer",
                "cell_type": "input_text",
                "cell_width": set.width,
                "disabled": true,
            };
        }
        else if (set.case == "number") {
            return {
                "id": set.id,
                "title": set.title,
                // "tooltip": "Dead Load",
                "default_value": 0,
                "type": "number",
                "cell_type": "input_text",
                "cell_width": set.width,
                "disabled": false,
                "change": function (value, row_index, col_index) {
                    if (set.type == "single") {
                        let row_data = options.single_input_table.getRowData(row_index, true);
                        options.single_added_anchors["single_anchor_" + row_data.id][col_index] = value;
                        TAB_ANCHOR_THREE.drawAnchors({ type: "single" });
                    }
                    else if (set.type == "frame") {
                        let row_data = options.frame_input_table.getRowData(row_index, true);
                        options.frame_added_anchors["frame_anchor_" + row_data.id][col_index] = value;
                        TAB_ANCHOR_THREE.drawAnchors({ type: "frame" });
                    }
                    else if (set.type == "line") {
                        let row_data = options.line_input_table.getRowData(row_index, true);
                        options.line_added_anchors["line_anchor_" + row_data.id][col_index] = value;
                        TAB_ANCHOR_THREE.drawAnchors({ type: "line" });
                    }
                }
            };
        }
        else if (set.case == "checkbox") {
            return {
                "id": set.id,
                "title": set.title,
                // "tooltip": "Dead Load",
                "default_value": 0,
                "cell_type": "checkbox",
                "cell_width": set.width,
                "disabled": false,
                "change": function (value, row_index, col_index) {
                    if (set.type == "line") {
                        let row_data = options.line_input_table.getRowData(row_index, true);
                        options.line_added_anchors["line_anchor_" + row_data.id][col_index] = value;
                        TAB_ANCHOR_THREE.drawAnchors({ type: "line" });
                    }
                }
            };
        }
        else if (set.case == "delete") {
            return {
                "title": "Delete",
                "id": "delete",
                "tooltip": "",
                "default_value": '<div class="row-delete-btn"><i class="icon large red delete link"></i></div>',
                "cell_type": "text",
                "cell_width": set.width,
                "disabled": false,
            };
        }

    };


    let genSingleInputTable = function () {

        let width = jQuery("#left-panel").width() / 4;
        let columns = [];
        let columns_data = [
            { case: "id", width: width },
            { case: "number", id: "x", title: "Z (" + TAB_PROJECT_DETAILS.options.units_length + ")", width: width, type: "single" },
            { case: "number", id: "y", title: "Y (" + TAB_PROJECT_DETAILS.options.units_length + ")", width: width, type: "single" },
            { case: "delete", width: "auto" }
        ];
        for (let i = 0; i < columns_data.length; i++) {
            columns.push(tableRowTemplates(columns_data[i]));
        }
        options.single_input_table = new ENTRY_TABLE({
            'selector': '#anchor-input-table',
            'onDelete': function (row_index) {
                updateBasedOnRowRemove(options.single_input_table, "last_id_single", true);
                TAB_ANCHOR_THREE.drawAnchors({ type: "single" });
                THREE_INIT.update();
            },
            'columns': columns
        });
    };

    let genFrameInputTable = function () {

        let width = jQuery("#left-panel").width() / 8;
        let columns = [];
        let columns_data = [
            { case: "id", width: width },
            { case: "number", id: "x1", title: "Z1<br>(" + TAB_PROJECT_DETAILS.options.units_length + ")", width: width, type: "frame" },
            { case: "number", id: "y1", title: "Y1<br>(" + TAB_PROJECT_DETAILS.options.units_length + ")", width: width, type: "frame" },
            { case: "number", id: "x2", title: "Z2<br>(" + TAB_PROJECT_DETAILS.options.units_length + ")", width: width, type: "frame" },
            { case: "number", id: "y2", title: "Y2<br>(" + TAB_PROJECT_DETAILS.options.units_length + ")", width: width, type: "frame" },
            { case: "number", id: "num_x", title: "Num.<br>X", width: width, type: "frame" },
            { case: "number", id: "num_y", title: "Num.<br>Y", width: width, type: "frame" },
            { case: "delete", width: width }
        ];
        for (let i = 0; i < columns_data.length; i++) {
            columns.push(tableRowTemplates(columns_data[i]));
        }
        options.frame_input_table = new ENTRY_TABLE({
            'selector': '#anchor-input-table',
            'onDelete': function (row_index) {
                updateBasedOnRowRemove(options.frame_input_table, "last_id_frame", true);
                TAB_ANCHOR_THREE.drawAnchors({ type: "frame" });
                THREE_INIT.update();
                if (options.is_pick) {
                    TAB_ANCHOR_THREE.disable();
                    TAB_ANCHOR_THREE.init({ method: "frame" });
                }
            },
            'columns': columns
        });
    };

    let genLineInputTable = function () {

        let width = jQuery("#left-panel").width() / 9;
        let columns = [];
        let columns_data = [
            { case: "id", width: width },
            { case: "number", id: "x1", title: "Z1<br>(" + TAB_PROJECT_DETAILS.options.units_length + ")", width: width, type: "line" },
            { case: "number", id: "y1", title: "Y1<br>(" + TAB_PROJECT_DETAILS.options.units_length + ")", width: width, type: "line" },
            { case: "number", id: "x2", title: "Z2<br>(" + TAB_PROJECT_DETAILS.options.units_length + ")", width: width, type: "line" },
            { case: "number", id: "y2", title: "Y2<br>(" + TAB_PROJECT_DETAILS.options.units_length + ")", width: width, type: "line" },
            { case: "checkbox", id: "P1", title: "P1", width: width, type: "line" },
            { case: "checkbox", id: "P2", title: "P2", width: width, type: "line" },
            { case: "number", id: "num", title: "Inter.<br>number", width: width, type: "line" },
            { case: "delete", width: width }
        ];
        for (let i = 0; i < columns_data.length; i++) {
            columns.push(tableRowTemplates(columns_data[i]));
        }
        options.line_input_table = new ENTRY_TABLE({
            'selector': '#anchor-input-table',
            'onDelete': function (row_index) {
                updateBasedOnRowRemove(options.line_input_table, "last_id_line", true);
                TAB_ANCHOR_THREE.drawAnchors({ type: "line" });
                THREE_INIT.update();
                if (options.is_pick) {
                    TAB_ANCHOR_THREE.disable();
                    TAB_ANCHOR_THREE.init({ method: "line" });
                }
            },
            'columns': columns
        });
    };


    let disablePick = function () {
        jQuery("#anchor-input-model-point").removeClass("active");
        jQuery("#anchor-input-model-point").removeClass("blue");
        jQuery("#anchor-input-model-point").addClass(" grey");
        TAB_ANCHOR_THREE.disable();
        options.is_pick = false;
        if (["anchot-input-frame", "anchot-input-line"].indexOf(options.id) != -1) {
            jQuery("#anchor-input-method-support-inputs > div").transition('slide top');
        }
    };


    let init = function () {

        jQuery('#anchor-tabs .item').click(function (e) { if (options.is_pick) disablePick(); });

        jQuery("#anchor-input-method-checks>div").change(function () {

            let id = jQuery(this).attr("id");
            options.id = id;
            if (options.is_pick) options.is_pick = false;

            if (id != options.prev_input_method) {
                stopPickingEvent();
                if (id == "anchot-input-single-set") {
                    jQuery("#anchor-input-method-support-inputs").empty();
                    genSingleInputTable();
                    options.single_input_table.setData(Object.values(options.single_added_anchors), false, true);
                }
                else if (id == "anchot-input-frame") {
                    jQuery("#anchor-input-method-support-inputs").empty().append(`
                        <div class="w3-bar transition hidden" style="margin-top: 10px;">
                            <div class="w3-bar-item" style="padding: 0px;">
                                <div class="ui input">
                                    <input id="pick-frame-num-x" value="0" style="width:80px;">
                                </div>
                            </div>
                            <div class="w3-bar-item" style="padding: 0px;margin-left:15px;margin-top:10px">
                                <label>Num. X</label>
                            </div>
                            <div class="w3-bar-item" style="padding: 0px;">
                                <div class="ui input">
                                    <input id="pick-frame-num-y" value="0" style="width:80px;margin-left:15px">
                                </div>
                            </div>
                            <div class="w3-bar-item" style="padding: 0px;margin-left:15px;margin-top:10px">
                                <label>Num. Y</label>
                            </div>
                        </div>
                    `);
                    genFrameInputTable();
                    options.frame_input_table.setData(Object.values(options.frame_added_anchors), false, true);
                    jQuery("#pick-frame-num-x").keyup((e) => {
                        options.pick_frame_data.num_x = parseInt(e.target.value);
                        TAB_ANCHOR_THREE.disable();
                        TAB_ANCHOR_THREE.init({ method: "frame" });
                    });
                    jQuery("#pick-frame-num-y").keyup((e) => {
                        options.pick_frame_data.num_y = parseInt(e.target.value);
                        TAB_ANCHOR_THREE.disable();
                        TAB_ANCHOR_THREE.init({ method: "frame" });
                    });
                }
                else if (id == "anchot-input-line") {
                    jQuery("#anchor-input-method-support-inputs").empty().append(`
                        <div class="w3-bar transition hidden" style="margin-top: 10px;">
                            <div class="w3-bar-item" style="padding: 0px;">
                                <div class="ui checked checkbox" style="margin-top:10px">
                                    <input id="pick-line-check-p1" type="checkbox" checked="">
                                    <label>Anchor P1</label>
                                </div>
                            </div>
                            <div class="w3-bar-item" style="padding: 0px;margin-left:15px;margin-top:10px">
                                <div class="ui checked checkbox">
                                    <input id="pick-line-check-p2" type="checkbox" checked="">
                                    <label>Anchor P2</label>
                                </div>
                            </div>
                            <div class="w3-bar-item" style="padding: 0px;">
                                <div class="ui input">
                                    <input id="pick-line-num" value="0" style="width:80px;margin-left:15px">
                                </div>
                            </div>
                            <div class="w3-bar-item" style="padding: 0px;">
                                <p style="margin-left:10px;margin-top:10px">Num.</p>
                            </div>
                        </div>
                    `);
                    genLineInputTable();
                    options.line_input_table.setData(Object.values(options.line_added_anchors), false, true);
                    jQuery("#pick-line-num").keyup((e) => {
                        options.pick_line_data.num = parseInt(e.target.value);
                        TAB_ANCHOR_THREE.disable();
                        TAB_ANCHOR_THREE.init({ method: "line" });
                    });
                    jQuery("#pick-line-check-p1").change(function (e) { options.pick_line_data.is_p1 = e.target.checked; });
                    jQuery("#pick-line-check-p2").change(function (e) { options.pick_line_data.is_p2 = e.target.checked; });
                }
                jQuery("#" + id).find("i").css("color", "#2185D0");
                jQuery("#" + options.prev_input_method).find("i").css("color", "#D9D9D9");
                options.prev_input_method = id;
            }
            TAB_ANCHOR_THREE.disable();

        });



        // Pick on model functionality -----------------------------------------------------------------------------
        jQuery("#anchor-input-model-point").click(function (e) {
            if (jQuery(this).hasClass("active")) {
                jQuery(this).removeClass("active");
                jQuery(this).removeClass("blue");
                jQuery(this).addClass(" grey");
                TAB_ANCHOR_THREE.disable();
                options.is_pick = false;
            }
            else {
                jQuery(this).addClass(" active");
                jQuery(this).removeClass("grey");
                jQuery(this).addClass(" blue");
                if (options.id == "anchot-input-single-set") TAB_ANCHOR_THREE.init({ method: "single" });
                else if (options.id == "anchot-input-frame") TAB_ANCHOR_THREE.init({ method: "frame" });
                else if (options.id == "anchot-input-line") TAB_ANCHOR_THREE.init({ method: "line" });
                options.is_pick = true;
            }
            if (["anchot-input-frame", "anchot-input-line"].indexOf(options.id) != -1) {
                jQuery("#anchor-input-method-support-inputs > div").transition('slide top');
            }
        });


        // Add Remove Rows -----------------------------------------------------------------------------------------

        jQuery("#anchor-input-add-row").click(e => {
            if (options.prev_input_method == "anchot-input-single-set") {
                options.last_id_single++;
                options.single_input_table.addRow({ id: options.last_id_single, x: 0, y: 0 }, true);
                options.single_added_anchors["single_anchor_" + options.last_id_single] = [options.last_id_single, 0, 0];
                TAB_ANCHOR_THREE.drawAnchors({ type: "single" });
            }
            else if (options.prev_input_method == "anchot-input-frame") {
                options.last_id_frame++;
                options.frame_input_table.addRow({ id: options.last_id_frame, x1: 0, y1: 0, x2: 0, y2: 0, num_x: 0, num_y: 0 }, true);
                options.frame_added_anchors["frame_anchor_" + options.last_id_frame] = [options.last_id_frame, 0, 0, 0, 0, 0, 0];
                TAB_ANCHOR_THREE.drawAnchors({ type: "frame" });
            }
            else if (options.prev_input_method == "anchot-input-line") {
                options.last_id_line++;
                options.line_input_table.addRow({ id: options.last_id_line, x1: 0, y1: 0, x2: 0, y2: 0, P1: true, P2: true, num: 0 }, true);
                options.line_added_anchors["line_anchor_" + options.last_id_line] = [options.last_id_line, 0, 0, 0, 0, true, true, 0];
                TAB_ANCHOR_THREE.drawAnchors({ type: "line" });
            }
        });

        jQuery("#anchor-input-remove-row").click(e => {
            if (options.prev_input_method == "anchot-input-single-set") {
                updateBasedOnRowRemove(options.single_input_table, "last_id_single", false);
                TAB_ANCHOR_THREE.drawAnchors({ type: "single" });
                THREE_INIT.update();
            }
            else if (options.prev_input_method == "anchot-input-frame") {
                updateBasedOnRowRemove(options.frame_input_table, "last_id_frame", false);
                TAB_ANCHOR_THREE.drawAnchors({ type: "frame" });
                THREE_INIT.update();
                if (options.is_pick) {
                    TAB_ANCHOR_THREE.disable();
                    TAB_ANCHOR_THREE.init({ method: "frame" });
                }
            }
            else if (options.prev_input_method == "anchot-input-line") {
                updateBasedOnRowRemove(options.line_input_table, "last_id_line", false);
                TAB_ANCHOR_THREE.drawAnchors({ type: "line" });
                THREE_INIT.update();
                if (options.is_pick) {
                    TAB_ANCHOR_THREE.disable();
                    TAB_ANCHOR_THREE.init({ method: "line" });
                }
            }
        });


        // Default --------------------------------------------------------------------------------------------------
        genSingleInputTable();
        jQuery("#" + options.prev_input_method).find("i").css("color", "#2185D0");
        options.single_input_table.setData([], true, true);
        updateBasedOnUnits(TAB_PROJECT_DETAILS.options.length_factor);

        options.single_input_table.setData([
            [1, "130", "130"],
            [2, "-130", "130"],
            [3, "-130", "-130"],
            [4, "130", "-130"]
        ], false);

        options.single_added_anchors = {
            single_anchor_1: [1, "130", "130"],
            single_anchor_2: [2, "-130", "130"],
            single_anchor_3: [3, "-130", "-130"],
            single_anchor_4: [4, "130", "-130"]
        };

        options.last_id_single = 4;

        // Tooltips --------------------------------------------------------------------------------------------------

        function initTooltips(class_id, img_name, width) {
            let tooltip = $('.' + class_id);
            tooltip.attr('data-html', '<img style="max-width: ' + width + 'px" src="' + ANCHOR_TAB_BASE_64[img_name] + '">');
            tooltip.unbind().popup();
        }

        initTooltips('anchor-pos-method-1-tooltip', 'method_1', '300');
        initTooltips('anchor-pos-method-2-tooltip', 'method_2', '500');
        initTooltips('anchor-pos-method-3-tooltip', 'method_3', '500');


        // Anchor properties -----------------------------------------------------------------------------------------

        function redrawAnchors() {
            TAB_ANCHOR_THREE.drawAnchors({ type: "single" });
            TAB_ANCHOR_THREE.drawAnchors({ type: "frame" });
            TAB_ANCHOR_THREE.drawAnchors({ type: "line" });
            THREE_INIT.update();
        }

        COMMON_FUNC.fillDropData("anchor-prop-diam", COMMON_FUNC.anchor_names, "5/8");
        COMMON_FUNC.fillDropData("anchor-prop-material", COMMON_FUNC.bolt_mat, 'A325');
        jQuery("#anchor-prop-ending").dropdown("set selected", "straight");
        jQuery("#anchor-prop-guide-img").empty().append('<img style="width:80%" src="' + ANCHOR_TAB_BASE_64.anchor_type_straight + '" />');
        jQuery('#anchor-prop-length').on('change', redrawAnchors);


        jQuery("#anchor-prop-ending").dropdown({
            onChange: function (value) {
                let content = '';

                if (value == "straight") {
                    jQuery("#anchor-prop-guide-img").empty().append('<img style="width:80%" src="' + ANCHOR_TAB_BASE_64.anchor_type_straight + '" />');
                    jQuery("#anchor-prop-sizes-box").empty();
                }
                else if (value == "circular") {
                    jQuery("#anchor-prop-guide-img").empty().append('<img style="width:80%" src="' + ANCHOR_TAB_BASE_64.anchor_type_circular + '" />');
                    content += `
                        <div class="field" style="padding-left: 15px;">
                            <label>Side Length <sup>(`+ TAB_PROJECT_DETAILS.options.units_length + `)</sup></label>
                            <input id="anchor-prop-side-dim" type="text" autocomplete="off">
                        </div>
                    `;
                    jQuery("#anchor-prop-sizes-box").empty().append(content);
                    jQuery("#anchor-prop-side-dim").val(data["anchor-prop-side-dim"].toFixed(2));
                }
                else if (value == "rectangle") {
                    jQuery("#anchor-prop-guide-img").empty().append('<img style="width:80%" src="' + ANCHOR_TAB_BASE_64.anchor_type_rectangular + '" />');
                    content += `
                        <div class="field" style="padding-left: 15px;">
                            <label>Side Length <sup>(`+ TAB_PROJECT_DETAILS.options.units_length + `)</sup></label>
                            <input id="anchor-prop-side-dim" type="text" autocomplete="off">
                        </div>
                    `;
                    jQuery("#anchor-prop-sizes-box").empty().append(content);
                    jQuery("#anchor-prop-side-dim").val(data["anchor-prop-side-dim"].toFixed(2));
                }
                redrawAnchors();
                jQuery('#anchor-prop-side-dim').on('change', redrawAnchors);
                // TAB_ANCHORS.updateBasedOnUnits(TAB_PROJECT_DETAILS.options.length_factor);
            }
        });

        jQuery("#anchor-prop-diam").dropdown({
            onChange: function (dim) {
                redrawAnchors();
                PANEL_INFO.options.info_anchor_name = dim;
                PANEL_INFO.init();
            }
        });

        jQuery("#anchor-prop-material").dropdown({
            onChange: function (name) {
                PANEL_INFO.options.info_anchor_mat = name;
                PANEL_INFO.init();
            }
        });

        jQuery("#anchor-prop-length").val(data["anchor-prop-length"]);

    };


    let stopPickingEvent = function () {
        if (jQuery("#anchor-input-model-point").hasClass("active")) {
            jQuery("#anchor-input-model-point").removeClass("active");
            jQuery("#anchor-input-model-point").removeClass("blue");
            jQuery("#anchor-input-model-point").addClass(" grey");
            if (["anchot-input-frame", "anchot-input-line"].indexOf(options.id) != -1) {
                jQuery("#anchor-input-method-support-inputs > div").transition('slide top');
            }
        }
    };


    let updateBasedOnRowRemove = function (table, last_id, row_delete) {
        if (row_delete) {
            let temp = table.getData(false);
            if (last_id == "last_id_single") {
                options.single_added_anchors = {};
                for (let i = 0; i < temp.length; i++) {
                    options.single_added_anchors["single_anchor_" + temp[i][0]] = temp[i];
                    if (i == temp.length - 1) options[last_id] = temp[i][0];
                }
            }
            else if (last_id == "last_id_frame") {
                options.frame_added_anchors = {};
                for (let i = 0; i < temp.length; i++) {
                    options.frame_added_anchors["frame_anchor_" + temp[i][0]] = temp[i];
                    if (i == temp.length - 1) options[last_id] = temp[i][0];
                }
            }
            else if (last_id == "last_id_line") {
                options.line_added_anchors = {};
                for (let i = 0; i < temp.length; i++) {
                    options.line_added_anchors["line_anchor_" + temp[i][0]] = temp[i];
                    if (i == temp.length - 1) options[last_id] = temp[i][0];
                }
            }
            if (temp.length == 0) options[last_id] = 0;
        }
        else {
            if (last_id == "last_id_single") options.single_added_anchors = _.omit(options.single_added_anchors, "single_anchor_" + options.last_id_single);
            else if (last_id == "last_id_frame") options.frame_added_anchors = _.omit(options.frame_added_anchors, "frame_anchor_" + options.last_id_frame);
            else if (last_id == "last_id_line") options.line_added_anchors = _.omit(options.line_added_anchors, "line_anchor_" + options.last_id_line);
            let temp_1 = [];
            let temp_2 = table.getData(true);
            for (let i = 0; i < temp_2.length - 1; i++) {
                temp_1.push(temp_2[i]);
                if (i == temp_2.length - 2) options[last_id] = temp_2[i].id;
            }
            table.setData(temp_1, true, true);
            if (temp_1.length == 0) options[last_id] = 0;
        }
    };



    return {
        init: init,
        updateBasedOnUnits: updateBasedOnUnits,
        options: options,
        data: data,
        disablePick: disablePick
    };


})();