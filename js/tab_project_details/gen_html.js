


let GEN_HTML_PROJECT_DETAILS = (function () {


    let init = function (id) {

        let div = jQuery("#" + id);
        div.append(`
        <div id="left-panel-project-details-box" style="display:block;padding: 20px 50px 0px 50px">
            <form class="ui form" style="">
                <div class="field">
                    <h3>Project Details</h3>
                </div>
                <div class="field" style="margin-top: 15px;">
                    <!-- Row -->
                    <div class="two fields">
                        <div class="field" style="padding-right: 15px;">
                            <label>Project Units</label>
                            <div id="project-units" class="ui fluid selection dropdown">
                                <input type="hidden" name="gender">
                                <i class="dropdown icon"></i>
                                <div class="default text"></div>
                                <div class="menu">
                                    <div class="item" data-value="imperial">Imperial</div>
                                    <div class="item" data-value="metric">Metric</div>
                                </div>
                            </div>
                        </div>
                        <div class="field" style="padding-left: 15px;">
                            <label>Project Name</label>
                            <input type="text" name="last-name" placeholder="Project Name">
                        </div>
                    </div>
                    <!-- Row -->
                    <div class="two fields" style="margin-top: 30px;">
                        <div class="field" style="padding-right: 15px;">
                            <label>Company</label>
                            <input type="text" name="first-name" placeholder="Company">
                        </div>
                        <div class="field" style="padding-left: 15px;">
                            <label>Designer</label>
                            <input type="text" name="last-name" placeholder="Designer">
                        </div>
                    </div>
                    <!-- Row -->
                    <div class="two fields" style="margin-top: 30px;">
                        <div class="field" style="padding-right: 15px;">
                            <label>Project ID</label>
                            <input type="text" name="first-name" placeholder="Project ID">
                        </div>
                        <div class="field" style="padding-left: 15px;">
                            <label>Client</label>
                            <input type="text" name="last-name" placeholder="Client">
                        </div>
                    </div>
                    <!-- Row -->
                    <div class="field" style="margin-top: 30px;">
                        <label>Project Notes</label>
                        <textarea rows="5" placeholder="Use this area for project notes"></textarea>
                    </div>
                </div>
            </form>
        </div>
        `);
    };


    return {
        init: init
    };

})();