


let TAB_PROJECT_DETAILS = (function(){

    let options = {
        units:'metric',
        units_length: "mm",
        axial_f_units: "kN",
        bending_f_units: "kN-m",
        length_factor: 25.4,
        to_mm: 1.0,
        to_in: 1.0,
        kNm_Kipft: 1.0,
        kN_Kip: 1.0
    };


    let init = function() {

        jQuery("#project-units").dropdown();
        jQuery("#project-units").dropdown("set selected", "metric");

        jQuery("#project-units").dropdown({
            onChange:function(units){
                options.units = units;
                if (units == "metric") {
                    options.units_length = "mm";
                    options.axial_f_units = "kN";
                    options.bending_f_units = "kN-m";
                    options.length_factor = 25.4;
                    options.to_mm = 1.0;
                    options.to_in = 0.0393701;
                    options.kNm_Kipft = 1 / 0.737561919672895;
                    options.kN_Kip = 1 / 224.808873116298;
                }
                else if (units == "imperial") {
                    options.units_length = "in";
                    options.axial_f_units = "Ibf";
                    options.bending_f_units = "Kip-ft";
                    options.length_factor = 0.0393701;
                    options.to_mm = 25.4;
                    options.to_in = 1.0;
                    options.kNm_Kipft = 0.737561919672895;
                    options.kN_Kip = 224.808873116298;
                }
                jQuery(".panel-unit-length").html(`(${options.units_length})`);
                PANEL_INFO.options.info_units = options.units;
  
                TAB_STEEL_MEMBERS.updateBasedOnUnits(options.length_factor);
                TAB_ANCHORS.updateBasedOnUnits(options.length_factor);
                TAB_STIFFENERS.updateBasedOnUnits(options.length_factor);
                // TAB_WELDS.updateBasedOnUnits(options.length_factor);
                TAB_LOADS.updateBasedOnUnits();
            }
        });

        jQuery(".panel-unit-length").html(`(${options.units_length})`);
        PANEL_INFO.options.info_units = options.units;
    };


    return {
        init:init,
        options:options
    };

})();