
let GEN_HTML_LOADS = (function () {


    let init = function (id) {

        let div = jQuery("#" + id);
        div.append(`
        <div id="left-panel-loads-box" style="display:none;padding: 20px 50px 0px 50px">
            <!-- TOP TABS -->
            <div id="loads-tabs" class="ui top attached tabular menu">
                <a class="item active" data-tab="loads-tabs-column">Loads</a>
            </div>
            <!-- Column Tab -->
            <div class="ui bottom attached tab segment active" data-tab="loads-tabs-column">
                <form class="ui form" style="padding: 20px;">
                    <div class="field">
                        <h3>Column section loads</h3>
                    </div>

                    <div class="field" style="margin-top:30px">
                        <div class="w3-bar">
                            <div class="w3-bar-item" style="padding: 0;">
                                <div id="loads-add-row" class="ui grey basic circular icon button">
                                    <i class="plus icon"></i>
                                </div>
                                <div id="loads-remove-row"
                                    class="ui grey basic circular icon button">
                                    <i class="minus icon"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="loads-table" style="margin-top: 0px;"></div>

                </form>
            </div>
        </div>
        `);
    };


    return {
        init: init
    };

})();