


let TAB_LOADS = (function () {

    let options = {
        input_table_loads: null,
        last_row_id: 1
    };

    let loadCaseNames = {
        "American": ['DL','LL','W', 'E', 'LR', 'RL', 'SL']
    };

    let updateBasedOnUnits = function () {

        let data = options.input_table_loads.getData(false);
        init();
        options.input_table_loads.setData([], false);
        let axi_f = TAB_PROJECT_DETAILS.options.kN_Kip;
        let ben_f = TAB_PROJECT_DETAILS.options.kNm_Kipft;
        for (let i = 0; i < data.length; i++) {
            options.input_table_loads.addRow([data[i][0], data[i][1] * axi_f, data[i][2] * ben_f, data[i][3] * ben_f, data[i][4] * axi_f, data[i][5] * axi_f, data[i][6]], false, true);
        }
    };


    let tableRowTemplates = function (set) {
        if (set.case == "id") {
            return {
                "id": "lc",
                "title": "LC",
                //"tooltip": "Where is the rebar measured from?",
                "default_value": 1,
                "type": "integer",
                "cell_type": "input_text",
                "cell_width": set.width,
                "disabled": true,
            };
        }
        else if (set.case == "number") {
            return {
                "id": set.id,
                "title": set.title,
                // "tooltip": "Dead Load",
                "default_value": 0,
                "type": "number",
                "cell_type": "input_text",
                "cell_width": set.width,
                "disabled": set.disabled,
                "change": set.func
            };
        }
        else if (set.case == "dropdown") {
            return {
                "id": set.id,
                "title": set.title,
                //"tooltip": "Where is the rebar measured from?",
                "default_value": "both",
                "type": "string",
                "cell_type": "dropdown",
                "cell_dropdown_values": loadCaseNames[DESIGN_CODE],
                "cell_dropdown_labels": loadCaseNames[DESIGN_CODE],
                "cell_width": set.width,
                "disabled": false,
                "change": set.func,
            };
        }
        else if (set.case == "delete") {
            return {
                "title": "Del.",
                "id": "delete",
                "tooltip": "",
                "default_value": '<div class="row-delete-btn"><i class="icon large red delete link"></i></div>',
                "cell_type": "text",
                "cell_width": set.width,
                "disabled": false,
            };
        }

    };

    let eventFunc = function (value, row_index, col_index) {

        THREE_INIT.genLoadsPart(options.input_table_loads.getRowData(row_index, true));

        if (value == '0' || isNaN(Number(value))) { return; }
        else if (row_index == options.last_row_id - 1) {
            options.input_table_loads.addRow([++options.last_row_id, 0, 0, 0, 0, 0, "DL"]);
            addRowClickEvent();
        }
    };


    let updateOnPatse = function () {

        THREE_INIT.genLoadsPart({ id: 0, Mx: 0, My: 0, Vx: 0, Vy: 0, Nx: 0, load_type: "DL" });

        let temp = options.input_table_loads.getData(true);
        let lc_index = 1;
        for (let i = 0; i < temp.length; i++) {
            temp[i].lc = lc_index;
            lc_index++;
        }
        options.input_table_loads.setData(temp, true, true);
        options.last_row_id = lc_index - 1;
        addRowClickEvent();
    };


    let init = function () {

        let width = jQuery("#left-panel").width();
        let columns = [];
        let columns_data = [
            { case: "id", width: 0.1 * width },
            { case: "number", disabled: false, id: "Nx", title: "N<sub>X</sub><br><span style='font-size:11px'>(" + TAB_PROJECT_DETAILS.options.axial_f_units + ")</span>", width: 0.133 * width, func: eventFunc },
            { case: "number", disabled: false, id: "Mx", title: "M<sub>Z</sub><br><span style='font-size:11px'>(" + TAB_PROJECT_DETAILS.options.bending_f_units + ")</span>", width: 0.133 * width, func: eventFunc },
            { case: "number", disabled: false, id: "My", title: "M<sub>Y</sub><br><span style='font-size:11px'>(" + TAB_PROJECT_DETAILS.options.bending_f_units + ")</span>", width: 0.133 * width, func: eventFunc },
            { case: "number", disabled: false, id: "Vx", title: "V<sub>Z</sub><br><span style='font-size:11px'>(" + TAB_PROJECT_DETAILS.options.axial_f_units + ")</span>", width: 0.133 * width, func: eventFunc },
            { case: "number", disabled: false, id: "Vy", title: "V<sub>Y</sub><br><span style='font-size:11px'>(" + TAB_PROJECT_DETAILS.options.axial_f_units + ")</span>", width: 0.133 * width, func: eventFunc },
            { case: "dropdown", disabled: false, id: "load_type", title: "Load<br>type", width: 0.133 * width, func: eventFunc },
            { case: "delete", width: 0.1 * width }
        ];
        for (let i = 0; i < columns_data.length; i++) columns.push(tableRowTemplates(columns_data[i]));
        options.input_table_loads = new ENTRY_TABLE({
            'selector': '#loads-table',
            'onDelete': function (row_index) {
                options.last_row_id--;
                THREE_INIT.genLoadsPart({ id: 0, Mx: 0, My: 0, Vx: 0, Vy: 0, Nx: 0, load_type: "DL" });
                if (options.last_row_id == 0) jQuery('#loads-table').find('tr').off();
            },
            'copy_paste': true,
            'selectable': true,
            'onPaste': function () { updateOnPatse(); },
            'columns': columns
        });

        addRowClickEvent();

        jQuery("#loads-add-row").on("click", e => {
            options.input_table_loads.addRow([++options.last_row_id, 0, 0, 0, 0, 0, "DL"]);
            addRowClickEvent();
        });

        jQuery("#loads-remove-row").on("click", e => {
            let temp = options.input_table_loads.getData(false);
            if (temp.length != 0) {
                options.last_row_id--;
                options.input_table_loads.setData(temp.map((v, i) => { if (i != temp.length - 1) return v; }), false, true);
                if (options.last_row_id == 0) jQuery('#loads-table').find('tr').off();
            }

        });
    };

    let addRowClickEvent = function () {
        jQuery('#loads-table').find('tr').click(function () {
            let row_index = $(this).index();
            THREE_INIT.genLoadsPart(options.input_table_loads.getRowData(row_index, true));
        });
    };

    return {
        init: init,
        options: options,
        updateBasedOnUnits: updateBasedOnUnits
    };

})();