


let VALID = (function () {

    let options = {};


    let checkInput = function (input_id, type, box_id) {

        // type:
        // 1: Numeric, > 0
        // 2: Numeric, >= 0
        // 3: special cases

        let error = false;
        let error_msg = "";
        let val = $("#" + input_id).val();

        if (isNaN(Number(val))) {
            error = true;
            error_msg = "The input value is not numeric.";
        }
        if (type == 1) {
            if (parseFloat(Number(val)) <= 0) {
                error = true;
                error_msg = "The input value is equal or less than zero.";
            }
        }
        else if (type == 2) {
            if (parseFloat(Number(val)) < 0) {
                error = true;
                error_msg = "The input value is less than zero.";
            }
        }

        if (error) $("#" + input_id).addClass(" input-error");
        else $("#" + input_id).removeClass("input-error");


        
        if (error) {
            $("#" + box_id).empty();
            $("#" + box_id).append(`
                <div class="ui negative message" style="margin-left: 50px; margin-right: 50px; margin-bottom:15px;margin-top:20px">
                <i class="close icon"></i>
                <div class="header">Input Data Error!</div>
                <p>${error_msg}</p></div>
            `);
            $('.message .close').on('click', function () {
                $(this).closest('.message').transition('fade');
            });
        }
        else {
            $("#" + box_id).empty();
        }
        

        
    };




    return {
        checkInput: checkInput
    };

})();