

let TOP_MENU_FUNC = (function () {

    let options = {
        prev_clicked_step: "top-menu-btns-details",
    };
    let selected_id = "";

    let btn_ids = {
        "top-menu-btns-details": "left-panel-project-details-box",
        "top-menu-btns-steel": "left-panel-steel-box",
        "top-menu-btns-anchor": "left-panel-anchor-box",
        "top-menu-btns-stiffener": "left-panel-stiffener-box",
        "top-menu-btns-welds": "left-panel-welds-box",
        "top-menu-btns-loads": "left-panel-loads-box"
    };

    let init = function () {

        jQuery('.menu .item').tab();

        // Top menu buttons css --------------------------------------------------------------------------------------------------------

        jQuery("#top-menu-btns-details").addClass(" clicked-step");

        let prev_selected_id = {
            "top-menu-btns-steel": "top-menu-btns-details",
            "top-menu-btns-anchor": "top-menu-btns-steel",
            "top-menu-btns-stiffener": "top-menu-btns-anchor",
            "top-menu-btns-welds": "top-menu-btns-stiffener",
            "top-menu-btns-loads": "top-menu-btns-welds",
            "top-menu-btns-analysis": "top-menu-btns-loads",
        };

        function stepMouseOver(set) {
            jQuery(set).css({
                "border-top": "1px solid #EDF0F2",
                "border-bottom": "1px solid #EDF0F2",
                "border-right": "1px solid #EDF0F2",
                "border-right": "1px solid #EDF0F2",
                "border-left": "1px solid #EDF0F2",
                "color": "#EDF0F2",
                "transition": "0.3s"
            });

            jQuery("#" + prev_selected_id[selected_id]).addClass('after');
            jQuery(set).addClass('after');
        }

        function stepMouseOut(set) {
            jQuery(set).css({
                "border-top": "1px solid #646f81",
                "border-bottom": "1px solid #646f81",
                "border-right": "1px solid #646f81",
                "border-left": "1px solid #646f81",
                "color": "#646f81",
            });

            jQuery("#" + prev_selected_id[selected_id]).removeClass('after');
            jQuery(set).removeClass('after');
        }

        jQuery("#top-menu-btns>a.step").mouseover(function (e) {
            if (e.target.id) selected_id = e.target.id;
            stepMouseOver(this);
        });

        jQuery("#top-menu-btns>a.step").click(function (e) {
            jQuery(this).addClass(" clicked-step");

            if (options.prev_clicked_step == "top-menu-btns-anchor") {
                if (TAB_ANCHORS.options.is_pick) TAB_ANCHORS.disablePick();
            }

            if (options.prev_clicked_step != "") {
                jQuery("#" + options.prev_clicked_step).removeClass("clicked-step");
                jQuery("#" + btn_ids[options.prev_clicked_step]).css("display", "none");
                jQuery("#" + btn_ids[jQuery(this).attr('id')]).css("display", "block");

                if (jQuery(this).attr('id') == "top-menu-btns-details") {

                }
                else if (jQuery(this).attr('id') == "top-menu-btns-steel") {

                    TAB_STEEL_MEMBERS.resizePanel();
                }
                else if (jQuery(this).attr('id') == "top-menu-btns-anchor") {

                }

                else if (jQuery(this).attr('id') == "top-menu-btns-welds") {
                    if (TAB_WELDS.options.stiff_tab_check && STIFFENER_PART.options.jsx_data.length > 0) {
                        jQuery("#toggle-welds-pos-jsx").append('<div id="welds-tab-jsx" style="width:50%;margin-top: 20px;margin-bottom: 0px;"></div>');
                        TAB_WELDS.resizePanel();
                    }
                    else jQuery("#toggle-welds-pos-jsx").empty();

                }

            }
            options.prev_clicked_step = jQuery(this)[0].id;
        });

        jQuery("#top-menu-btns>a.step").mouseout(function (e) {
            stepMouseOut(this);
        });


        // Windows Resize ----------------------------------------------------------------------------------------------------------------

        function updatePanelHeight() {
            let wind_height = jQuery("body").height();
            let top_menu_height = jQuery("#main-container div.w3-container").height();
            jQuery("#left-panel>div").height(wind_height - top_menu_height);
            jQuery("#right-panel").height(wind_height - top_menu_height);
        }

        function updatePanelWidth() {
       
            if (options.prev_clicked_step == "top-menu-btns-steel") TAB_STEEL_MEMBERS.resizePanel();
            else if (options.prev_clicked_step == "top-menu-btns-welds") {
                if (TAB_WELDS.options.stiff_tab_check && STIFFENER_PART.options.jsx_data.length > 0) TAB_WELDS.resizePanel();
            }
        }

     
        updatePanelHeight();

        jQuery(window).resize(function (e) {
 
            updatePanelHeight();
            updatePanelWidth();
        });

        // Panel resize ---------------------------------------------------------------------------------------------------------------------
        
        jQuery("#dragging-box").css({
            "width": "4px",
            "height": jQuery("#right-panel").height() + "px"
        });

        let changePanelWidth = function (move_e) {
            let diff = jQuery("body").width() - (jQuery("#top-menu-btns").parent().width() + 32);
            jQuery("#left-panel").width(move_e.clientX - 0.5 * diff);
            jQuery("#right-panel").width(jQuery("#top-menu-btns").parent().width() + 32 - (move_e.clientX - 0.5 * diff));
            THREE_INIT.onWindowResize();
            updatePanelWidth();
        };

        let onDruggingClick = function (click_e) {
            document.getElementById("all-panels").addEventListener('mousemove', changePanelWidth, false);
        };

        document.getElementById("dragging-box").addEventListener('mousedown', onDruggingClick, false);

        document.addEventListener('mouseup', function (e) {
            document.getElementById("all-panels").removeEventListener("mousemove", changePanelWidth, false);
            let left = jQuery("#left-panel").width();
            let right = jQuery("#right-panel").width();
            jQuery("#left-panel").css("width", 100 * left / (left + right) + "%");
            jQuery("#right-panel").css("width", 100 * right / (left + right) + "%");
        });


        // Settings modal ------------------------------------------------------------------------------------------

        jQuery("#menu-settings").click(function(e){
            jQuery("#menu-settings-modal").modal({centered: false}).modal('show');
            
        });

        jQuery("#settings-tabs").tab();

        jQuery("#settings-modal-ok-btn").click(e=>{jQuery("#menu-settings-modal").modal('hide');});


        // if (DESIGN_CODE == "American") {

        //     jQuery("#set-factors-box").append();

        // }

    };






    return {
        init: init,
        options: options
    };

})();