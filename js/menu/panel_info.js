


let PANEL_INFO = (function () {


    let options = {
        info_col_name: "",
        info_shear_name: "",
        info_anchor_name: "5/8",
        info_col_mat: "",
        info_base_mat: "",
        info_anchor_mat: "A325",
        info_shear_mat: "",
        info_stiff_mat:"A36",
        info_grout_mat: "",
        info_foundation_mat: "3000",
        info_units: "",
    };


    let init = function () {

        let content = "";

        content += `<tr><td style="color:#ff531a">Profiles</td><td><p></p></td></tr>`;
        content += `<tr><td>Column:</td><td><p class="info-col-name">${options.info_col_name}</p></td></tr>`;
        if (options.info_shear_name != "") content += `<tr><td>Shear-lug:</td><td><p class="info-shear-name">${options.info_shear_name}</p></td></tr>`;
        content += `<tr><td>Anchor:</td><td><p class="anchor-name">${options.info_anchor_name}</p></td></tr>`;
        content += `<tr><td style="color:#ff531a">Materials</td><td><p></p></td></tr>`;
        content += `<tr><td>Column:</td><td><p class="info-col-mat">${options.info_col_mat}</p></td></tr>`;
        content += `<tr><td>Base plate:</td><td><p class="info-base-mat">${options.info_base_mat}</p></td></tr>`;
        content += `<tr><td>Stiffeners:</td><td><p class="info-stiff-mat">${options.info_stiff_mat}</p></td></tr>`;
        content += `<tr><td>Anchors:</td><td><p class="info-anchor-mat">${options.info_anchor_mat}</p></td></tr>`;
        if (options.info_shear_mat != "") content += `<tr><td>Shear-lug:</td><td><p class="info-shear-mat">${options.info_shear_mat}</p></td></tr>`;
        content += `<tr><td>Foundation:</td><td><p class="info-foundation-mat">${options.info_foundation_mat}</p></td></tr>`;
        content += `<tr><td>Grouting:</td><td><p class="info-foundation-mat">${options.info_grout_mat}</p></td></tr>`;
        content += `<tr style="color:#ff531a"><td>Units:</td><td><p class="info-units-mat">${options.info_units}</p></td></tr>`;

        jQuery("#info-panel").empty().append("<table>" + content + "</table>");
    };


    return {
        init: init,
        options: options
    };


})();