


let MAIN_JSX_DRAWINGS = (function () {

    let options = {
        board: null,
        base_ratio: 1.0,
        weld_data: {}
    };


    let resizePanel = function () {
        let baseplate_width = parseFloat(TAB_STEEL_MEMBERS.data["steel-baseplate-width"]);
        let baseplate_height = parseFloat(TAB_STEEL_MEMBERS.data["steel-baseplate-height"]);
        let ratio = baseplate_height / baseplate_width;
        let jsx_div = jQuery("#base-view-jsx");
        if (ratio < 1.0) jsx_div.height(jsx_div.width() * ratio);
        else jsx_div.height(jsx_div.width());
        options.base_ratio = ratio;
    };


    let initColumn = function (set) {

        let points = TAB_STEEL_JSX_DRAWINGS.options.points.map((val) => { return [val[0] * set.factor, val[1] * set.factor]; });

        let sec = TAB_STEEL_JSX_DRAWINGS.options.sec;
        if (sec == "i-shape") {
            set.board.create('polygon', points, { fillColor: set.back_color_1, highlight: false, fixed: true, fillOpacity: 1.0, vertices: { visible: false }, borders: { fixed: true, strokeColor: set.back_color_2, highlight: false, strokeWidth: set.edge_width }, });
        }
        else if (["rectangular", "pipe"].indexOf(sec) != -1) {
            set.board.create('polygon', points, { fillColor: set.back_color_1, highlight: false, fixed: true, fillOpacity: 1.0, vertices: { visible: false }, borders: { fixed: true, strokeColor: set.back_color_2, highlight: false, strokeWidth: set.edge_width }, });
            points = TAB_STEEL_JSX_DRAWINGS.options.sub_points;
            set.board.create('polygon', points, { fillColor: set.back_color_2, highlight: false, fixed: true, fillOpacity: 1.0, vertices: { visible: false }, borders: { fixed: true, strokeColor: set.back_color_2, highlight: false, strokeWidth: set.edge_width }, });
        }

        let sec_size = TAB_STEEL_JSX_DRAWINGS.options.size * set.factor;

        if (set.axis) {
            set.board.create('line', [[-0.5 * sec_size, 0], [0.5 * sec_size, 0]], { straightFirst: false, straightLast: false, strokeWidth: 1, strokeColor: 'black', highlight: false, dash: 2, fixed: true });
            set.board.create('line', [[0, -0.5 * sec_size], [0, 0.5 * sec_size]], { straightFirst: false, straightLast: false, strokeWidth: 1, strokeColor: 'black', highlight: false, dash: 2, fixed: true });
            set.board.create('text', [-0.5 * sec_size, 0, 'Z'], { strokeColor: 'black', anchorX: 'right', anchorY: 'middle', fontSize: 12, display: 'internal', highlight: false, fixed: true });
            set.board.create('text', [0.5 * sec_size, 0, 'Z'], { strokeColor: 'black', anchorX: 'left', anchorY: 'middle', fontSize: 12, display: 'internal', highlight: false, fixed: true });
            set.board.create('text', [0, 0.5 * sec_size, 'Y'], { strokeColor: 'black', anchorX: 'middle', anchorY: 'bottom', fontSize: 12, display: 'internal', highlight: false, fixed: true });
            set.board.create('text', [0, -0.5 * sec_size, 'Y'], { strokeColor: 'black', anchorX: 'middle', anchorY: 'top', fontSize: 12, display: 'internal', highlight: false, fixed: true });
        }
    };


    let iniAnchors = function (set) {

        let bolt_data = TAB_ANCHOR_DIMS[jQuery("#anchor-prop-diam").dropdown("get value")];

        let num = 1;
        if (TAB_ANCHOR_THREE.options.jsx_coord_single.length != 0) {
            let anchor = TAB_ANCHOR_THREE.options.jsx_coord_single;
            for (let i = 0; i < anchor.length; i++) {
                let p = set.board.create('point', [anchor[i][0], anchor[i][1]], { name: '', size: 0 });
                set.board.create('circle', [p, 0.5 * bolt_data.d], { strokeColor: set.back_color_1, fillColor: set.back_color_1, fixed: true, highlight: false });
                num++;
            }
        }
        if (TAB_ANCHOR_THREE.options.jsx_coord_frame.length != 0) {
            let anchor = TAB_ANCHOR_THREE.options.jsx_coord_frame;
            for (let i = 0; i < anchor.length; i++) {
                let p = set.board.create('point', [anchor[i][0], anchor[i][1]], { name: '', size: 0 });
                set.board.create('circle', [p, 0.5 * bolt_data.d], { strokeColor: set.back_color_1, fillColor: set.back_color_1, fixed: true, highlight: false });
                num++;
            }
        }
        if (TAB_ANCHOR_THREE.options.jsx_coord_line.length != 0) {
            let anchor = TAB_ANCHOR_THREE.options.jsx_coord_line;
            for (let i = 0; i < anchor.length; i++) {
                let p = set.board.create('point', [anchor[i][0], anchor[i][1]], { name: '', size: 0 });
                set.board.create('circle', [p, 0.5 * bolt_data.d], { strokeColor: set.back_color_1, fillColor: set.back_color_1, fixed: true, highlight: false });
                num++;
            }
        }
    };


    let initStiffeners = function (set) {
        let points = STIFFENER_PART.options.jsx_data;
        for (let i = 0; i < points.length; i++) {
            set.board.create('polygon', points[i], { fillColor: set.back_color_1, highlight: false, fixed: true, fillOpacity: 0.6, vertices: { visible: false }, borders: { fixed: true, strokeColor: set.back_color_2, highlight: false, strokeWidth: set.edge_width }, });
        }
    };


    let initWelds = function (set) {

        function drawPoly(poly, color) {
            set.board.create('polygon', poly, {
                fillColor: color,
                highlight: false,
                fixed: true,
                fillOpacity: 1.0,
                vertices:
                    { visible: false }, borders: { fixed: true, strokeColor: color, highlight: false, strokeWidth: 1.0 },
            });
        }

        let welds = WELDS_COLUMN_PART.options.welds;
        let poly_arr = [];

        if (TAB_STEEL_JSX_DRAWINGS.options.sec == "i-shape") {

            if (welds.bfl != 0) poly_arr.push(TAB_STEEL_JSX_DRAWINGS.weld_bfl(welds.bfl/set.factor, set.factor));
            if (welds.bfr != 0) poly_arr.push(TAB_STEEL_JSX_DRAWINGS.weld_bfr(welds.bfr/set.factor, set.factor));
            if (welds.tb != 0) poly_arr.push(TAB_STEEL_JSX_DRAWINGS.weld_tb(welds.tb/set.factor, set.factor));
            if (welds.tf != 0) poly_arr.push(TAB_STEEL_JSX_DRAWINGS.weld_tf(welds.tf/set.factor, set.factor));
            if (welds.tfl != 0) poly_arr.push(TAB_STEEL_JSX_DRAWINGS.weld_tfl(welds.tfl/set.factor, set.factor));
            if (welds.tfr != 0) poly_arr.push(TAB_STEEL_JSX_DRAWINGS.weld_tfr(welds.tfr/set.factor, set.factor));
            if (welds.wl != 0) poly_arr.push(TAB_STEEL_JSX_DRAWINGS.weld_wl(welds.wl/set.factor, set.factor));
            if (welds.wr != 0) poly_arr.push(TAB_STEEL_JSX_DRAWINGS.weld_wr(welds.wr/set.factor, set.factor));
            for (let i = 0; i < poly_arr.length; i++)  drawPoly(poly_arr[i], set.back_color_1);
        }
        else if (["rectangular", "pipe"].indexOf(TAB_STEEL_JSX_DRAWINGS.options.sec) != -1) {
            if (welds.p != 0) {
                let out_edge = [];
                let init_edge = [];
                if (TAB_STEEL_JSX_DRAWINGS.options.sec == "rectangular") {
                    init_edge = TAB_STEEL_JSX_DRAWINGS.getRectPoints(TAB_STEEL_JSX_DRAWINGS.options.dims, 0, set.factor);
                    out_edge = TAB_STEEL_JSX_DRAWINGS.getRectPoints(TAB_STEEL_JSX_DRAWINGS.options.dims, -welds.p, set.factor);
                }
                else if (TAB_STEEL_JSX_DRAWINGS.options.sec == "pipe") {
                    init_edge = TAB_STEEL_JSX_DRAWINGS.getPipePoints(TAB_STEEL_JSX_DRAWINGS.options.dims, 0, set.factor);
                    out_edge = TAB_STEEL_JSX_DRAWINGS.getPipePoints(TAB_STEEL_JSX_DRAWINGS.options.dims, welds.p, set.factor);
                }
                for (let i = 0; i < out_edge.length; i++) {
                    if (i == out_edge.length - 1) {
                        let p1 = out_edge[0];
                        let p2 = out_edge[i];
                        let p3 = init_edge[i];
                        let p4 = init_edge[0];
                        drawPoly([p1, p2, p3, p4], set.back_color_1);
                    }
                    else {
                        let p1 = out_edge[i];
                        let p2 = out_edge[i + 1];
                        let p3 = init_edge[i + 1];
                        let p4 = init_edge[i];
                        drawPoly([p1, p2, p3, p4], set.back_color_1);
                    }
                }
            }
        }

        if (WELDS_STIFFENER_PART.options.jsx_data.length > 0) {
            for (let i = 0; i < WELDS_STIFFENER_PART.options.jsx_data.length; i++) {
                drawPoly(WELDS_STIFFENER_PART.options.jsx_data[i], set.back_color_1);
            }
        }

    };


    let initStiffID = function (set) {

        function drawLine(poly, color, name) {
            set.board.create('text', [0.5 * (poly[0][0] + poly[1][0]), 0.5 * (poly[0][1] + poly[1][1]), name[0]], {
                strokeColor: 'black',
                anchorX: name[1],
                anchorY: name[2],
                fontSize: 14,
                display: 'internal',
                highlight: false,
                fixed: true
            });
        }

        let poly_arr = [];

        if (TAB_STEEL_JSX_DRAWINGS.options.sec == "i-shape") {

            poly_arr.push(TAB_STEEL_JSX_DRAWINGS.weld_bfl(0, set.factor));
            poly_arr.push(TAB_STEEL_JSX_DRAWINGS.weld_bfr(0, set.factor));
            poly_arr.push(TAB_STEEL_JSX_DRAWINGS.weld_tb(0, set.factor));
            poly_arr.push(TAB_STEEL_JSX_DRAWINGS.weld_tf(0, set.factor));
            poly_arr.push(TAB_STEEL_JSX_DRAWINGS.weld_tfl(0, set.factor));
            poly_arr.push(TAB_STEEL_JSX_DRAWINGS.weld_tfr(0, set.factor));
            poly_arr.push(TAB_STEEL_JSX_DRAWINGS.weld_wl(0, set.factor));
            poly_arr.push(TAB_STEEL_JSX_DRAWINGS.weld_wr(0, set.factor));
        }
        else if (["rectangular", "pipe"].indexOf(TAB_STEEL_JSX_DRAWINGS.options.sec) != -1) {
            if (TAB_STEEL_JSX_DRAWINGS.options.sec == "rectangular") poly_arr = TAB_STEEL_JSX_DRAWINGS.getRectPoints(TAB_STEEL_JSX_DRAWINGS.options.dims, 0, set.factor);
            else if (TAB_STEEL_JSX_DRAWINGS.options.sec == "pipe") poly_arr = TAB_STEEL_JSX_DRAWINGS.getPipePoints(TAB_STEEL_JSX_DRAWINGS.options.dims, 0, set.factor);
        }

        let points = STIFFENER_PART.options.jsx_data;

        for (let i = 0; i < points.length; i++) {
            drawLine([points[i][0], points[i][2]], "black", ["s" + (i + 1), "middle", "middle"]);
            drawLine([points[i][1], points[i][2]], "black", ["", "middle", "top"]);
        }

    };




    let initWeldsBase = function () {

        JXG.Options.infobox.strokeColor = 'black';
        let lf = (TAB_PROJECT_DETAILS.options.units == "imperial") ? 25.4 : 1.0;

        options.board_w = JXG.JSXGraph.initBoard('welds-tab-jsx', { showNavigation: false, showCopyright: false, axis: false, grid: false, keepaspectratio: true });

        let baseplate_width = parseFloat(TAB_STEEL_MEMBERS.data["steel-baseplate-width"]) * lf;
        let baseplate_height = parseFloat(TAB_STEEL_MEMBERS.data["steel-baseplate-height"]) * lf;
        let ratio = baseplate_height / baseplate_width;
        let size = Math.max(baseplate_width, baseplate_height);

        if (ratio < 1.0) {
            let BB = [-0.52 * size, 0.52 * size * ratio, 0.52 * size, -0.52 * size * ratio];
            options.board_w.setBoundingBox(BB);
        }
        else {
            let BB = [-0.52 * size, 0.52 * size, 0.52 * size, -0.52 * size];
            options.board_w.setBoundingBox(BB);
        }

        // baseplate
        let points = [
            [-0.5 * baseplate_width, 0.5 * baseplate_height],
            [0.5 * baseplate_width, 0.5 * baseplate_height],
            [0.5 * baseplate_width, -0.5 * baseplate_height],
            [-0.5 * baseplate_width, -0.5 * baseplate_height],
        ];

        options.board_w.create('polygon', points, { fillColor: '#FFFFFF', highlight: false, fixed: true, fillOpacity: 1.0, vertices: { visible: false }, borders: { fixed: true, strokeColor: "#808080", highlight: false, strokeWidth: 1 }, });
        initColumn({ board: options.board_w, factor: lf, edge_width: 1.0, back_color_1: "#cccccc", back_color_2: "#FFFFFF", axis: false });
        iniAnchors({ board: options.board_w, factor: lf, back_color_1: "#cccccc", edge_width: 0.0 });
        initStiffeners({ board: options.board_w, back_color_1: "#cccccc", back_color_2: "#808080", edge_width: 1.0 });
        initStiffID({ board: options.board_w, factor: lf, back_color_1: "#808080", back_color_2: "black", edge_width: 0.0 });
    };


    let initBase = function (set) {

        resizePanel();

        JXG.Options.infobox.strokeColor = 'black';
        let lf = (TAB_PROJECT_DETAILS.options.units == "imperial") ? 25.4 : 1.0;

        options.board = JXG.JSXGraph.initBoard('base-view-jsx', { showNavigation: false, showCopyright: false, axis: false, grid: false, keepaspectratio: true });

        let baseplate_width = parseFloat(TAB_STEEL_MEMBERS.data["steel-baseplate-width"]) * lf;
        let baseplate_height = parseFloat(TAB_STEEL_MEMBERS.data["steel-baseplate-height"]) * lf;
        let ratio = baseplate_height / baseplate_width;
        let size = Math.max(baseplate_width, baseplate_height);
        if (ratio < 1.0) {
            let BB = [-0.52 * size, 0.52 * size * ratio, 0.52 * size, -0.52 * size * ratio];
            options.board.setBoundingBox(BB);
        }
        else {
            let BB = [-0.52 * size, 0.52 * size, 0.52 * size, -0.52 * size];
            options.board.setBoundingBox(BB);
        }

        // baseplate
        let points = [
            [-0.5 * baseplate_width, 0.5 * baseplate_height],
            [0.5 * baseplate_width, 0.5 * baseplate_height],
            [0.5 * baseplate_width, -0.5 * baseplate_height],
            [-0.5 * baseplate_width, -0.5 * baseplate_height],
        ];
        options.board.create('polygon', points, { fillColor: '#00B6FF', highlight: false, fixed: true, fillOpacity: 0.2, vertices: { visible: false }, borders: { fixed: true, strokeColor: "black", highlight: false, strokeWidth: 1 }, });

        initColumn({ board: options.board, factor: lf, edge_width: 1.0, back_color_1: "#00B6FF", back_color_2: "#c3e9fb", axis: true });
        iniAnchors({ board: options.board, factor: lf, back_color_1: "#ff9933", edge_width: 1.0 });
        initStiffeners({ board: options.board, back_color_1: "#33cc33", edge_width: 0.0 });
        initWelds({ board: options.board, factor: lf, back_color_1: "red", back_color_2: "black", edge_width: 0.0 });
    };

    return {
        options: options,
        resizePanel: resizePanel,
        initBase: initBase,
        initWeldsBase: initWeldsBase
    };

})();