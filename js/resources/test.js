function rotate(CX, CY, X, Y, angle) {
    var rad = angle * Math.PI / 180.0;
    var nx = Math.cos(rad) * (X - CX) - Math.sin(rad) * (Y - CY) + CX;
    var ny = Math.sin(rad) * (X - CX) + Math.cos(rad) * (Y - CY) + CY;
    return [nx, ny];
}



console.log(rotate(0, 0, 1, 0, -180))